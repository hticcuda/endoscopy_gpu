#ifndef CONTROLS_USER_H
#define CONTROLS_USER_H
#include <QDataStream>

class Controls_sensor
{
public:
    int brightness1,brightness2, blacklev, exposure, variable_gain,alc_aec_state,led;
    void reset();
    void copyFrom(Controls_sensor&);
};
QDataStream & operator << (QDataStream & st, const Controls_sensor & obj);
QDataStream & operator >> (QDataStream & st, Controls_sensor & obj);
#endif // CONTROLS_USER_H
