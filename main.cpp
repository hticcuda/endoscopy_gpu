#include "mainwidget.h"
#include <QApplication>
#include <debug.h>
#include <iostream>
#include <QScopedPointer>
#include <QTextStream>
#include <QDateTime>
#include <QLoggingCategory>
#include <QFile>
#include <QDir>




QScopedPointer<QFile>   m_logFile;

void messageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg);


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QString logFolderName(QDir::currentPath()+"/logs/");

    if(!QDir(logFolderName).exists())
    {
        QDir().mkdir(logFolderName);
    }

    QString logFileName = QString(logFolderName+"/Application_%1.txt")
            .arg(QDate::currentDate().toString("yyyy_MM_dd"));

    m_logFile.reset(new QFile(logFileName));
        // Open the file logging
    m_logFile.data()->open(QFile::Append | QFile::Text);
        // Set handler
    qInstallMessageHandler(messageHandler);

    qInfo() << "** Starting application **";

    mainwidget w;
//    GPU_Render * render = new GPU_Render();
//    render
    QMainWindow window,
//            window1,
            window2; //separate window for chart not managed by main

    //FIXME: get this from argv
    w.move(1920,0);
//    QApplication::processEvents();
//    w.getRend()->loop();
#if enable_chart == 1
//    w.chart->setTitle("Y histogram");
//    w.chart_1->setTitle("Y-horizontal");
//    w.chart_2->setTitle("LUT chart");

//    w.chart->legend()->hide();
//    w.chart_1->legend()->hide();
//    w.chart_2->legend()->hide();

//    w.chart->setAnimationOptions(QChart::NoAnimation);
//    w.chart_1->setAnimationOptions(QChart::NoAnimation);
//    w.chart_2->setAnimationOptions(QChart::NoAnimation);

   QChartView chartView(w.chart);
////    QChartView chartview1(w.chart_1);
  QChartView chartview2(w.chart_2);
//    //chartView.setRenderHint(QPainter::Antialiasing);
//    window.setCentralWidget(&chartView);
////    window1.setCentralWidget(&chartview1);
//    window2.setCentralWidget(&chartview2);

   // QChartView chartView(w.chartRGB);
//    QChartView chartview1(w.chart_1);
    //QChartView chartview2(w.chart_2);
    //chartView.setRenderHint(QPainter::Antialiasing);
    window.setCentralWidget(&chartView);
//    window1.setCentralWidget(&chartview1);
    window2.setCentralWidget(&chartview2);


    window.resize(800, 300);
//    window1.resize(800, 300);
   // window2.resize(800, 300);

    window.show();
//    window1.show();
    window2.show();
//    video.show();
#endif
    std::cout<<"main" <<std::endl;

//    w.show();
//new QShortcut(QKeySequence(Qt::Key_Q), w, SLOT(o));
    return a.exec();
     std::cout<<"main" <<std::endl;
}


void messageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    // Open stream file writes
    QTextStream out(m_logFile.data());
    // Write the date of recording
    out << QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss.zzz ");
    // By type determine to what level belongs message
    switch (type)
    {
    case QtInfoMsg:     out << "INF "; break;
    case QtDebugMsg:    out << "DBG "; break;
    case QtWarningMsg:  out << "WRN "; break;
    case QtCriticalMsg: out << "CRT "; break;
    case QtFatalMsg:    out << "FTL "; break;
    }
    // Write to the output category of the message and the message itself
    out << context.category << ": "
        << msg << endl;
    out.flush();    // Clear the buffered data
}
