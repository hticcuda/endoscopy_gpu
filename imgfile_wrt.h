#ifndef IMGFILE_WRT_H
#define IMGFILE_WRT_H

#include <QObject>
#include <iostream>
using namespace std;

#include <GL/freeglut.h>

#include <libavcodec/avcodec.h>
#include <libavutil/imgutils.h>
#include <libavutil/opt.h>
#include <libswscale/swscale.h>
#include <png.h>

class ImgFile_wrt : public QObject
{
    Q_OBJECT

    QThread * workerThread;
//    cv::Mat cv_pixels( height, width, CV_8UC3 );
    uint8_t *rgb = NULL;

    GLubyte *pixels = NULL;
    png_byte **png_bytes = NULL;
    png_byte ***png_rows = NULL;

public:
    explicit ImgFile_wrt(QObject *parent = nullptr);
    void ffmpeg_encoder_set_frame_yuv_from_rgb(uint8_t *rgb);
    void ffmpeg_encoder_encode_frame(uint8_t *rgb);


signals:

public slots:
    void ppmwrite(const char *filename, unsigned int width, unsigned int height, unsigned char **pixels);
    void vidwrite(uint8_t **rgb, GLubyte **pixels, unsigned int width, unsigned int height);
    void ffmpeg_encoder_start(const char *filename, int codec_id, int fps, int width, int height);
    void ffmpeg_encoder_finish(void);
    void pngwrite(const char *filename, unsigned int width, unsigned int height, GLubyte **pixels, png_byte **png_bytes, png_byte ***png_rows);
};

#endif // IMGFILE_WRT_H
