#include "hotplug.h"
#include <iostream>
#include <libusb-1.0/libusb.h>
#include <QShortcut>

int done = 0;
libusb_device_handle *handle = NULL;
int rc;
cv::Mat hotplug_image= cv::imread("./hotplug_image/camera_disconnected.jpg"),rgb,rgb_save ;
bool window_close = false,restart_streaming = false;


static int LIBUSB_CALL hotplug_callback(libusb_context *ctx, libusb_device *dev, libusb_hotplug_event event, void *user_data)
{
    struct libusb_device_descriptor desc;
    int rc;

    (void)ctx;
    (void)dev;
    (void)event;
    (void)user_data;

    rc = libusb_get_device_descriptor(dev, &desc);
    if (LIBUSB_SUCCESS != rc) {
        fprintf (stderr, "Error getting device descriptor\n");
    }

//    printf ("Device attached: %04x:%04x\n", desc.idVendor, desc.idProduct);

    if (handle) {
        libusb_close (handle);
        handle = NULL;
    }

    rc = libusb_open (dev, &handle);
    if (LIBUSB_SUCCESS != rc) {
        fprintf (stderr, "Error opening device\n");
    }

    done++;

    if(window_close)
    {
        cv::destroyWindow("hotplug");
        window_close = false;
        restart_streaming = true;
    }

    return 0;
}

static int LIBUSB_CALL hotplug_callback_detach(libusb_context *ctx, libusb_device *dev, libusb_hotplug_event event, void *user_data)
{
    (void)ctx;
    (void)dev;
    (void)event;
    (void)user_data;

//    printf ("Device detached\n");

    if (handle) {
        libusb_close (handle);
        handle = NULL;
    }

//   cv::namedWindow( "hotplug",cv::WINDOW_NORMAL );
//   cv::moveWindow("hotplug",1024,0);
//   cv::setWindowProperty("hotplug",cv::WND_PROP_FULLSCREEN,cv::WINDOW_FULLSCREEN);
//   cv::imshow("hotplug",hotplug_image);
   window_close = true;
   done++;
   restart_streaming = false;

    return 0;
}


HotPlug::HotPlug(gpu_qrend * hrendObj, UvcCommunication *uvcobj)
{
    uvc = uvcobj;
//    std::cout<<uvc->ready();
    hrend = hrendObj;
    libusb_hotplug_callback_handle hp[2];
    libusb_device_descriptor desc;
    libusb_device *dev;
    int product_id, vendor_id, class_id;

    vendor_id  = 8730;
    product_id = 256;
    class_id   = LIBUSB_HOTPLUG_MATCH_ANY;

    rc = libusb_init (NULL);
    if (rc < 0)
    {
        printf("failed to initialise libusb: %s\n", libusb_error_name(rc));
    }

    if (!libusb_has_capability (LIBUSB_CAP_HAS_HOTPLUG)) {
        printf ("Hotplug capabilites are not supported on this platform\n");
        libusb_exit (NULL);
    }

    rc = libusb_hotplug_register_callback (NULL, (libusb_hotplug_event)LIBUSB_HOTPLUG_EVENT_DEVICE_ARRIVED, (libusb_hotplug_flag)0, vendor_id,
        product_id, class_id, hotplug_callback, NULL, &hp[0]);
    if (LIBUSB_SUCCESS != rc) {
        fprintf (stderr, "Error registering callback 0\n");
        libusb_exit (NULL);
    }

    rc = libusb_hotplug_register_callback (NULL, (libusb_hotplug_event)LIBUSB_HOTPLUG_EVENT_DEVICE_LEFT, (libusb_hotplug_flag)0, vendor_id,
        product_id,class_id, hotplug_callback_detach, NULL, &hp[1]);
    if (LIBUSB_SUCCESS != rc) {
        fprintf (stderr, "Error registering callback 1\n");
        libusb_exit (NULL);
    }


}

void HotPlug::run()
{
    while (exit) {
        if(uvc->getDevice_status())
        {
//            cv::namedWindow( "hotplug",cv::WINDOW_NORMAL );
//            cv::moveWindow("hotplug",1024,0);
//            cv::setWindowProperty("hotplug",cv::WND_PROP_FULLSCREEN,cv::WINDOW_FULLSCREEN);
//            cv::imshow("hotplug",hotplug_image);
//            hrend->hotplugHit();
            window_close = true;
            uvc->setDevice_status(false);
        }
        rc = libusb_handle_events (NULL);
        if(window_close)
        {
            uvc->setClose_window_flag(true);
        }
        if(restart_streaming)
        {
            uvc->setRestart_camera_streaming(true);
            restart_streaming = false;
        }
    }

    if (handle) {
        libusb_close (handle);
    }

    libusb_exit (NULL);
}
