#ifndef CODEC_H
#define CODEC_H
#include <opencv2/core/core.hpp>
#include <libuvc/libuvc.h>
#include <QMutex>
#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/opengl.hpp>
#include <debug.h>
#include "messagetypes.h"
#include "postprocessing.h"

#include <QObject>

class Codec : public QObject
{
    Q_OBJECT

    cv::Mat Y1[2], CB1[2], CR1[2],
        Y1_1[2]; //,field1;
    cv::Mat //Y,CB,CR;
        Ycomb, Yframe, CB_frame, CR_frame;
    cv::Mat signMat;
    cv::Mat signMatField2;
    cv::Mat fieldT1, fieldT2, v2_minus_v1, chromaMat;
    cv::Mat CB_half[2],CR_half[2];
    cv::Mat CBcombined_half, CRcombined_half;
    bool flagg = false;
    int countt = 0;
    cv::Size framesize;

    YCbCr_container data;
    bool adaptiveDeinter = false;
    bool combineFields = false;
    int count = 0;


    unsigned short fieldwidth;
    unsigned short fieldheight;

    unsigned short last_frame; //,diff;

    static unsigned short data16[962*383];
    static QMutex mutex;

    void raw2ycbcr( const cv::Mat field, unsigned short sequencenum);
    bool combining_fields2frames(unsigned short sequencenum);
    void setNewField(const cv::Mat field, unsigned long sequencenum);

    bool linear_doubling_fields(unsigned short sequencenum);
    void savemat(cv::Mat D, std::string fname, size_t dtypesiz);
public:
    explicit Codec(QObject *parent = nullptr);
    std::string filename;
    bool save_raw_flag = false;
    int limit = 10;
//    void copyResult(cv::Mat &Yout, cv::Mat &CBout, cv::Mat &CRout);
    static void cb(uvc_frame_t *frame, void * ptr);

//    void copyResultframe(cv::Mat &Yout, cv::Mat &CBout, cv::Mat &CRout);

    void setAdaptiveDeinter(bool value);
    void setCombineFields(bool value);
signals:
    void frameReady(YCbCr_container, short sequencenum);


public slots:

};

#endif // CODEC_H
