#ifndef MESSAGETYPES_H
#define MESSAGETYPES_H

#include <opencv2/core/core.hpp>

struct YCbCr_container {
    cv::Mat Y,CB,CR;
};


#endif // MESSAGETYPES_H
