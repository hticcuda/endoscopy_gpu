#ifndef POSTPROCESSING_H
#define POSTPROCESSING_H
#include <opencv2/core/core.hpp>
//#include <opencv2/cudaarithm.hpp>
#include <QDebug>

class Postprocessing
{
    cv::Mat blurred;
    cv::Mat lowContrastMask;
    cv::Mat sharpened;
//    double min,max;
    std::vector<float> cumhist_float;
    std::vector<float> hist_Y_quantized;

    cv::Mat hist_Y[2]; // used by mainwindows

    bool auto_sensor_on = false;
    std::vector<uchar> LUT8U_auto_bright; //(256);


    int lower = 0,upper = 255, upper_prev = 0;
//    cv::Ptr<cv::cuda::LookUpTable> lut_d;

public:
    Postprocessing();
    double sigma ;//,threshold;//= 1.2, threshold = 2;
    void image_sharpening(float sharp_value, float bgthreshold, const cv::Mat & img);
    cv::Mat getSharpened() const;

    void hist_find_limits();
    void update_lut_auto_bright(float brightness, float contrast,float gamma, bool auto_brightness_set, bool auto_brightness_peak);

    void update_hist(const cv::Mat &Y8,short sequencenum);
    void applyLUT(cv::cuda::GpuMat &Y8);
    void applyLUT(cv::Mat &Y8);
    std::vector<float> getHistY1();
    std::vector<float> getHistY2();
    std::vector<uchar> getLUT() const;

    void update_hist2(const cv::Mat &altY);
    void image_zoom(cv::Mat &rgb,float zoom);

    void quantizehist(unsigned char nbins=4);
    int compute_nextvga(int vga);

    void printhist();
    float dimmingFactor(const cv::Mat & ROI);
    void wb_apply(cv::Mat & BGR);
    bool wb_calculate(cv::Mat input);
    void enhance_triscan(cv::Mat &R, cv::Mat &G, cv::Mat &B, cv::Mat &rgb);
    bool wb_simple_calculate(cv::Mat input);
    static void findHist(cv::Mat histInput, cv::Mat &histCalc);
    void apply_clahe(cv::Mat & image);
    int getUpper();
private:
    void smooth_lut_auto_bright(float slope);
    void fixedLUT(float gamma);
};

#endif // POSTPROCESSING_H
