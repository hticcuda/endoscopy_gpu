#ifndef HOTPLUG_H
#define HOTPLUG_H

#include <QThread>
#include <uvccommunication.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <codec.h>
#include <gpu_qrend.h>


class HotPlug : public QThread
{

    UvcCommunication * uvc;
    Codec * codecproc;
    gpu_qrend * hrend;


public:
    HotPlug(gpu_qrend * hrendObj, UvcCommunication *uvcobj);
     bool exit = true;

private:
     void run();

signals:
     void hotplugReady();
};

#endif // HOTPLUG_H
