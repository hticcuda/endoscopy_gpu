#ifndef GPU_QREND_H
#define GPU_QREND_H

#include "imgfile_wrt.h"
#include <QObject>
#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include <QTime>


//// includes, cuda
#include <vector_types.h>
#include <driver_functions.h>
#include <cuda_runtime.h>
#include <cuda_gl_interop.h>

//// CUDA utilities and system includes
#include <helper_cuda.h>
//#include <helper_cuda_gl.h>
#include <helper_functions.h>
#include <png.h>

typedef unsigned int  uint;
typedef unsigned char uchar;

const uint height = 1080, width = 1920, imgs = 4;
const uint sheight = 360, swidth = 480;
const uint sheight1 = 540, swidth1 = 720;

const cudaExtent volumeSize = make_cudaExtent(960, 720, 1);

const dim3 blockSize(16, 16, 1);
const dim3 gridSize((width + blockSize.x - 1) / blockSize.x, (height + blockSize.y - 1) / blockSize.y);
const dim3 gridPip1((swidth + blockSize.x - 1) / blockSize.x, (sheight + blockSize.y - 1) / blockSize.y);
const dim3 gridPip2((swidth1 + blockSize.x - 1) / blockSize.x, (sheight1 + blockSize.y - 1) / blockSize.y);

class gpu_qrend : public QObject
{
    Q_OBJECT

    QThread * workerThread;
    ImgFile_wrt * wrtObj;
    uint pheight = 0, pwidth = 0;
    uint imSz = 0, pipSz = 0;
    bool prFrz;
    uint maskSz = 0;
    float zoomVal = 0;
    float wb[9];
    float gcontrast, ggam, gchroma, grGain, gbGain, gbright;
    cv::Mat rgba;
    cv::Mat hotplug_image= cv::imread("./camera_disconnected.jpg");
    uint hotHit = 0;

//    png_byte **png_bytes = NULL;
//    png_byte ***png_rows = NULL;
public:
    explicit gpu_qrend(QObject *parent = nullptr);
    int flag = 1;
    void saveImg();

    void wb_apply();

    void setPheight(const uint &value);

    void setPwidth(const uint &value);

    void setPrFrz(bool value);

    void setImSz(const uint &value);

    void setPipSz(const uint &value);

    void setMaskSz(const uint &value);

    void setZoomVal(float value);

    void setGcontrast(float value);

    void setGgam(float value);

    void setGchroma(float value);

    void setGrGain(float value);

    void setGbGain(float value);

    void setGbright(float value);

    void setHotHit(const uint &value);

private:
    void initGL(int *argc, char **argv);

    void screenshot_ppm(const char *filename, unsigned int width, unsigned int height, GLubyte **pixels);
    void ffmpeg_encoder_glread_rgb(uint8_t **rgb, GLubyte **pixels, unsigned int width, unsigned int height);
    void screenshot_png(const char *filename, unsigned int width, unsigned int height, GLubyte **pixels, png_byte **png_bytes, png_byte ***png_rows);
    void loadVolumeData();
signals:
    void writeppm(const char *filename, unsigned int width, unsigned int height, unsigned char **pixels);
//    void ffmpeg_encoder_encode_frame(uint8_t *pixels, unsigned int width, unsigned int height);
    void writevid(uint8_t **rgb, GLubyte **pixels, unsigned int width, unsigned int height);
    void writepng(const char *filename, unsigned int width, unsigned int height, GLubyte **pixels, png_byte **png_bytes, png_byte ***png_rows);

public slots:
    void gpuCpy(cv::Mat rgb, bool ispip);
    void hotplugHit();
};

#endif // GPU_QREND_H
