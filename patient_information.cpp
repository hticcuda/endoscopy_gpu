#include "patient_information.h"

Patient_Information::Patient_Information(QObject *parent) : QObject(parent)
{

}

QDataStream & operator << (QDataStream & out, const Patient_Information & obj)
{
    out<<obj.id << obj.name << obj.gender << obj.dob << obj.comments << obj.age;
    return out;
}

QDataStream & operator >> (QDataStream & in,  Patient_Information & obj)
{
    in >> obj.id
            >> obj.name
            >> obj.gender
            >> obj.dob
            >> obj.comments
            >> obj.age;
    return in ;
}
