#include "patientwindow.h"
#include "ui_patientwindow.h"
#include "QDir"
#include <QFile>
#include <QTextStream>
#include <QShortcut>

PatientWindow::PatientWindow(FrameContext *frameobj,QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::PatientWindow)
{
    ui->setupUi(this);
    frame = frameobj;
    if( !QDir("Patient_information").exists())
    {
        QDir().mkdir("Patient_information");
    }
    new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_S), this, SLOT(on_patient_save_pushButton_clicked()));
    new QShortcut(QKeySequence(Qt::Key_N), this, SLOT(on_new_patient_create()));
    new QShortcut(QKeySequence(Qt::Key_C), this, SLOT(on_clear_patients_info()));
    new QShortcut(QKeySequence(Qt::Key_E), this, SLOT(on_edit_patient_info()));
    new QShortcut(QKeySequence(Qt::Key_Q), this, SLOT(close()));
    new QShortcut(QKeySequence(Qt::Key_R), this, SLOT(on_patient_reset_pushButton_clicked()));
    new QShortcut(QKeySequence(Qt::Key_S), this, SLOT(on_patient_set_pushButton_clicked()));
    new QShortcut(QKeySequence(Qt::Key_F12), this, SLOT(show_shortcut_list_on_display()));
    new QShortcut(QKeySequence(Qt::Key_Escape), this, SLOT(remove_focus()));
    new QShortcut(QKeySequence(Qt::Key_D), this, SLOT(delete_delected_patient_info()));

    //    new QShortcut(QKeySequence(Qt::Key_F12), this, SLOT());

//    QShortcut *shortcut = new QShortcut(QKeySequence("F1"), parent);
//    QObject::connect(shortcut, SIGNAL(activated()), this, SLOT(on_clear_patients_info()));


//    ui->patient_time->setText(acq->getCurrent_time());
//    ui->patient_date->setText(acq->getCurrent_date());
    ui->patient_info_frame->setDisabled(true);
    ui->home_pushButton->setDisabled(true);
    ui->patient_listWidget->setDisabled(true);

    QString patients_list_file = QDir::currentPath() +"/Patient_information/"+"patients_list.txt";
    QStringList fields;

    if (QFileInfo::exists(patients_list_file))
    {
        QFile file(patients_list_file);
        file.open(QIODevice::ReadOnly | QIODevice::Text);
        QTextStream in_all(&file);
        while(!in_all.atEnd()) {
            QString line = in_all.readLine();
            fields = line.split(",");

         }
        if(fields.size() > 0){
            for(int li = 0; li< fields.size();li++){
                if(fields[li] != ""){
                    ui->patient_listWidget->addItem(fields[li]);
                }

            }

        }
        file.close();
        {

        }
    }

}

PatientWindow::~PatientWindow()
{
    delete ui;
}

void PatientWindow::on_patient_save_pushButton_clicked()
{

    patient_id = ui->patient_id_lineEdit->text();
    patient_name = ui->patient_name_lineEdit->text();
    patient_gender = ui->patient_gender_lineEdit->text();
    patient_dob = ui->patient_dob_lineEdit->text();
    patient_comments = ui->patient_comment_lineEdit->text();
    patient_age = ui->patient_age_lineEdit->text();

    patient_info.id = patient_id;
    patient_info.name = patient_name;
    patient_info.gender = patient_gender;
    patient_info.dob = patient_dob;
    patient_info.comments = patient_comments;
    patient_info.age = patient_age;



    ui->patient_listWidget->addItem(patient_name+"_"+patient_id);


    QString preference = QDir::currentPath()+"/Patient_information/"+patient_id+"_"+patient_name+".bin";
     {
         QFile file(preference);
         file.open(QIODevice::WriteOnly | QIODevice::Text);
         QDataStream out(&file);
         out<<patient_info;
         file.close();
     }

    QString patient_list_file = QDir::currentPath() + "/Patient_information"+"/patients_list.txt";
    QStringList fields;
    if (QFileInfo::exists(patient_list_file)){
        QFile file(patient_list_file);

        file.open(QIODevice::ReadOnly | QIODevice::Text);
        QTextStream in_all(&file);
        while(!in_all.atEnd()) {
            QString line = in_all.readLine();
            fields = line.split(",");

        }

        ui->patient_listWidget->clear();
        ui->patient_listWidget->addItem(patient_id+"_"+patient_name);
        if(fields.size() > 0){
            for(int li = 0; li< fields.size();li++){
                if((fields[li] != NULL)&&(fields[li] != patient_id + "_" + patient_name)){
                    ui->patient_listWidget->addItem(fields[li]);
                }
            }

        }
        file.close();

        QFile file_1(patient_list_file);
        file_1.open(QIODevice::WriteOnly | QIODevice::Text);
        QTextStream out_1(&file_1);
        out_1 << patient_id +"_"+patient_name;
        if(fields.size() > 0){
            for(int li = 0; li< fields.size();li++){
                if((fields[li] != NULL)&&(fields[li] != patient_id + "_" + patient_name)){
                    if((fields[li] != NULL)&&(fields[li] != patient_id + "_" + patient_name)){
                        out_1<< ",";
                        out_1<< fields[li];
                    }

                }


            }
        }

        file_1.close();
    }
    else
    {
        QFile file(patient_list_file);
        file.open(QIODevice::WriteOnly | QIODevice::Text);
        QTextStream out(&file);
        out << patient_id+"_"+patient_name;
        file.close();
        ui->patient_listWidget->clear();
        ui->patient_listWidget->addItem(patient_id+"_"+patient_name);

    }

    ui->patient_info_frame->setDisabled(true);
    ui->patient_listWidget->setDisabled(true);
    on_clear_patients_info();


}

void PatientWindow::on_patient_listWidget_currentTextChanged(const QString &currentText)
{
    QString selected_user_path_file = QDir::currentPath() + "/Patient_information" + "/" + currentText+".bin" ;
    if (QFileInfo::exists(selected_user_path_file)){
        QFile file(selected_user_path_file);
        file.open(QIODevice::ReadOnly | QIODevice::Text);
        QDataStream in_all(&file);
        in_all>>patient_info;
        file.close();
    }

    ui->patient_id_lineEdit->setText(patient_info.id);
    ui->patient_name_lineEdit->setText(patient_info.name);
    ui->patient_gender_lineEdit->setText(patient_info.gender);
    ui->patient_dob_lineEdit->setText(patient_info.dob);
    ui->patient_comment_lineEdit->setText(patient_info.comments);
    ui->patient_age_lineEdit->setText(patient_info.age);

}

void PatientWindow::on_new_patient_create()
{
    ui->patient_info_frame->setDisabled(false);
    ui->patient_id_lineEdit->setFocus();
    on_clear_patients_info();
    ui->patient_listWidget->setDisabled(true);
}

void PatientWindow::on_clear_patients_info()
{
    ui->patient_id_lineEdit->clear();
    ui->patient_name_lineEdit->clear();
    ui->patient_gender_lineEdit->clear();
    ui->patient_dob_lineEdit->clear();
    ui->patient_comment_lineEdit->clear();
    ui->patient_age_lineEdit->clear();
    ui->patient_listWidget->setDisabled(true);
}

void PatientWindow::on_edit_patient_info()
{
    ui->patient_listWidget->setDisabled(false);
    ui->patient_listWidget->setFocus();
    ui->patient_info_frame->setDisabled(false);
}

void PatientWindow::on_patient_reset_pushButton_clicked()
{
    on_clear_patients_info();
    on_new_patient_create();
    ui->patient_listWidget->setDisabled(true);
}

void PatientWindow::on_patient_set_pushButton_clicked()
{
//    displaywid->name = ui->patient_name_lineEdit->text();

//    displaywid->id = ui->patient_id_lineEdit->text();
//    displaywid->age = ui->patient_age_lineEdit->text();
//    displaywid->gender = ui->patient_gender_lineEdit->text();
////    acq->set_patient_info_display(patient_name,patient_id,patient_gender,patient_age,"test Doctor");
//    displaywid->patient_flag = 1;
//    frame->name = displaywid->name;
//    frame->id = displaywid->id;
}

void PatientWindow::show_shortcut_list_on_display()
{
    shortcut = new Form_shortcut();
    shortcut->show();

}

void PatientWindow::remove_focus()
{
    ui->patient_info_frame->setDisabled(true);
}

void PatientWindow::delete_delected_patient_info()
{
   QString id = ui->patient_id_lineEdit->text();
   QString name = ui->patient_name_lineEdit->text();

   QString patient_list_file = QDir::currentPath() + "/Patient_information"+"/patients_list.txt";

   QStringList fields;
//    QStringList new_list;
   if (QFileInfo::exists(patient_list_file)){
       QFile file(patient_list_file);

       file.open(QIODevice::ReadOnly | QIODevice::Text);
       QTextStream in_all(&file);
       while(!in_all.atEnd()) {
           QString line = in_all.readLine();
           fields = line.split(",");

       }

       ui->patient_listWidget->clear();
       if(fields.size() > 0){
           for(int li = 0; li< fields.size();li++){
               if((fields[li] != NULL)&&(fields[li] == id + "_" + name)){
                   fields.removeOne(id + "_" + name);
               }
           }
       }
       file.close();

       QFile file_1(patient_list_file);
       file_1.open(QIODevice::WriteOnly | QIODevice::Text);
       QTextStream out_1(&file_1);
       if(fields.size() > 0){
           for(int li = 0; li< fields.size();li++){
               out_1<< fields[li];
               out_1<< ",";
           }
           ui->patient_listWidget->clear();
           for(int li = 0; li< fields.size();li++){
               if(fields[li] != ""){
                   ui->patient_listWidget->addItem(fields[li]);
               }
           }
       }

       file_1.close();
       on_clear_patients_info();
       remove_focus();

   }
}

