#ifndef FORM_UVC_H
#define FORM_UVC_H

#include <QWidget>
#include <controls_calibration.h>
#include <uvccommunication.h>


namespace Ui {
class Form_uvc;
}

class Form_uvc : public QWidget
{
    Q_OBJECT
//    FrameContext frame;
    UvcCommunication *uvc;

    Controls_calibration calib;

public:
    explicit Form_uvc(UvcCommunication *uvcobj, QWidget *parent = 0);
    ~Form_uvc();

    void initialize_calibration();

public slots:
    void on_reset_calibration_clicked();

    void on_h1_slider_valueChanged(int value);

    void on_h2_slider_valueChanged(int value);

    void on_shp_slider_valueChanged(int value);

    void on_rg_slider_valueChanged(int value);

    void on_shd_slider_valueChanged(int value);

    void on_shd_width_slider_valueChanged(int value);



    void on_close_calib_ui_clicked();

signals:
//    void calib_changed(Controls_calibration & calib);
private slots:
    void on_save_calib_ui_clicked();

private:
    Ui::Form_uvc *ui;
};

#endif // FORM_UVC_H
