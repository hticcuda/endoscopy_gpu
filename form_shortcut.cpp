#include "form_shortcut.h"
#include "ui_form_shortcut.h"

#include "iostream"

Form_shortcut::Form_shortcut(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Form_shortcut)
{
    ui->setupUi(this);

    new QShortcut(QKeySequence(Qt::Key_W), this, SLOT(show_shortcut_list()));
}

Form_shortcut::~Form_shortcut()
{
    delete ui;
}

void Form_shortcut::show_shortcut_list()
{
//    std::cout<<"here"<<std::endl;
    close();
}
