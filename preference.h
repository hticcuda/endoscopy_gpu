#ifndef PREFERENCE_H
#define PREFERENCE_H

#include <QObject>

class Preference : public QObject
{
    Q_OBJECT
public:
    explicit Preference(QObject *parent = nullptr);
    int pip_window_size,pip_window_position,gain_r,gain_b,enhancement_level,image_preset,mask=2;
    int button_mapping,image_compression_level,video_compression_level;

signals:

public slots:
};

QDataStream & operator << (QDataStream & st, const Preference & obj);
QDataStream & operator >> (QDataStream & st, Preference & obj);

#endif // PREFERENCE_H
