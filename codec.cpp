#include "codec.h"
#include <unistd.h>
#include <chrono>
#include <thread>

unsigned short Codec::data16[]; //= new unsigned short[962*383];
QMutex Codec::mutex;

Codec::Codec(QObject *parent) : QObject(parent),
   fieldwidth(962)
  ,fieldheight(383)
{
    signMat.create(fieldheight,fieldwidth-1,CV_32FC1);
    signMatField2.create(fieldheight,fieldwidth-1,CV_32FC1);

    float sign=1.0;
    for(int row = 0; row < fieldheight; ++ row)
    {
        for(int col = 0; col < fieldwidth-1; col++)
        {
            bool even_row = (row%2==0);  //0,2,4,...row is even
            bool even_col = (col%2==0);  //0,2,4,...col is even
            if((even_row && even_col) || (!even_row && !even_col))
            {
                signMat.at<float>(row,col)  = -sign;   //chroma = -chroma;
                signMatField2.at<float>(row,col)  = sign;
            }
            else
            {
                signMat.at<float>(row,col)  = sign;
                signMatField2.at<float>(row,col)  = -sign;
            }
        }
    }

    int wid = fieldwidth; //field.cols;
    int hei = fieldheight; //field.rows;

    framesize=cv::Size(960,720);

    int newwid = wid-1;//wid/2;
    //int newhei = hei-1;


//    int idx = sequencenum%2;

    //Y CB CR declartion variables in 32bit float character

    for(int idx = 0; idx < 2; ++idx)
    {
        Y1[idx].create( hei, newwid,CV_32FC1);
        CB1[idx].create(hei/2, newwid, CV_32FC1);
        CR1[idx].create( hei/2, newwid, CV_32FC1);
    }

    int yheight = Y1[0].rows;
    int ywidth = Y1[0].cols;

    //Ycomb.create(cv::Size(ywidth,yheight*2),Y1[0].type());
    Ycomb.create(cv::Size(framesize.width,framesize.height),Y1[0].type());
    Y1_1[0].create(cv::Size(framesize.width,framesize.height/2),Y1[0].type());
    Y1_1[1].create(cv::Size(framesize.width,framesize.height/2),Y1[1].type());

    cv::Size cbsiz(CB1[0].cols/2,CB1[0].rows),
            crsiz(CR1[0].cols/2, CR1[0].rows);

//    if(cbsiz.width==0 || cbsiz.height==0)
//    {
//        std::cerr <<"cb empty: ";
//        return;
//    }
    CB_half[0].create(cbsiz,CB1[0].type());
    CR_half[0].create(crsiz,CR1[0].type());
    CB_half[1].create(cbsiz,CB1[1].type());
    CR_half[1].create(crsiz,CR1[1].type());

    CBcombined_half.create(cbsiz.height*2,cbsiz.width,CB_half[0].type());
    CRcombined_half.create(crsiz.height*2,crsiz.width,CR_half[0].type());

}

void Codec::setAdaptiveDeinter(bool value)
{
    adaptiveDeinter = value;
}

void Codec::setCombineFields(bool value)
{
    combineFields = value;
}

void Codec::setNewField(const cv::Mat field, unsigned long sequencenum)
{
    static cv::TickMeter tm1;
    static unsigned long fieldcount_local = 0;
#if time_raw_ycbcr == 1
    TIC(tm1);
#endif

    mutex.lock();
    raw2ycbcr(field,sequencenum);
    mutex.unlock();

    int idx = sequencenum%2;

    cv::resize(Y1[idx],Y1_1[idx],cv::Size(framesize.width,framesize.height/2));
//    cv::resize(Y1[1],Y1_1[1],cv::Size(framesize.width,framesize.height/2));
    cv::Mat Ytempp;
    Y1[idx].convertTo(Ytempp,CV_8UC1,1/64.);
    //cv::imshow("Yframe",Ytempp);
    //cv::waitKey(1);

    cv::resize(CB1[idx],CB_half[idx],cv::Size(0,0),0.5,1,cv::INTER_AREA);
    cv::resize(CR1[idx],CR_half[idx],cv::Size(0,0),0.5,1,cv::INTER_AREA);

    if(fieldcount_local>0)
    {
//#if COMBINING == 1
        if(combineFields)
        {
            if(combining_fields2frames(sequencenum))
            {
                data.Y = Yframe;
                data.CB = CB_frame;
                data.CR = CR_frame;
    //            cv::resize(Yframe,data.Y,framesize);
    //            cv::resize(CB_frame,data.CB,framesize);
    //            cv::resize(CR_frame, data.CR, framesize);

                emit frameReady(data, sequencenum);
            }
        }
//#else
        else
        {
            if(linear_doubling_fields(sequencenum))
            {
                data.Y = Yframe;
                data.CB = CB_frame;
                data.CR = CR_frame;

                emit frameReady(data,sequencenum);
            }
        }
//#endif
    }
    fieldcount_local ++;

#if time_raw_ycbcr == 1
    TOC(tm1,sequencenum);
#endif

}



void Codec:: cb(uvc_frame_t *frame, void *ptr)
{

    Codec *_cp = static_cast<Codec*>(ptr);
    static cv::TickMeter tm;
    #if time_cb == 1
        TIC(tm);
    #endif
    if(frame->height*frame->width*2!=frame->data_bytes) {
        std::cerr<< std::endl << "data size mismatch : " << frame->height << "x" << frame->width << "x2 != " << frame->data_bytes;
//        _cp->stop_streaming();
        return;
    }
#if raw == 1
    int wid = static_cast<int>(frame->width);
    int hei = static_cast<int>(frame->height);

//    static unsigned short data16[962*383];



    mutex.lock();

#if 0
    unsigned char * bytedata = static_cast<unsigned char*>(frame->data);

    for(int ii=0; ii < wid*hei; ii++)
    {
        data16[ii] = static_cast<unsigned short>(bytedata[2*ii]) | static_cast<unsigned short>(bytedata[2*ii+1] << 8) ;
    }
#else
    memcpy((void*)data16,frame->data, wid*hei*(sizeof(short)));
#endif




    unsigned short sequencenum = data16[0];
//    std::cout << "start " << sequencenum << " ";
//    fieldcount_local++;

    static unsigned long lastfieldnum = 0;

    int skip = -1;

    if(lastfieldnum>0)
    {
        skip = sequencenum-lastfieldnum;
        if(skip>1)
        {
            std::cerr << "@" << sequencenum<<": skip " << skip << " ";
        }
    }
    else {
        std::cerr << "start " << sequencenum << " ";
    }

    lastfieldnum = sequencenum;

    data16[0]=0;

    cv::Mat field( hei, wid, CV_16UC1, static_cast<void*>(data16));
//    cv::namedWindow("yy");
//    cv::imshow("field",field);
    if(_cp->save_raw_flag)
        {
//        std::cout<<"i am here !!"<<std::endl;
//        std::cout<<_cp->filename<<std::endl;
//        std::cout<<_cp->countt<<std::endl;
            if(sequencenum%5 == 0)
            {
                _cp->flagg = true;
                _cp->countt = _cp->countt + 1;
//                std::cout<<sequencenum<<std::endl;
            }
//            std::cout<<"count_"<<_cp->flagg<<std::endl;
            if(_cp->flagg)
            {
                _cp->savemat(field, _cp->filename+"/"+std::to_string(sequencenum) +".dat", field.depth());
                _cp->count = _cp->count + 1;

//                std::cout<<"countt_"<<_cp->countt<<std::endl;
                if(_cp->count == 2)
                {
//                    std::cout<<"countdjhdhjdhjd"<<std::endl;
                    _cp->count =0;
                    _cp->flagg = false;
                }
                if(_cp->countt == _cp->limit)
                {
                    _cp->countt =0;
                    _cp->save_raw_flag = false;
                }

            }

            }



    mutex.unlock();
    cv::Mat Ytempp;
    field.convertTo(Ytempp,CV_8UC1,1/64.);

    if(skip>0 && skip%2==1)
    {
#if halfrate==1
        if(sequencenum%2==0)
#endif
        _cp->setNewField(field, sequencenum); //, Ymat,Cbmat,Crmat);


    //if(fieldcount_local%2==0 && fieldcount_local>1)
    //{
        //display only even field
        //_this->emitFrameReady(sequencenum);
    }
    //_this->display_imshow(currentfieldnum);
#else
    std::cout<<"Hi Ananth!!!!"<<std::endl;
    static unsigned char R[962*766];
    static unsigned char G[962*766];
    static unsigned char B[962*766];

    //getting the width and height of the frame received
    //the type conversion happens for the height width, which will be used in the cv::Mat wihch is integer
    int wid = static_cast<int>(frame->width);
    int hei = static_cast<int>(frame->height);

    //stroing the frame data to the byte data for the channel separation
    unsigned char * bytedata = static_cast<unsigned char*>(frame->data);

    //looping varibales for storing the data RGB datas
    unsigned int gidx=0, bidx=0, ridx=0;

    //mat varibale which holds the mat format of the raw data
    cv::Mat bgrmat, Led,  foregroundMask;

    //data in order from the fpga-> ORGB
    //skipping the every fouth bit whihc is ZERO - storing all the RGB separately
    for(int ii=0; ii<wid*hei*4; ii+=4)
    {
        B[ridx++] = /*(unsigned short)*/ (bytedata[ii]) ;
        G[gidx++] = /*(unsigned short)*/ (bytedata[ii+1]) ;
        R[bidx++] = /*(unsigned short) */(bytedata[ii+2]) ;
    }

    //making the data received from array format to matrix in R G B matrix
    cv::Mat Rmat( hei, wid,CV_8UC1, static_cast<void*>(R));
    cv::Mat Bmat(hei, wid, CV_8UC1, static_cast<void*>(B));
    cv::Mat Gmat( hei, wid, CV_8UC1, static_cast<void*>(G));

    //merging the channels into a single rgb channel
    cv::Mat channels [] = {Bmat,Gmat,Rmat};
    cv::merge(channels,3, bgrmat);
    cv::namedWindow("yy");
    cv::imshow("field",bgrmat);
#endif
    #if time_cb == 1
        TOC(tm,sequencenum);
        if(tm.getCounter()%600==0) //once in 10 seconds
            qDebug() << "cb: " << laptime << " ";
    #endif
}

bool Codec::linear_doubling_fields(unsigned short sequencenum)
{
    int idx = sequencenum%2;

    int other = (idx+1)%2;

    cv::Size crsiz = CR_half[0].size();
    int yheight = Y1[idx].rows;

    yheight = framesize.height/2;

/*
    for(int ri=0;ri<crsiz.height;ri++)
    {
        CB_half[idx].row(ri).copyTo(CBcombined_half.row(2*ri+idx));
        CR_half[idx].row(ri).copyTo(CRcombined_half.row(2*ri+idx));
        CB_half[other].row(ri).copyTo(CBcombined_half.row(2*ri+other));
        CR_half[other].row(ri).copyTo(CRcombined_half.row(2*ri+other));
    }
*/

    cv::Range range0(0,3);

    cv::Mat CBtemp, CRtemp;

    cv::parallel_for_(range0,[&](const cv::Range & range)
        {
        for(int r = range.start; r < range.end; r++){
            if(r==0){
                cv::resize(Y1_1[idx],Yframe,framesize);
            }

            if(r==1){
                GaussianBlur(CB_half[idx],CBtemp,cv::Size(),2,3);
                cv::resize(CBtemp,CB_frame,Yframe.size());
            }
            if(r==2){
                GaussianBlur(CR_half[idx],CRtemp, cv::Size(),2,3);
                cv::resize(CRtemp,CR_frame,Yframe.size());
            }
        }
    }, 3);
    return true;
}

bool Codec::combining_fields2frames(unsigned short sequencenum)
{
    int idx = sequencenum%2;

    int other = (idx+1)%2;
//    static  unsigned short last_frame =  0;

    short diff = sequencenum - last_frame;
    if(diff > 1)
        qDebug()<<"combining field drop: ("<<sequencenum<<") " << diff << "";
    last_frame = sequencenum;

    if(diff%2==0) // can't combine in the below method
        return false;

    cv::Size crsiz = CR_half[0].size();
    int yheight = Y1[idx].rows;

    yheight = framesize.height/2;
    cv::Mat currentField, nextField, fieldDiff, interm, alphaField, histDiff,alphaComb,alphaInter;
    float thresh1;
    float thresh2;

//#if ADAPTIVE_COMBINE == 1
    if(adaptiveDeinter)
    {
        Y1_1[idx].convertTo(currentField,CV_8UC1,1/64.);
        Y1_1[other].convertTo(nextField,CV_8UC1,1/64.);
        cv::absdiff(currentField, nextField,fieldDiff);
        cv::resize(Y1_1[idx],Ycomb,framesize);
        Postprocessing::findHist(fieldDiff, histDiff);
        cv::Mat_<float>::const_iterator iter=histDiff.begin<float>(),
                end = histDiff.end<float>();

        int xi = 0;//,maxindex=0;
        bool lower_found = false,
                upper_found = false;
        float sum_i = 0;
        for(;iter!=end;++iter,++xi) {
            sum_i += *iter;
            if(sum_i>0.25f && !lower_found) {
                thresh1 = xi;
                lower_found = true;
            }
            if(sum_i>=0.5f && !upper_found) {
                thresh2 = xi;
                upper_found = true;
            }
        }
        //thresh1 = 0;
        //thresh1 = thresh2 - thresh1;
        thresh2 = thresh2+0.0001f; //thresh2 - thresh1;
        alphaField = (fieldDiff - thresh1)/(thresh2 - thresh1);

        double minVal;
        double maxVal;
        cv::Point minLoc;
        cv::Point maxLoc;
        cv::minMaxLoc( fieldDiff, &minVal, &maxVal, &minLoc, &maxLoc );
        std::cerr << "Thesh " << thresh1 << " and " << thresh2 << " "<<minVal <<" "<< maxVal<<std::endl;
        alphaField.convertTo(alphaField,Y1_1[idx].type(),1);
    }
//#endif

    for(int ri=0;ri<yheight;ri++)
    {
//#if ADAPTIVE_COMBINE == 1
        if(adaptiveDeinter)
        {
            Y1_1[other].row(ri).copyTo(Ycomb.row(2*ri+other), fieldDiff.row(ri) <= thresh1);
            alphaComb = 1 - alphaField.row(ri);
            cv::multiply(alphaComb,Y1_1[other].row(ri),alphaComb);
            cv::multiply(alphaField.row(ri),Ycomb.row(2*ri+other),alphaInter);
            cv::add(alphaComb,alphaInter,interm);
            interm.copyTo(Ycomb.row(2*ri+other), (fieldDiff.row(ri) > thresh1) & (fieldDiff.row(ri) <= thresh2));
        }
//#else
        else{
            Y1_1[idx].row(ri).copyTo(Ycomb.row(2*ri+idx));
            Y1_1[other].row(ri).copyTo(Ycomb.row(2*ri+other));
        }
//#endif
//        Y1[idx].row(ri).copyTo(Ycomb.row(2*ri+idx));
//        Y1[other].row(ri).copyTo(Ycomb.row(2*ri+other));
        if(ri<crsiz.height) {
            CB_half[idx].row(ri).copyTo(CBcombined_half.row(2*ri+idx));
            CR_half[idx].row(ri).copyTo(CRcombined_half.row(2*ri+idx));
            CB_half[other].row(ri).copyTo(CBcombined_half.row(2*ri+other));
            CR_half[other].row(ri).copyTo(CRcombined_half.row(2*ri+other));
        }
    }

    //cv::resize(Ycomb,Yframe,framesize);
    Yframe = Ycomb;

    cv::Range range0(0,2);

    cv::Mat CBtemp, CRtemp;

    cv::parallel_for_(range0,[&](const cv::Range & range)
        {
            for(int r = range.start; r < range.end; r++){
                if(r==0){
                    GaussianBlur(CBcombined_half,CBtemp,cv::Size(),2,3);
                    cv::resize(CBtemp,CB_frame,Yframe.size());

                }
                if(r==1){
                    GaussianBlur(CRcombined_half,CRtemp, cv::Size(),2,3);
                    cv::resize(CRtemp,CR_frame,Yframe.size());
                }
            }
        } ,2);

    return true;
}


void Codec::raw2ycbcr( cv::Mat field1, unsigned short sequencenum) //, cv::Mat & Y, cv::Mat & CB, cv::Mat & CR )
{
    int wid = fieldwidth; //field.cols;
    int hei = fieldheight; //field.rows;

    int newwid = wid-1;//wid/2;

    cv::Mat fieldTemp1 = field1(cv::Rect(0,0,newwid,hei));   //16U here
    cv::Mat fieldTemp2 = field1(cv::Rect(1,0,newwid,hei));

    fieldT1.create( hei, newwid,CV_32FC1);
    fieldT2.create( hei, newwid,CV_32FC1);

    fieldTemp1.convertTo(fieldT1,Y1[0].type()); //FIXME: why convert to 32F
    fieldTemp2.convertTo(fieldT2,Y1[0].type());

    v2_minus_v1 = fieldT2 - fieldT1;

    chromaMat.create( hei, newwid,CV_32FC1);

    if(sequencenum%2==1)//odd field
    {
        cv::multiply(v2_minus_v1,signMat,chromaMat);// if it is field 1
    }
    else
    {
        cv::multiply(v2_minus_v1,signMatField2,chromaMat);// if it is field 2, use signmatfield2
    }


//    mutex.lock();
    //calculation of Y from the field
    //cv::Mat Ytemp = (field(cv::Rect(0,0,newwid,hei)) + field(cv::Rect(1,0,newwid,hei)))/2;
    //cv::Mat Ytemp = fieldTemp1 + fieldTemp2;

    //Ytemp.convertTo(Y, Y.type());//type conversion to float
    //Ytemp = Ytemp/2;
    int idx = sequencenum%2;
    cv::Mat Y = Y1[idx],
            CB = CB1[idx],
            CR = CR1[idx];

    Y = (fieldT1 + fieldT2)*0.5;

#if Y_field_save == 1
    std::string fname =  std::to_string(sequencenum) ;
    FILE* FP = fopen(fname.c_str(),"wb");
        int sizeImg[2] = { Y.cols , Y.rows };
        fwrite(sizeImg, 2, sizeof(int), FP);
        fwrite(Y.data, size_t(Y.cols * Y.rows), sizeof(float), FP);
        fclose(FP);
        std::cerr<<"written";
#endif

#if Y_plane_display_org == 1
    cv::namedWindow("Y_plane");
    cv::imshow("Y_plane",Y/16383);
#endif
    // added for blurring between rows
    //cv::Mat Ytemp1 = (Ytemp(cv::Rect(0,0,newwid,newhei)) + Ytemp(cv::Rect(0,1,newwid,newhei)))/2;
    //Ytemp1.convertTo(Y, Y.type());

//    cv::Mat //v2_minus_v1( hei, newwid,CV_32FC1);



    if(sequencenum%2==1)
    {
        for(int row1 = 0; row1 < hei-1; row1++)
        {
            bool even_row = (row1%2==0);  //0,2,4,...row is even

            if(even_row)// index is even
                chromaMat.row(row1).copyTo(CB.row(row1/2));//CB.at<float>(row/2,col)=chroma;
            else
                chromaMat.row(row1).copyTo(CR.row((row1-1)/2)); //CR.at<float>((row-1)/2, col)=chroma;
        }
    }
    else
    {
        for(int row1 = 0; row1 < hei-1; row1++)
        {
            bool even_row = (row1%2==0);  //0,2,4,...row is even
            if(even_row)
                chromaMat.row(row1).copyTo(CR.row(row1/2));//CB.at<float>(row/2,col)=chroma;
            else
                chromaMat.row(row1).copyTo(CB.row((row1-1)/2)); //CR.at<float>((row-1)/2, col)=chroma;
        }
    }


//    std::this_thread::sleep_for(std::chrono::milliseconds(20));

//    fieldcount++;
//    mutex.unlock();
}

//void Codec::copyResult(cv::Mat & Yout, cv::Mat & CBout, cv::Mat & CRout)
//{
////    if(mutex.tryLock(8))
////    {
//        Yframe.copyTo(Yout);
//        CB_frame.copyTo(CBout);
//        CR_frame.copyTo(CRout);
////        mutex.unlock();
////    }
//}

void Codec::savemat(cv::Mat D, std::string fname, size_t dtypesiz)
{
//              std::cerr<<"written";
//    std::cout<<fname<<std::endl;
    FILE* FP = fopen(fname.c_str(),"wb");
    int sizeImg[2] = { D.cols , D.rows };
    fwrite(sizeImg, 2, sizeof(int), FP);
    fwrite(D.data, size_t(D.cols * D.rows), dtypesiz, FP);
    fclose(FP);
//           std::cerr<<"written";
}
