#ifndef DEBUG_H
#define DEBUG_H

#if enable_chart == 1
#include <QtCharts/QChartView>
#include <QtCharts/QChart>
#include <QtCharts/QLineSeries>
#include <QtCharts/QValueAxis>
#include <QtCharts/QScatterSeries>
#endif

#include <QDebug>
#include "QTime"
#if enable_chart == 1
QT_CHARTS_USE_NAMESPACE
#endif

#define enable_chart 0
#define time_cb 0
#define time_raw_ycbcr 0
#define time_ycbcr_rgb 1
#define collectfielddata 0
#define display 0
#define paintfunc 0
#define RGB_DISPLAY 1
#define COMBINING 0
#define ADAPTIVE_COMBINE 0
#define dbg_write_img 0
#define halfrate 0
#define enable_glare_detection 0
#define field_display 0
#define Y_plane_display_org 0
#define Y_field_save 0
#define led_ramp 0
#define raw 1
#define RGB_conversion 1

#define TIC(tm) tm.start()

#define TOC(tm,seq) \
    tm.stop(); \
    float laptime = tm.getTimeMilli()/tm.getCounter();\
    if(laptime > 14) \
        qDebug() << "slow " << __FUNCTION__ <<"(" << seq << "):" << laptime ;

#endif // DEBUG_H
