#include "gpu_qrend.h"
#include "QThread"
#include <QDebug>
#include <helper_gl.h>
#include <GL/freeglut.h>

#include <iostream>

#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

//#include <png.h>
//#include <libavcodec/avcodec.h>
//#include <libavutil/imgutils.h>
//#include <libavutil/opt.h>



using namespace std;

float w = 0;  // texture coordinate in z
dim3 gridSz;
GLuint pbo[3];     // OpenGL pixel buffer object
struct cudaGraphicsResource *cuda_pbo_resource, *cuda_pbo_resource0, *cuda_pbo_resource1, *cuda_pbo_resource2; // CUDA Graphics Resource (to transfer PBO)

bool animate = true;

StopWatchInterface *timer = NULL, *timer1 = NULL;

uint *d_output = nullptr;

//// Auto-Verification Code
const int frameCheckNumber = 4;
int fpsCount = 0;        // FPS count for averaging
int fpsLimit = 1;        // FPS limit for sampling
int fpsCount1 = 0;        // FPS count for averaging
int fpsLimit1 = 1;        // FPS limit for sampling
int g_Index = 0;
unsigned int frameCount = 0;
unsigned int frameCount1 = 0;
unsigned int g_TotalErrors = 0;
volatile int g_GraphicsMapFlag = 0;
uint offheight = 0, offwidth = 0;
bool preFrz = 0;
float ifps, ifps1;
uint imSize = 3, pipSize = 0;
int pipwid = 0, piphei = 0;
uint mask = 1;
float zf = 1;
string fnf;
float contrast = 1, gam = 0.55f, chroma = 0.6f, rGain = 1, bGain = 1, bright = 0;

int hBufNo = 0;
int saveFlag = 0;
unsigned char *savedImg;
uchar *asd;
int pressCount = 0;
cv::VideoWriter outputVideo;
cv::Mat vpixels( height, width, CV_8UC3 );
cv::Mat cv_pixels( height, width, CV_8UC3 );

void *font = GLUT_BITMAP_TIMES_ROMAN_24;

static GLubyte *pixels = NULL;
static png_byte *png_bytes = NULL;
static png_byte **png_rows = NULL;
//static AVCodecContext *c = NULL;
//static AVFrame *frame;
//static AVPacket pkt;
//static FILE *file;
//static struct SwsContext *sws_context = NULL;
static uint8_t *rgb = NULL;
//static unsigned int nframes = 0;
gpu_qrend * srend;

extern "C" void cleanup();
extern "C" void cpyD2H(uchar *);
extern "C" void wbCpy(float *wb);
extern "C" void cudaCpy(const uchar *h_volume, cudaExtent volumeSize, int dBufNo);
extern "C" void initCuda(cudaExtent volumeSize, ushort *h_volume);
extern "C" void render_kernel(dim3 gridSize, dim3 blockSize, uint *d_output, uint imageW, uint imageH, float w, uint f, uint g, float zf,
                              float contrast, float gam, float chroma, float rGain, float bGain, float bright);

void gpu_qrend::screenshot_ppm(const char *filename, unsigned int width,
        unsigned int height, GLubyte **pixels) {
    const size_t format_nchannels = 3;
    *pixels = (GLubyte *)realloc(*pixels, format_nchannels * sizeof(GLubyte) * width * height);
    glReadPixels(0, 0, width, height, GL_RGB, GL_UNSIGNED_BYTE, *pixels);
    emit writeppm(filename,width,height,pixels);
}

void gpu_qrend::screenshot_png(const char *filename, unsigned int width, unsigned int height,
        GLubyte **pixels, png_byte **png_bytes, png_byte ***png_rows) {
    size_t /*i,*/ nvals;
    nvals = 4 * width * height;
    *pixels = (GLubyte *)realloc(*pixels, nvals * sizeof(GLubyte));
    *png_bytes = (png_byte *)realloc(*png_bytes, nvals * sizeof(png_byte));
    *png_rows = (png_byte **)realloc(*png_rows, height * sizeof(png_byte*));
    glReadPixels(0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, *pixels);
    emit writepng(filename, width, height, pixels, png_bytes, png_rows);
}

void gpu_qrend::ffmpeg_encoder_glread_rgb(uint8_t **rgb, GLubyte **pixels, unsigned int width, unsigned int height) {
    size_t /*i, j, k, cur_gl, cur_rgb,*/ nvals;
    const size_t format_nchannels = 4;
    nvals = format_nchannels * width * height;
    *pixels = (GLubyte *)realloc(*pixels, nvals * sizeof(GLubyte));
    *rgb = (uint8_t *)realloc(*rgb, nvals * sizeof(uint8_t));
    /* Get RGBA to align to 32 bits instead of just 24 for RGB. May be faster for FFmpeg. */
    glReadPixels(0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, *pixels);

    writevid(rgb,pixels,1920,1080);
}

void drawString(const char *str, int x, int y, float color[4], void *font)
{
    glPushAttrib(GL_LIGHTING_BIT | GL_CURRENT_BIT); // lighting and color mask
    glDisable(GL_LIGHTING);     // need to disable lighting for proper text color
    glDisable(GL_TEXTURE_2D);

    glColor4fv(color);          // set text color
    glRasterPos2i(x, y);        // place text position

    // loop all characters in the string
    while(*str)
    {
        glutBitmapCharacter(font, *str);
        ++str;
    }

    glEnable(GL_TEXTURE_2D);
    glEnable(GL_LIGHTING);
    glPopAttrib();
}

void showInfo()
{
    // backup current model-view matrix
    glPushMatrix();                     // save current modelview matrix
    glLoadIdentity();                   // reset modelview matrix
    gluOrtho2D(0, width, 0, height);  // set to orthogonal projection

    float color[4] = {1, 0, 1, 1};

    std::stringstream ss;

    ss << "  GL fps   " << int(ifps) << std::ends;
    drawString(ss.str().c_str(), 0, 1000, color, font);
    ss.str("");

    color[0] = 1;/*r*/    color[1] = 0;/*g*/    color[2] = 0;/*b*/    color[3] = 1;/*a*/

    ss << "  CV fps   " << int(ifps1) << std::ends;
    drawString(ss.str().c_str(), 0, 960, color, font);
    ss.str("");

    color[0] = 0;/*r*/    color[1] = 1;/*g*/    color[2] = 1;/*b*/    color[3] = 1;/*a*/

    ss << "  Hi Ananth " << saveFlag++ << std::ends;
    drawString(ss.str().c_str(), 0, 920, color, font);
    ss.str("");

    color[0] = 0;/*r*/    color[1] = 0;/*g*/    color[2] = 0;/*b*/    color[3] = 1;/*a*/

    if(saveFlag%60<40)
    {
        glBegin(GL_QUADS);
            glColor3d(1,0,0);
            glVertex3f(0,875,0);
            glColor3d(1,1,0);
            glVertex3f(4*420,875,0);
            glColor3d(1,1,1);
            glVertex3f(4*420,900,0);
            glColor3d(0,1,1);
            glVertex3f(0,900,0);
        glEnd();
        ss << "  Congratulations for this dynamic overlay " << std::ends;
        drawString(ss.str().c_str(), 0, 880, color, font);
    }

    // unset floating format
    ss << std::resetiosflags(std::ios_base::fixed | std::ios_base::floatfield);

    glPopMatrix();                   // restore to previous modelview matrix
}

void computeFPS()
{
    frameCount++;
    fpsCount++;

    if (fpsCount == fpsLimit)
    {
        char fps[256];
        /*float*/ ifps = 1.f / (sdkGetAverageTimerValue(&timer) / 1000.f);
        sprintf(fps, "simpleTexture3D: %3.1f fps", ifps);

        glutSetWindowTitle(fps);
        fpsCount = 0;

        fpsLimit = ftoi(MAX(1.0f, ifps));
        sdkResetTimer(&timer);
    }
}

void computeFPS1()
{
    frameCount1++;
    fpsCount1++;

    if (fpsCount1 == fpsLimit1)
    {
        ifps1 = 1.f / (sdkGetAverageTimerValue(&timer1) / 1000.f);
        fpsCount1 = 0;

        fpsLimit1 = ftoi(MAX(1.0f, ifps1));
        sdkResetTimer(&timer1);
    }
}

void reshape(int x, int y)
{
    glViewport(0, 0, x, y);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0.0, 1.0, 0.0, 1.0, 0.0, 1.0);
}

// render image using CUDA
void render()
{
    preFrz = 0;
    if(!preFrz)
        cuda_pbo_resource = cuda_pbo_resource0;
    else if(pipSize == 0)
    {
        cuda_pbo_resource = cuda_pbo_resource1;
        gridSz = gridPip1;
    }
    else if(pipSize == 1)
    {
        cuda_pbo_resource = cuda_pbo_resource2;
        gridSz = gridPip2;
    }

    // map PBO to get CUDA device pointer
    g_GraphicsMapFlag++;
    checkCudaErrors(cudaGraphicsMapResources(1, &cuda_pbo_resource, 0));
    size_t num_bytes;
    checkCudaErrors(cudaGraphicsResourceGetMappedPointer((void **)&d_output, &num_bytes, cuda_pbo_resource));
    //printf("CUDA mapped PBO: May access %ld bytes\n", num_bytes);

    // call CUDA kernel, writing results to PBO
    if(!preFrz)
        render_kernel(gridSize, blockSize, d_output, width, height, w, imSize, mask, zf, contrast, gam, chroma, rGain, bGain, bright);
    else
        render_kernel(gridSz, blockSize, d_output, pipwid, piphei, w, pipSize, mask, zf, contrast, gam, chroma, rGain, bGain, bright);

    getLastCudaError("render_kernel failed");

    if (g_GraphicsMapFlag)
    {
        checkCudaErrors(cudaGraphicsUnmapResources(1, &cuda_pbo_resource, 0));
        g_GraphicsMapFlag--;
    }
}

// display results using OpenGL (called by GLUT)
void display()
{
    sdkStopTimer(&timer);
    computeFPS();
    sdkStartTimer(&timer);

    if(pipSize == 0)
    {
        pipwid = swidth;
        piphei = sheight;
    }
    else
    {
        pipwid = swidth1;
        piphei = sheight1;
    }

    render();
    // display results
    glClear(GL_COLOR_BUFFER_BIT);

    // draw image from PBO
    glDisable(GL_DEPTH_TEST);
    glRasterPos2i(0, 0);

    glViewport(0, 0, width, height);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, pbo[0]);
    glDrawPixels(width, height, GL_RGBA, GL_UNSIGNED_BYTE, 0);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, 0);

    glEnable(GL_SCISSOR_TEST);
    glScissor(0, 0, 480, 1080);
    glViewport(0, 0, 480, 1080);
    glClear(GL_COLOR_BUFFER_BIT);
    showInfo();

    if(preFrz)
    {
        glEnable(GL_SCISSOR_TEST);
        glScissor(width - offwidth, height - offheight, pipwid, piphei);
        glViewport(width - offwidth, height - offheight, pipwid, piphei);
        glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, pbo[pipSize+1]);
        glDrawPixels(pipwid, piphei, GL_RGBA, GL_UNSIGNED_BYTE, 0);
        glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, 0);
    }
    else
        glDisable(GL_SCISSOR_TEST);
//    glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, 0);

//    showInfo();

    glutSwapBuffers();
    glutReportErrors();

//    sdkStopTimer(&timer);
//    computeFPS();
}

void idle()
{
    if (animate)
    {
        w += 1;
        glutPostRedisplay();
    }
}

void keyboard(unsigned char key, int x, int y)
{
    switch (key)
    {
        case 27:
            glutDestroyWindow(glutGetWindow());
            return;

        case '+':
            w += 1;
            break;

        case '-':
            w -= 1;
            break;

        case ' ':
            animate = !animate;
            break;

        default:
            break;
    }

    glutPostRedisplay();
}

void cleanup()
{
    sdkDeleteTimer(&timer);
    sdkDeleteTimer(&timer1);

    // add extra check to unmap the resource before unregistering it
    if (g_GraphicsMapFlag)
    {
        checkCudaErrors(cudaGraphicsUnmapResources(1, &cuda_pbo_resource0, 0));
        checkCudaErrors(cudaGraphicsUnmapResources(1, &cuda_pbo_resource1, 0));
        checkCudaErrors(cudaGraphicsUnmapResources(1, &cuda_pbo_resource2, 0));
        g_GraphicsMapFlag--;
    }

    // unregister this buffer object from CUDA C
    checkCudaErrors(cudaGraphicsUnregisterResource(cuda_pbo_resource0));
    checkCudaErrors(cudaGraphicsUnregisterResource(cuda_pbo_resource1));
    checkCudaErrors(cudaGraphicsUnregisterResource(cuda_pbo_resource2));
    glDeleteBuffers(3, pbo);
}

void initGLBuffers()
{
    // create pixel buffer object
    glGenBuffers(3, pbo);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, pbo[0]);
    glBufferData(GL_PIXEL_UNPACK_BUFFER_ARB, width*height*sizeof(GLubyte)*4, 0, GL_STREAM_DRAW_ARB);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, pbo[1]);
    glBufferData(GL_PIXEL_UNPACK_BUFFER_ARB, swidth*sheight*sizeof(GLubyte)*4, 0, GL_STREAM_DRAW_ARB);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, pbo[2]);
    glBufferData(GL_PIXEL_UNPACK_BUFFER_ARB, swidth1*sheight1*sizeof(GLubyte)*4, 0, GL_STREAM_DRAW_ARB);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, 0);

    // register this buffer object with CUDA
    checkCudaErrors(cudaGraphicsGLRegisterBuffer(&cuda_pbo_resource0, pbo[0], cudaGraphicsMapFlagsWriteDiscard));
    checkCudaErrors(cudaGraphicsGLRegisterBuffer(&cuda_pbo_resource1, pbo[1], cudaGraphicsMapFlagsWriteDiscard));
    checkCudaErrors(cudaGraphicsGLRegisterBuffer(&cuda_pbo_resource2, pbo[2], cudaGraphicsMapFlagsWriteDiscard));
}

void gpu_qrend::initGL(int *argc, char **argv)
{
    // initialize GLUT callback functions
    glutInit(argc, argv);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
    glutInitWindowSize(width, height);
    glutCreateWindow("CUDA 3D texture");
    glutFullScreen();
    glutDisplayFunc(display);
    glutKeyboardFunc(keyboard);
    glutReshapeFunc(reshape);
    glutIdleFunc(idle);

    if (!isGLVersionSupported(2,0) || !areGLExtensionsSupported("GL_ARB_pixel_buffer_object"))
    {
        fprintf(stderr, "Required OpenGL extensions are missing.");
        exit(EXIT_FAILURE);
    }
}
//uchar *hBuf;
void gpu_qrend::loadVolumeData()
{
    initCuda(volumeSize, (ushort*)rgba.data);
    sdkCreateTimer(&timer);
    sdkCreateTimer(&timer1);
}

void gpu_qrend::saveImg()
{
    QTime time = QTime::currentTime();
    QString current_time = time.toString("hhmmss");
    QDate date = QDate::currentDate();
    QString current_date = date.toString("yyyyMMdd");

    fnf = "Img_" + current_date.toStdString() + current_time.toStdString();

    fnf = "Raw"+fnf;
    screenshot_ppm(fnf.c_str(),1920,1080,&pixels);

//    screenshot_png(fnf.c_str(), 1920, 1080, &pixels, &png_bytes, &png_rows);

//    pressCount = pressCount + 1;

//    if(pressCount%2==1)
//    {
////        outputVideo.open( "video.avi", CV_FOURCC('M', 'P', 'E', 'G'), 30, cv::Size( width, height ), true);

//        cout<<"GO1"<<endl;
//        wrtObj->ffmpeg_encoder_start("tmp.mpg", 1, 30, width, height);

//    }
//    else
//    {
////        outputVideo.release();
//        cout<<"GO2"<<endl;
//        wrtObj->ffmpeg_encoder_finish();
////        free(rgb);
    //    }
}

void gpu_qrend::wb_apply()
{
    ifstream fileread;
    fileread.open ("CATConfig.txt");
    fileread>>wb[0]>>wb[1]>>wb[2]>>wb[3]>>wb[4]>>wb[5]>>wb[6]>>wb[7]>>wb[8];
    fileread.close();
    wbCpy(wb);
}

void gpu_qrend::gpuCpy(cv::Mat rgb3, bool ispip)
{
//    for (uint h = 0; h < 65535; h++)
//        for (uint g = 0; g < 65535; g++)
//        for (uint f = 0; f < 200; f++)
//        for (uint e = 0; e < 65535; e++);
    sdkStopTimer(&timer1);
    computeFPS1();
    sdkStartTimer(&timer1);
    cv::cvtColor(rgb3, rgba, CV_RGB2RGBA, 4);
    cv::flip(rgba,rgba,0);
//    cv::flip(rgb3,rgba,0);
//    cout<<"herher"<<endl;
    cudaCpy(rgba.data, volumeSize, hBufNo);
    glutPostRedisplay();
    glutMainLoopEvent();
    qDebug()<<"GPUhit";
    //    free(hBuf);
    hBufNo = (hBufNo + 1) % 5;

//    QCoreApplication::processEvents();

//    if(outputVideo.isOpened())
//    {
//        glReadPixels(0, 0, width, height, GL_RGB, GL_UNSIGNED_BYTE, vpixels.data );
//        for( int y=0; y<height; y++ ) for( int x=0; x<width; x++ )
//        {
//            cv_pixels.at<cv::Vec3b>(y,x)[2] = vpixels.at<cv::Vec3b>(height-y-1,x)[0];
//            cv_pixels.at<cv::Vec3b>(y,x)[1] = vpixels.at<cv::Vec3b>(height-y-1,x)[1];
//            cv_pixels.at<cv::Vec3b>(y,x)[0] = vpixels.at<cv::Vec3b>(height-y-1,x)[2];
//        }
//        outputVideo << cv_pixels;
//    }
//    cout<<"pressCount = " <<pressCount<<endl;
//    if(pressCount%2==1)
//    {
////        writevid(1920,1080);
//        ffmpeg_encoder_glread_rgb(&rgb, &pixels, width, height);
//    }
//    sdkStopTimer(&timer1);
    //    computeFPS1();
}

void gpu_qrend::hotplugHit()
{
    cout<<"HOTPLUG HIT"<<endl;
//    imshow("dd",hotplug_image);
    gpuCpy(hotplug_image,0);
}

void gpu_qrend::setPheight(const uint &value)
{
    offheight = value;
}

void gpu_qrend::setPwidth(const uint &value)
{
    offwidth = value;
}

void gpu_qrend::setPrFrz(bool value)
{
    preFrz = value;
}

void gpu_qrend::setImSz(const uint &value)
{
    imSize = value;
}

void gpu_qrend::setPipSz(const uint &value)
{
    pipSize = value;
    cout<<pipSize<<endl;
}

void gpu_qrend::setMaskSz(const uint &value)
{
    mask = value;
}

void gpu_qrend::setZoomVal(float value)
{
    zf = value;
}

void gpu_qrend::setGcontrast(float value)
{
    contrast = value;
}

void gpu_qrend::setGgam(float value)
{
    gam = value;
}

void gpu_qrend::setGchroma(float value)
{
    chroma = value;
}

void gpu_qrend::setGrGain(float value)
{
    rGain = value;
}

void gpu_qrend::setGbGain(float value)
{
    bGain = value;
}

void gpu_qrend::setGbright(float value)
{
    bright = value;
}

void gpu_qrend::setHotHit(const uint &value)
{
    hotHit = value;
    hotplugHit();
}

gpu_qrend::gpu_qrend(QObject *parent) : QObject(parent)
{
    wrtObj = new ImgFile_wrt;
    rgba.create(960,720,CV_16UC4);
    connect(this,SIGNAL(writeppm(const char*,uint,uint,unsigned char**)),wrtObj,SLOT(ppmwrite(const char*,uint,uint,unsigned char**)),Qt::QueuedConnection);
    connect(this,SIGNAL(writepng(const char*,uint,uint,GLubyte **, png_byte **, png_byte ***)),wrtObj,SLOT(pngwrite(const char*,uint,uint,GLubyte **, png_byte **, png_byte ***)),Qt::QueuedConnection);
    connect(this,SIGNAL(ffmpeg_encoder_encode_frame(uint8_t*,uint,uint)),wrtObj,SLOT(vidwrite(uint8_t*,uint,uint)),Qt::QueuedConnection);
    connect(this,SIGNAL(writevid(uint8_t**,GLubyte**,uint,uint)),wrtObj,SLOT(vidwrite(uint8_t**,GLubyte**,uint,uint)),Qt::QueuedConnection);
    int arg = 1;
    std::string astr = "/home/htic/Desktop/GPU/endoscopy";//QDir::currentPath().toStdString();
    cout<<astr<<endl;
    initGL(&arg, (char **)&astr);
    findCudaDevice(arg, (const char **)&astr);
    // OpenGL buffers
    initGLBuffers();
    loadVolumeData();
    glutCloseFunc(cleanup);

//    glutMainLoop();
    workerThread = new QThread;
    moveToThread(workerThread);
    workerThread->start();
}
