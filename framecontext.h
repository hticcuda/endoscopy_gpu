#ifndef FRAMECONTEXT_H
#define FRAMECONTEXT_H

#include <QObject>
#include <QMutex>
#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/opengl.hpp>
#include "messagetypes.h"
#include "postprocessing.h"
//#include "form.h"
#include "QDir"
#include <vector>
#include <iostream>
#include <algorithm>
#include <QReadWriteLock>
#include "gpu_qrend.h"

class FrameContext : public QObject
{
    Q_OBJECT



    QThread * workerThread;
    Postprocessing * postproc;
    gpu_qrend * rend;
//    Form * form;
    cv::Mat Y, CB, CR, rgb, Ytemp, rgbG;
    cv::Mat R8,G8,B8;
    cv::Mat R,G,B;
    int fl = 0;
    cv::Mat Y16,CB16,CR16;
    cv::Mat arr_in [3], arrG [3];

//    cv::Mat Cbmat;
//    cv::Mat Crmat;
    unsigned short lastsequencenum;
    float zoom_value = 0;
    int mask = 2;
    bool freeze_flag = false,image_save = false,video_save=false;
    static QReadWriteLock rwlock;
    int pip_size = 1;
    cv::VideoWriter video;

    cv::Mat mask_circ,mask_oct,mask_oct1,mask_oct2,mask_oct3,mask_oct4;
    cv::Size mask_cutsiz,mask_cutsiz1;
    float saturation=0;
    int Yheight, Ywidth;
    cv::Mat Ydisp,
        RGB_freeze,Ydiv,Ydisptemp,Ydisp1   ;

    bool flipx=false,flipy=false;

        //pre-freeze related
    unsigned short buf_capacity;
    unsigned short currentbufidx;
    std::vector<cv::Mat> frame_buffer;
    std::vector<cv::Mat> Y_buffer;


    int sharpest_idx; //index into Y_buffer for currently sharpest

    float m_contrast = 1, m_brightness = 0,m_gamma=0.55;
    bool auto_brightness_set = false, auto_brightness_peak = false;
    float sharp_value=0;
    int threshold=1;
//    cv::Mat Y16;
    cv::Mat rgb32;
    cv::Mat hsv;
    cv::Mat hsv_split[3];
    cv::Mat sat_temp;    
    cv::Mat hist_R, hist_G, hist_B;
    cv::Mat hist_glare;
    std::vector<double> meanY, hist_vector, hist_cat_vector;

    cv::Mat hist_Ytemp;
    std::pair <float,float> dimFactorLed;
    //float dimFactorLed1, dimFactorLed2;
    float chromafactor = 1,R_gain = 1,B_gain = 1;
    bool white_balance_click = false;
    bool illum_click = false;
    cv::Mat rgb_save;

    void applymask(cv::Mat &input32f);

    void apply_shaprness();

    bool m_led_stepping;
    int m_new_led_setting;
    bool white_balance_available = true;

    float assess_sharpness(cv::Mat &Yimage, int ii);
    bool getFreeze_flag() const;
    void glare_detection(short sequencenum);
public:

    explicit FrameContext(gpu_qrend * rendObj,QObject *parent = nullptr);

     QString name,id;
     std::string filename,image_name;
     bool file = false/*test_flag = false*/;
//     float gam = 1;

    void setZoom_value(float value);

    void setMask(int value);

    void setFreeze_flag(bool value);

    void setPip_size(int value);

    void setImage_save(bool value);

    void setVideo_save(bool value);

    void adjust(short sequencenum);

    void setContrast(float contrast);

    void setBrightness(float brightness);

    void setGamma(float gamma);

    void setAuto_brightness_set(bool value);

    void setThreshold(int value);

    void setSharp_value(float value);

    void setSaturation(float value);

    void setChromafactor(float value);

    void setR_gain(float value);

    void setB_gain(float value);

    void setWhite_balance_click(bool value);

    void pre_freeze();

    int get_frozen(cv::Mat & output);

    void setFlipx(bool value);

    void setFlipy(bool value);

    std::vector<float> getHistY1();

    std::vector<float> getHistY2();

    std::vector<uchar> getLUT();

    void eval_mean(int lInd);

    void setLed_stepping(bool led_stepping);

    void setNew_led_setting(int new_led_setting);

    void wb_simple_calculate();
    bool getWhite_balance_click();
    std::pair<float, float> getDim_factor();
    std::vector<float> getHistR();
    std::vector<float> getHistG();
    std::vector<float> getHistB();
    void eval_hist(int ledvalue);
    bool get_buffer_image(int imgidx, cv::Mat &output);
    void setIllum_click(bool value);
    bool getWhite_balance_available();
    void setWhite_balance_available(bool value);

signals:
    void rgb_ready(cv::Mat rgb, bool ispip);
    void freeze_completed(int);
    void wbMsgReady();
    void lutReady();
    void histReady(int);
    void hist_rgbReady();


public slots:
    void ycbcr2rgb(short sequencenum);//const cv::Mat &Y, const cv::Mat &CB1, const cv::Mat &CR1); //cv::Mat &rgb,

    void receive_ycbcr(YCbCr_container, short sequencenum);
private slots:
    void buffer_append(cv::Mat &Y, cv::Mat &rgb);

    void image_saturation();
};

#endif // FRAMECONTEXT_H
