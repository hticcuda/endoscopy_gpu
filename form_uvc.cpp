#include "form_uvc.h"
#include "ui_form_uvc.h"
#include "iostream"
#include <QFile>
#include <QTextStream>
#include <QDir>
#include <QShortcut>

Form_uvc::Form_uvc(UvcCommunication *uvcobj,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Form_uvc)
{
    ui->setupUi(this);
    uvc=uvcobj;

    ui->rg_slider->setRange(0,63);
    ui->shd_slider->setRange(0,63);
    ui->shd_width_slider->setRange(0,255);
    ui->shp_slider->setRange(0,63);
    ui->h1_slider->setRange(0,63);
    ui->h2_slider->setRange(0,63);


    QString user_settings = QDir::currentPath()+"/calib.bin";
    if (QFileInfo::exists(user_settings)){
        QFile file(user_settings);
        file.open(QIODevice::ReadOnly | QIODevice::Text);
        QDataStream in_all(&file);
        in_all>>calib;
        file.close();
    }
//    std::cout<<"h1111"<<calib.h1<<std::endl;

    initialize_calibration();
    new QShortcut(QKeySequence(Qt::Key_Q), this, SLOT(on_close_calib_ui_clicked()));

}

Form_uvc::~Form_uvc()
{
//    std::cout<<"the calibration page closed"<<std::endl;
    delete ui;
}


void Form_uvc::initialize_calibration() {
//    uvc->send_calib(RG,calib.rg);
//    uvc->send_calib(SHD,calib.shd);
//    uvc->send_calib(SHD_WIDTH,calib.shd_width);
//    uvc->send_calib(SHP,calib.shp);
//    uvc->send_calib(H1,calib.h1);
//    uvc->send_calib(H2,calib.h2);

    ui->rg_slider->setValue(calib.rg);
    on_rg_slider_valueChanged(calib.rg);
    ui->shd_slider->setValue(calib.shd);
    on_shd_slider_valueChanged(calib.shd);
    ui->shd_width_slider->setValue(calib.shd_width);
    on_shd_width_slider_valueChanged(calib.shd_width);
    ui->shp_slider->setValue(calib.shp);
    on_shp_slider_valueChanged(calib.shp);
    ui->h1_slider->setValue(calib.h1);
    on_h1_slider_valueChanged(calib.h1);
    ui->h2_slider->setValue(calib.h2);
    on_h2_slider_valueChanged(calib.h2);
}


void Form_uvc::on_reset_calibration_clicked()
{
    calib.reset();
}

void Form_uvc::on_h1_slider_valueChanged(int value)
{
   uvc->send_calib(H1,value);
//   std::cout<<"i am here"<<value<<std::endl;
    ui->h1_slider_label->setNum(value);
    calib.h1 = value;
}

void Form_uvc::on_h2_slider_valueChanged(int value)
{
    uvc->send_calib(H2,value);
    ui->h2_slider_label->setNum(value);
    calib.h2 = value;
}

void Form_uvc::on_shp_slider_valueChanged(int value)
{
    uvc->send_calib(SHP,value);
    ui->shp_slider_label->setNum(value);
    calib.shp = value;
}

void Form_uvc::on_rg_slider_valueChanged(int value)
{
    uvc->send_calib(RG,value);
    ui->rg_slider_label->setNum(value);
    calib.rg = value;}

void Form_uvc::on_shd_slider_valueChanged(int value)
{
    uvc->send_calib(SHD,value);
    ui->shd_slider_label->setNum(value);
    calib.shd = value;
}

void Form_uvc::on_shd_width_slider_valueChanged(int value)
{
    uvc->send_calib(THRESHOLD,value);
    ui->shd_width_label->setNum(value);
    calib.shd_width = value;
//     emit calib_changed(calib);
}

void Form_uvc::on_close_calib_ui_clicked()
{

   deleteLater();
   close();
}

void Form_uvc::on_save_calib_ui_clicked()
{
    QString preference = QDir::currentPath()+"/calib.bin";
    {
        QFile file(preference);
        file.open(QIODevice::WriteOnly | QIODevice::Text);
        QDataStream out(&file);
        out<<calib;
        file.close();
    }

}
