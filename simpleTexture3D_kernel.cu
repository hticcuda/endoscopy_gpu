/*
 * Copyright 1993-2015 NVIDIA Corporation.  All rights reserved.
 *
 * Please refer to the NVIDIA end user license agreement (EULA) associated
 * with this source code for terms and conditions that govern your use of
 * this software. Any use, reproduction, disclosure, or distribution of
 * this software and related documentation outside the terms of the EULA
 * is strictly prohibited.
 *
 */

#ifndef _SIMPLETEXTURE3D_KERNEL_CU_
#define _SIMPLETEXTURE3D_KERNEL_CU_


#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <iostream>

#include <helper_cuda.h>
#include <helper_math.h>

#define YCbCr2RGB 0

typedef unsigned int  uint;
typedef unsigned char uchar;
#if YCbCr2RGB == 0
typedef uchar4 typeImg;
#else
typedef ushort4 typeImg;
#endif

//cudaStream_t stream1;

texture<typeImg, 3, cudaReadModeNormalizedFloat> tex;  // 3D texture

static cudaArray *d_volumeArray;// = 0;

static float *dKer, *dWb;
static float hKer[9] = {0, -1, 0, -1, 4, -1, 0, -1, 0};
static float hWb[9] = {0.986552f, -0.0655866f, 0.00208309f, 0.0034502f, 1.03821f, 3.79811e-05f, -0.00266897f, -0.0175958f, 0.897628f};//{1, 0, 0, 0, 1, 0, 0, 0, 1};

__global__ void
proc(uchar *img)
{
    uint x = __umul24(blockIdx.x, blockDim.x) + threadIdx.x;
    uint y = __umul24(blockIdx.y, blockDim.y) + threadIdx.y;
    uint i = y*960*4 + x;
    img[i] = img[i] * 1;
}

__device__ int rgbToInt(float y, float cb, float cr, float ydiv, float *wBal, float contrast, float gam, float chroma, float rGain, float bGain, float bright)
{
    float r, g, b, yd/*, chroma = 1*/;
#if YCbCr2RGB == 1
    yd = (((powf(y/(64*255),gam)*245*contrast)+bright)*64)/(y+10);
//    y = clamp(y, 0.0f, 245.0f);
//    cb = (powf(cb/255,gam)*245*contrast)+bright;
//    cr = (powf(cr/255,gam)*245*contrast)+bright;
//    ydiv = 1;
    r = (y + chroma*(1.13983f * cr))*rGain*yd/64;
    g = (y + chroma*(-0.39465f * cb - 0.58060f * cr))*yd/64;
    b =
            (y + chroma*(2.03211f * cb - 0.506f * cr))*bGain*yd/64;
#else
    r = y;    g = cb;    b = cr;
#endif

//    __syncthreads();

    r = wBal[0] * r + wBal[1] * g + wBal[2] * b;
    g = wBal[3] * r + wBal[4] * g + wBal[5] * b;
    b = wBal[6] * r + wBal[7] * g + wBal[8] * b;

    r = clamp(r, 0.0f, 255.0f);
    g = clamp(g, 0.0f, 255.0f);
    b = clamp(b, 0.0f, 255.0f);
//    w = clamp(w, 0.0f, 255.0f);
    return /*(int(w)<<24) |*/ (int(b)<<16) | (int(g)<<8) | int(r);
}

__global__ void
d_render(uint *d_output, uint imageW, uint imageH, float w, uint f, uint g, float zf, float *dWb,
         float contrast, float gam, float chroma, float rGain, float bGain, float bright)
{
    uint x = __umul24(blockIdx.x, blockDim.x) + threadIdx.x;
    uint y = __umul24(blockIdx.y, blockDim.y) + threadIdx.y;
    uint xmin, xmax, ymin, ymax, widOff, heiOff;
    float4 voxel = make_float4(0,0,0,0);

    if(f == 0)
    {
        xmin = 0;
        xmax = 480;
        ymin = 0;
        ymax = 360;
        widOff = 0;
        heiOff = 0;
    }
    else if(f == 1)
    {
        xmin = 0;
        xmax = 720;
        ymin = 0;
        ymax = 540;
        widOff = 0;
        heiOff = 0;
    }
    else if(f == 2)
    {
        xmin = 720;
        xmax = 1680;
        ymin = 180;
        ymax = 900;
        widOff = 960;
        heiOff = 360;
    }
    else if(f == 3)
    {
        xmin = 480;
        xmax = 1920;
        ymin = 0;
        ymax = 1080;
        widOff = 480;
        heiOff = 0;
    }

    if (x >= xmin && x < xmax && y >= ymin && y < ymax /*&& (x-xmin + y-ymin > 240)*/)
    {
        float u = (x-xmin) / ((float) imageW-widOff);
        float v = (y-ymin) / ((float) imageH-heiOff);
        float ps = 10, ns = 2, rads;

        if(g == 1)
        {
            ps = 12*u+9*v;
            ns = 12*u-9*v;
        }
        else if(g == 2)
        {
            rads = 16 * (u-0.5f) * (u-0.5f) + 9 * (v-0.5f) * (v-0.5f);//16 * pow(u-0.5,2) + 9 * pow(v-0.5,2);
        }

        float zoo = 1/zf, mv = (1-zoo)/2;
        u = u * zoo + mv;v = v * zoo + mv;

        // read from 3D texture
        if((g == 2 && rads<4) || (g == 1 && ps>2 && ps<19 && ns>-7 && ns<10) || g == 0)
            voxel = tex3D(tex, u, v, w);
    }
    else
        voxel = make_float4(0,0,0,0);

    if ((x < imageW) && (y < imageH))
    {
        // write output color
        uint i = __umul24(y, imageW) + x;
        d_output[i] = rgbToInt(voxel.x*255,voxel.y*255,voxel.z*255,voxel.w*255,dWb, contrast, gam, chroma, rGain, bGain, bright);
    }
}

static void /**dPtrName,*/ *dBufName, *dBuf0, *dBuf1, *dBuf2, *dBuf3, *dBuf4;
static cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc<typeImg>();
static cudaMemcpy3DParms copyParams;// = {0};

//extern "C"
//void cpyD2H(uchar *asd)
//{
//    cudaError_t ss = cudaMemcpy(asd, dImg, sizeof(uint) * 1920 * 1080, cudaMemcpyDeviceToHost);
//    std::cout<<"cudaError "<<ss<<std::endl;
//}

extern "C"
void wbCpy(float *wb)
{
    cudaMemcpy(dWb, wb, sizeof (float) * 9, cudaMemcpyHostToDevice);
}

extern "C"
void cudaCpy(const typeImg *h_volume, cudaExtent volumeSize, int dBufNo)
{
    switch(dBufNo)
    {
        case 0:
            dBufName = dBuf0;
//            dPtrName = dBuf1;
        break;
        case 1:
            dBufName = dBuf1;
//            dPtrName = dBuf2;
        break;
        case 2:
            dBufName = dBuf2;
//            dPtrName = dBuf3;
        break;
        case 3:
            dBufName = dBuf3;
//            dPtrName = dBuf4;
        break;
        case 4:
            dBufName = dBuf4;
//            dPtrName = dBuf0;
        break;
        default:
        break;
    }

    cudaMemcpy(dBufName, h_volume, sizeof(typeImg) * volumeSize.width * volumeSize.height, cudaMemcpyHostToDevice);
//    dim3 grid(240,45);
//    dim3 block(16,16,1);
//    proc<<<grid,block>>>((uchar *)dBufName);

    copyParams.srcPtr = make_cudaPitchedPtr((void *)dBufName, volumeSize.width*sizeof(typeImg), volumeSize.width, volumeSize.height);
    checkCudaErrors(cudaMemcpy3D(&copyParams));
    checkCudaErrors(cudaBindTextureToArray(tex, d_volumeArray, channelDesc));
}


extern "C"
void initCuda(cudaExtent volumeSize, typeImg *h_volume)
{
//    cudaStreamCreate(&stream1);
//    cudaHostRegister(h_volume, sizeof(typeImg) * volumeSize.width * volumeSize.height, cudaHostRegisterPortable);
//    cudaMalloc((void**)&dImg, sizeof(uint) * 1920 * 1080);
    cudaMalloc((void**)&dBuf0, sizeof(typeImg) * volumeSize.width * volumeSize.height);
    cudaMalloc((void**)&dBuf1, sizeof(typeImg) * volumeSize.width * volumeSize.height);
    cudaMalloc((void**)&dBuf2, sizeof(typeImg) * volumeSize.width * volumeSize.height);
    cudaMalloc((void**)&dBuf3, sizeof(typeImg) * volumeSize.width * volumeSize.height);
    cudaMalloc((void**)&dBuf4, sizeof(typeImg) * volumeSize.width * volumeSize.height);

    cudaMalloc((void**)&dWb, 9 * sizeof(float));
    cudaMemcpy(dWb, hWb, sizeof (float) * 9, cudaMemcpyHostToDevice);

    cudaMalloc((void**)&dKer, 9 * sizeof(float));
    cudaMemcpy(dKer, hKer, sizeof (float) * 9, cudaMemcpyHostToDevice);

    // create 3D array
    checkCudaErrors(cudaMalloc3DArray(&d_volumeArray, &channelDesc, volumeSize));

    copyParams.dstArray = d_volumeArray;
    copyParams.extent   = volumeSize;
    copyParams.kind     = cudaMemcpyDeviceToDevice;

    // set texture parameters
    tex.normalized = true;                      // access with normalized texture coordinates
    tex.filterMode = cudaFilterModeLinear;      // linear interpolation
    tex.addressMode[0] = cudaAddressModeWrap;   // wrap texture coordinates
    tex.addressMode[1] = cudaAddressModeWrap;
    tex.addressMode[2] = cudaAddressModeWrap;

    // bind array to 3D texture
    checkCudaErrors(cudaBindTextureToArray(tex, d_volumeArray, channelDesc));
}

extern "C"
void render_kernel(dim3 gridSize, dim3 blockSize, uint *d_output, uint imageW, uint imageH, float w, uint f, uint g, float zf,
                   float contrast, float gam, float chroma, float rGain, float bGain, float bright)
{
    d_render<<<gridSize, blockSize>>>(d_output, imageW, imageH, w, f, g, zf, dWb, contrast, gam, chroma, rGain, bGain, bright);
//    dim3 grid(80,45);
//    dim3 block(16,16,1);
//    d_render<<<grid, block>>>(d_output, 1280, 720, w);
}

#endif // #ifndef _SIMPLETEXTURE3D_KERNEL_CU_
