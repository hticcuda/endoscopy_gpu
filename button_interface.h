#ifndef BUTTON_INTERFACE_H
#define BUTTON_INTERFACE_H

#include <QThread>
#include <libusb-1.0/libusb.h>
#include <iostream>
#include <uvccommunication.h>

class Button_interface : public QThread
{
    libusb_device_handle *devh;
    int device_ready = 0;
    int result;
    int bytes_transferred;
    int i,a,c = 0;
    char data[4];
    unsigned char bulk_data_in[4];


    UvcCommunication *uvc;

public:
    Button_interface(UvcCommunication *uvc_obj);
     bool exit = false;

private:
     void run();
};

#endif // BUTTON_INTERFACE_H
