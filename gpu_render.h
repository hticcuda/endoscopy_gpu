#ifndef GPU_RENDER_H
#define GPU_RENDER_H

#include <QThread>
#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>


//// includes, cuda
#include <vector_types.h>
#include <driver_functions.h>
#include <cuda_runtime.h>
#include <cuda_gl_interop.h>

//// CUDA utilities and system includes
#include <helper_cuda.h>
#include <helper_cuda_gl.h>
#include <helper_functions.h>

typedef unsigned int  uint;
typedef unsigned char uchar;

const uint height = 1080, width = 1920, imgs = 4;
//const uint height = 966, width = 1500, imgs = 4;

const cudaExtent volumeSize = make_cudaExtent(960, 720, 1);

const dim3 blockSize(16, 16, 1);
const dim3 gridSize(width / blockSize.x, height / blockSize.y);

class GPU_Render : public QThread
{
public:
    GPU_Render();
    void gpuCpy(uchar * data);
    int flag = 1;
    void saveImg();
private:
    void initGL(int *argc, char **argv);
};

#endif // GPU_RENDER_H
