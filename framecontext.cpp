﻿#include "framecontext.h"
#include "QThread"
#include "debug.h"

QReadWriteLock FrameContext::rwlock;
void FrameContext::setZoom_value(float value)
{
    zoom_value = value;
}

void FrameContext::setMask(int value)
{
    mask = value;
}

void FrameContext::setFreeze_flag(bool value)
{
    rwlock.lockForWrite();
    freeze_flag = value;
    rwlock.unlock();
}

void FrameContext::setPip_size(int value)
{
    pip_size = value;
}

void FrameContext::setImage_save(bool value)
{
    image_save = value;
}

void FrameContext::setVideo_save(bool value)
{
    video_save = value;
}




FrameContext::FrameContext(gpu_qrend * rendObj,QObject *parent) : QObject(parent)
  ,buf_capacity(30)
  ,m_led_stepping(false)
{
    lastsequencenum = 0;
    sharpest_idx = -1;

    postproc = new Postprocessing;
    rend = rendObj;

    //form = new Form;

    currentbufidx = 0;
    frame_buffer.resize(buf_capacity);
    Y_buffer.resize(buf_capacity);

//    video = cv::VideoWriter("outcpp.avi",CV_FOURCC('D', 'I', 'V', 'X'),10, cv::Size(960,720),true);


    mask_circ = cv::imread("./masks/circ480.png",0);
    mask_circ.convertTo(mask_circ,CV_32FC1,1/255.);
    mask_oct = cv::imread("./masks/diagmask120.png",0);
    mask_oct.convertTo(mask_oct,CV_32FC1,1/255.);
    if(mask_oct.rows==0 || mask_oct.cols==0)
        mask_oct = cv::Mat::ones(120,120,CV_32F);
    else
//        std::cout<<mask_oct.size<<std::endl;

    mask_cutsiz = cv::Size(162,330);
    mask_cutsiz1 = cv::Size(120,120);

    workerThread = new QThread;
    moveToThread(workerThread);
    workerThread->start();

}

std::vector<float> FrameContext::getHistY1()
{
    return postproc->getHistY1();
}

std::vector<float> FrameContext::getHistY2()
{
    return postproc->getHistY2();
}

std::vector<uchar> FrameContext::getLUT()
{
    return postproc->getLUT();
}

std::vector<float> FrameContext::getHistR()
{
    return hist_R;
}
std::vector<float> FrameContext::getHistG()
{
    return hist_G;
}
std::vector<float> FrameContext::getHistB()
{
    return hist_B;
}



void FrameContext::setContrast(float contrast)
{
    m_contrast = contrast;
}

void FrameContext::setBrightness(float brightness)
{
    m_brightness = brightness;
}

void FrameContext::setGamma(float gamma)
{
    m_gamma = gamma;
}

void FrameContext::setAuto_brightness_set(bool value)
{
    auto_brightness_set = value;
}

void FrameContext::setThreshold(int value)
{
    threshold = value;
}

void FrameContext::setSharp_value(float value)
{
    sharp_value = value;

}

void FrameContext::setSaturation(float value)
{
    saturation = value;
}

void FrameContext::setChromafactor(float value)
{
    chromafactor = value;
}

void FrameContext::setR_gain(float value)
{
    R_gain = value;
}

void FrameContext::setB_gain(float value)
{
    B_gain = value;
}

void FrameContext::setWhite_balance_click(bool value)
{
    white_balance_click = value;
}

void FrameContext::setIllum_click(bool value)
{
    illum_click = value;
}

bool FrameContext::getWhite_balance_click()
{
    return white_balance_click;
}

std::pair<float,float> FrameContext::getDim_factor()
{
    return dimFactorLed;
}

//void FrameContext::pre_freeze()
//{
//    sharpest_idx = 3;
//}

float FrameContext::assess_sharpness(cv::Mat & Yimage,int ii)
{
    cv::Mat lapimage, Ydown;
    cv::pyrDown(Yimage,Ydown);
    cv::pyrDown(Ydown,Ydown);
    cv::Laplacian(Ydown,lapimage,CV_8UC1);
    cv::Scalar meanv,stdv;
#if dbg_write_img == 1
    char fname[15];
    sprintf(fname,"freezeim%d.png",ii);
    cv::imwrite(fname,Ydown);
    sprintf(fname,"RGBfreezeim%d.png",ii);
    cv::imwrite(fname,frame_buffer[ii]);
#endif
    cv::meanStdDev(lapimage,meanv,stdv);
    return stdv.val[0];//meanv.val[0];
}


void FrameContext::pre_freeze()
{   // this is running from a different thread

    setFreeze_flag(true);
    int bufsiz = Y_buffer.size();
    std::vector<float> sv(bufsiz);
    sv.assign(bufsiz,-1);

    float maxsharpness = 0;
    int maxidx = -1;
    for(int ii=0; ii < bufsiz; ++ii) {
        sv[ii] = assess_sharpness(Y_buffer[ii], ii);
//        std::cerr<<"sharpeness of frame is " << sv[ii] << std::endl;

        if(sv[ii]>maxsharpness || ii == 0) {
            maxsharpness = sv[ii];
            maxidx = ii;
        }

    }
    if(maxidx!=-1)
        sharpest_idx = maxidx;
    else
        sharpest_idx = bufsiz-1;    //FIXME: could not find any suitable sharp frame
    std::cerr<<"sharpest frame is " << sharpest_idx << std::endl;
    emit freeze_completed(sharpest_idx);

}

//int FrameContext::get_frozen(cv::Mat & output) {
//    int sharpest = sharpest_idx;
//    if(sharpest!=-1)
//    {
//        sharpest_idx = -1;
//        frame_buffer[sharpest].copyTo(output);
//    }
//    return sharpest;
//}

bool FrameContext::get_buffer_image(int imgidx, cv::Mat & output) {
    if(imgidx>=0 && imgidx<frame_buffer.size()) {
        frame_buffer[imgidx].copyTo(output);
        return true;
    }
    else {
        //??
        return false;
    }
}

void FrameContext::setFlipx(bool value)
{
    flipx = value;
}

void FrameContext::setFlipy(bool value)
{
    flipy = value;
}

bool FrameContext::getFreeze_flag() const
{
    bool ff;
    rwlock.lockForRead();
    ff = freeze_flag;
    rwlock.unlock();
    return ff;
}

void FrameContext::applymask(cv::Mat & input32f) {


    if(mask == 1) //circular
    {
        int maskwid = mask_cutsiz.width,
                maskhei = mask_cutsiz.height;

        cv::Rect tlrect(0,0,maskwid,maskhei),
                blrect(0,720-maskhei,maskwid,maskhei),
                trrect(960-maskwid,0,maskwid,maskhei),
                brrect(960-maskwid,720-maskhei,maskwid,maskhei);

        cv::multiply(input32f(tlrect),mask_circ(tlrect),input32f(tlrect));
        cv::multiply(input32f(trrect),mask_circ(trrect),input32f(trrect));
        cv::multiply(input32f(blrect),mask_circ(blrect),input32f(blrect));
        cv::multiply(input32f(brrect),mask_circ(brrect),input32f(brrect));

    }
    else if(mask == 2)
    {
        int maskwid = mask_cutsiz1.width,
                maskhei = mask_cutsiz1.height;

        cv::Rect tlrect(0,0,maskwid,maskhei),
                blrect(0,720-maskhei,maskwid,maskhei),
                trrect(960-maskwid,0,maskwid,maskhei),
                brrect(960-maskwid,720-maskhei,maskwid,maskhei);


        cv::flip(mask_oct,mask_oct1,-1);
        cv::flip(mask_oct1,mask_oct1,0);
        cv::multiply(input32f(tlrect),mask_oct1,input32f(tlrect));
        cv::flip(mask_oct1,mask_oct2,0);
        cv::multiply(input32f(blrect),mask_oct2,input32f(blrect));
        cv::flip(mask_oct1,mask_oct3,1);
        cv::multiply(input32f(trrect),mask_oct3,input32f(trrect));
        cv::flip(mask_oct1,mask_oct4,-1);
        cv::multiply(input32f(brrect),mask_oct4,input32f(brrect));
    }

}

#include <stdio.h>

void savemat(cv::Mat D, std::string fname, size_t dtypesiz)
{
    FILE* FP = fopen(fname.c_str(),"wb");
        int sizeImg[2] = { D.cols , D.rows };
        fwrite(sizeImg, 2, sizeof(int), FP);
        fwrite(D.data, size_t(D.cols * D.rows), dtypesiz, FP);
        fclose(FP);
        std::cerr<<"written";
}
void FrameContext::ycbcr2rgb(short sequencenum) //const cv::Mat & Y, const cv::Mat & CB1, const cv::Mat & CR1)
{
#if RGB_conversion == 1
    cv::Mat arr_in []  = {R,G,B};
    double cb_scale[] ={0,-0.39465,2.03211};
    double cr_scale[] ={-1.13983,0.58060,0.506};
    cv::Mat arr_8[]  = {R8,G8,B8};
    //cv::Mat arr[]  = {R8,G8,B8};
    double arr_gain[] = {R_gain,1.0f,B_gain};
#if RGB_DISPLAY==1
//    range = cv::Range(0,3)

    cv::parallel_for_(cv::Range(0,3),[&](const cv::Range & range)
    {
        for(int r = range.start; r < range.end; r++)
        {
            arr_in[r] = (Y + chromafactor*(cb_scale[r] * CB - cr_scale[r] * CR))*arr_gain[r];
            cv::multiply(arr_in[r],Ydiv,arr_in[r]);
            arr_in[r].convertTo(arr_8[r],CV_8UC1,1/64.);

//            if(r==0) {
//                R = Ytemp + chromafactor*(1.13983 * CR);
//                //R = Y + chromafactor*(1.13983 * CR);
//                //cv::multiply(R,Ydiv,R,R_gain);
//                R.convertTo(R8,CV_8UC1,1/64.);
//            }
//            if(r==1){
//                G = Ytemp + chromafactor*(-0.39465 * CB - 0.58060 * CR);
//                //G = Y + chromafactor*(-0.39465 * CB - 0.58060 * CR);
//                //cv::multiply(G,Ydiv,G);
//                G.convertTo(G8,CV_8UC1,1/64.);
//            }
//            if(r==2){
//                B = Ytemp + chromafactor*(2.03211 * CB -0.506 * CR);
//                //B = Y + chromafactor*(2.03211 * CB -0.506 * CR);
//                //cv::multiply(B,Ydiv,B,B_gain);
//                B.convertTo(B8,CV_8UC1,1/64.);
//            }
        }
    },3);

//    for(int r = 0; r < 3; r++)
//    {
//        arr_in[r] = (Ytemp + chromafactor*(cb_scale[r] * CB - cr_scale[r] * CR))*arr_gain[r];
//        cv::multiply(arr_in[r],Ydiv,arr_in[r]);
//        arr_in[r].convertTo(arr_8[r],CV_8UC1,1/64.);
//    }
    R8 = arr_8[0];G8 = arr_8[1];B8 = arr_8[2];
    R = arr_in[0];G = arr_in[1];B = arr_in[2];
    cv::Mat arr []  = {arr_8[0],arr_8[1],arr_8[2]};

#else
    cv::Mat Y8;
    Ytemp.convertTo(Y8,CV_8UC1,1/64.f);
    cv::Mat arr [] = {Y8,Y8,Y8};

#endif
    cv::merge(arr,3,rgb);
//#endif
#else
//    cv::Mat Y16,CB16,CR16,Ydiv16;
    arr_in [0]  = Y; arr_in [1]  = CB; arr_in [2]  = CR;
    arrG [0]  = Y16; arrG [1]  = CB16; arrG [2]  = CR16;
//    arrG []  = {Y16,CB16,CR16};
    cv::parallel_for_(cv::Range(0,3),[&](const cv::Range & range)
    {
        for(int r = range.start; r < range.end; r++)
        {
            arr_in[r].convertTo(arrG[r],CV_16UC1);
        }
    },3);
//    Y.convertTo(Y16,CV_16UC1);
//    cv::imshow("Y",Y16);
//    CB.convertTo(CB16,CV_16UC1);
//    CR.convertTo(CR16,CV_16UC1);
//    Ydiv.convertTo(Ydiv16,CV_16UC1);
//    cv::Mat arrG []  = {Y8,CB8,CR8,Ydiv8};
//    cv::merge(arrG,4,rgbG);
//    cv::Mat arrG []  = {Y16,CB16,CR16,Ydiv16};
//    cv::merge(arrG,4,rgbG);
    cv::merge(arrG,3,rgbG);
#endif
}



void FrameContext::adjust(short sequencenum)
{
    //use 8 bit version to adjust (auto) contrast, brightness and gamma

//    Ydisp1.create(cv::Size(960,720),CV_8UC1);
//    cv::resize(Y,Ydisp1,Ydisp1.size());
    Y.convertTo(Ydisp,CV_8UC1,1/64.);

//    Ytemp.create(Ydisp.size(),Y.type());
//     Ydiv.create(Ydisp.size(),CV_32FC1);
//     Ytemp.convertTo(Ydisp,Ydisp.type(),1/64.);
//     postproc->update_hist(Ydisp(cv::Rect(320,240, 320, 240)),sequencenum);
   postproc->update_hist(Ydisp,sequencenum);
#if chart_Y_histogram == 1
     emit histReady(2);
#endif

     postproc->hist_find_limits();
     postproc->update_lut_auto_bright(m_brightness,m_contrast,m_gamma,auto_brightness_set,auto_brightness_peak);
     postproc->applyLUT(Ydisp); //in place update

#if enable_chart == 1
        if(sequencenum%7 == 0)
        {
            emit lutReady();
        }
        if(sequencenum%3 == 0)
        {
           postproc->update_hist2(Ydisp(cv::Rect(320,240, 320, 240)));
            emit histReady(2);
        }
#endif

}

void FrameContext::apply_shaprness()
{
    postproc->image_sharpening(sharp_value, threshold, Ydisp);
    postproc->getSharpened().copyTo(Ydisp);
}

bool FrameContext::getWhite_balance_available()
{
    return white_balance_available;
}

void FrameContext::setWhite_balance_available(bool value)
{
    white_balance_available = value;
}

void FrameContext::setNew_led_setting(int new_led_setting)
{
    m_new_led_setting = new_led_setting;
}

void FrameContext::setLed_stepping(bool led_stepping)
{
    m_led_stepping = led_stepping;
}

void FrameContext::eval_mean(int ledvalue){
//    meanY.create(1,10,Y.type());
    int roiw = 320;
    int roih = 240;
    cv::Rect roi[] = {cv::Rect(0,0, roiw, roih),cv::Rect(320,0, roiw, roih),cv::Rect(640,0, roiw, roih),cv::Rect(0,240, roiw, roih),cv::Rect(320,240, roiw, roih),
                     cv::Rect(640,240, roiw, roih),cv::Rect(0,480, roiw, roih),cv::Rect(320,480, roiw, roih),cv::Rect(640,480, roiw, roih)};

    int channels [] = {0};
    int histsize [] = {64};
    float yrange [] = {0,255};
    const float * ranges [] = {yrange};

//    meanY.at<double>(0,0)=(double)ledvalue;
    meanY[0]=(double)ledvalue;

    hist_vector.clear();
    std::vector<float> temp_vector;
    for(int i = 0; i < 9; i++){
        double v = cv::mean(Ydisp(roi[i])).val[0];
//        meanY.at<double>(0,i+1) = v;
        meanY[i+1]=v;
//        std::cerr << v << " ";
        cv::Mat histIn = Ydisp(roi[i]); //8192.0;
        //cv::calcHist(&histIn,1,0,cv::Mat(),hist_Ytemp,1,histsize,ranges,true,false);

        cv::calcHist(&histIn,1,channels,cv::Mat(),hist_Ytemp,1,histsize,ranges,true,false);
        //cv::normalize(hist_Ytemp,hist_Ytemp, 0,1,cv::NORM_MINMAX);
        std::cerr << "Hist values " << hist_Ytemp.at<double>(0,0) << hist_Ytemp.at<double>(0,1) << hist_Ytemp.at<double>(0,2)
                  << hist_Ytemp.at<double>(0,3) << hist_Ytemp.at<double>(0,4) << hist_Ytemp.at<double>(0,5);


        hist_Ytemp.copyTo(temp_vector);
        hist_vector.insert(hist_vector.end(), temp_vector.begin(), temp_vector.end());
//        for(int j = 0;j < hist_Ytemp.rows; j++)
//        {
//            meanY[(32*i)+9+j] = hist_Ytemp.at<double>(0,j);
//        }
    }
}


void FrameContext::glare_detection(short sequencenum){

    // function to detect glare
    int glareChannels [] = {0};
    float upper = postproc->getUpper();
    int glareHistSize [] = {int(upper)-int(upper/2)+1};
    float range1 = upper/2;
    float yrange2 [] = {range1,upper};
    const float * ranges2 [] = {yrange2};
    cv::calcHist(&Ydisp,1,glareChannels,cv::Mat(),hist_glare,1,glareHistSize,ranges2,true,false);
    cv::normalize(hist_glare,hist_glare,255,0,cv::NORM_L1);

//    cv::Mat cumHist;/*
//    cumHist.rows = 961;
//    cumHist.cols = 721;*/
//    cv::integral(hist_glare,cumHist,1);
    double minVal;
    double maxVal;
    cv::Point minLoc;
    cv::Point maxLoc;

    cv::minMaxLoc( hist_glare, &minVal, &maxVal, &minLoc, &maxLoc );
    //hist_glare = hist_glare * maxVal;
    int aa = range1+minLoc.y;
    int x1 = range1+maxLoc.y;
    int x2 = x1+1;
    for(int i = x2;i < upper; i++)
    {
        double ll = hist_glare.at<double>(i,0);
        if(ll == 0.0){
            x2 = i;
            break;
        }
    }
    float slope = upper/(x2 - x1 + 0.1); //maxVal*upper/(x2 - x1 + 0.1)
    int a = 2;

}

void FrameContext::wb_simple_calculate(){
    // gain adjustment parameters, can probably be optimized via more control system analysis
    double u = .01;  //gain step size
    double a = 8; //double step threshold
    double b = 10; //convergence threshold
    double maxDiff = 10.0;
    //static int iter = 1;
    //int maxIter = 1000;

    cv::Rect roiLed1(960/4,0, 480, 400);
    cv::Rect roiLed2(960/4,300, 480, 400);
    cv::Mat Y8 = Y/64.f;
//    dimFactorLed.first = postproc->dimmingFactor(Y8(roiLed1));
//    dimFactorLed.second = postproc->dimmingFactor(Y8(roiLed2));

    cv::Rect roi(960/2-150,720/2-150, 300, 300);

    double diff1, diff2, diff3;
    Postprocessing::findHist(R8(roi), hist_R);
    Postprocessing::findHist(G8(roi), hist_G);
    Postprocessing::findHist(B8(roi), hist_B);

    emit hist_rgbReady();

    diff1 =  cv::abs(cv::mean(R8(roi)).val[0] - cv::mean(G8(roi)).val[0]);
    diff2 =  cv::abs(cv::mean(G8(roi)).val[0] - cv::mean(B8(roi)).val[0]);
    diff3 =  cv::abs(cv::mean(B8(roi)).val[0] - cv::mean(R8(roi)).val[0]);

    if ((diff1 < maxDiff) && (diff2 < maxDiff) && (diff3 < maxDiff)){
        setWhite_balance_click(false);
        std::cerr << std::endl << "Done....  R " << cv::mean(R8(roi)).val[0] << "G " << cv::mean(G8(roi)).val[0]
                     << "B " << cv::mean(B8(roi)).val[0];
        emit wbMsgReady();
    }
    else
    {
        cv::Mat CB_temp,CR_temp, Y_temp, R_temp, G_temp, B_temp;
        //double delta2 = 127.5;
        //Y_temp  = 0.3328 * R8 + 0.587 * G8 + 0.114 * B8;

    //    cv::divide(R,Ydiv,R_temp,R_gain);
    //    cv::divide(G,Ydiv,G_temp);
    //    cv::divide(B,Ydiv,B_temp,B_gain);

        std::cerr << std::endl << "R gain2 " << R_gain << " B gain2 " << B_gain;
        CB_temp = -0.018 * R * R_gain - 0.397 * G+ 0.415 * B * B_gain;
        CR_temp = 0.5854 * R * R_gain - 0.4902 * G - 0.0952 * B * B_gain;

        //CB_temp = -0.299*R8 -0.587*G8 +0.886*B8;
        //CR_temp = 0.701*R8 -0.587*G8 -0.114*B8;


        double U_bar = cv::mean(CB_temp(roi),CB_temp>0).val[0]/64.f;
        double V_bar = cv::mean(CR_temp(roi),CB_temp>0).val[0]/64.f;
        double errU, errV, deltaU, deltaV;
        short ch;
        static double gradU = 0, gradV = 0;


        if(R_gain < 0){
            std::cerr << "U_bar " << U_bar << "V_bar " << V_bar;
        }

//        if (cv::abs(U_bar) > cv::abs(V_bar)) // U > V; blue needs adjustment
//        {
            errU = U_bar;
            gradU = U_bar - gradU;
//            ch = 3; //blue channel
//        }
//        else
//        {
            errV = V_bar;
            gradV = V_bar - gradV;
//            ch = 1; //red channel
//        }

        int signU, signV;
        if (errU>0)
            signU = +1;
        else
            signU = -1;

        if (errV>0)
            signV = +1;
        else
            signV = -1;
        //int sign = (std::signbit(err) == 0) ? 1: -1;

        if(cv::abs(errU) >= a){
            deltaU = 2 * signU * u; //accelerate gain adjustment if far off
        }
        else if (cv::abs(errU) < b) //converged
        {
            deltaU = 0;
        }
        else
            deltaU = signU * gradU*0.01;

        if(cv::abs(errV) >= a){
            deltaV = 2 * signV * u; //accelerate gain adjustment if far off
        }
        else if (cv::abs(errV) < b) //converged
        {
            deltaV = 0;
        }
        else
            deltaV = signV * gradV*0.01;

        if(deltaV != 0){
            R_gain = R_gain - (float)deltaV;
            std::cerr << " R gain " << R_gain << " V_bar " << V_bar << " B gain " << B_gain << " B_bar " << U_bar;
        }
        if(deltaU != 0){
            B_gain = B_gain - (float)deltaU;
            std::cerr << " B gain " << B_gain << " U_bar " << U_bar << " R gain " << R_gain << " V_bar " << V_bar;
        }


        std::cerr << std::endl << "Diff1 " << diff1 << "diff2 " << diff2 << "diff3 " << diff3;


        if((deltaU == 0 && deltaV == 0)){
            setWhite_balance_click(false);
            std::cerr << std::endl << "Done....  U_bar " << U_bar << "V_bar " << V_bar;
            std::cerr << std::endl << "Done....  R " << cv::mean(R8(roi)).val[0] << "G " << cv::mean(G8(roi)).val[0]
                         << "B " << cv::mean(B8(roi)).val[0];
            emit wbMsgReady();
        }
    }
}

void FrameContext::receive_ycbcr(YCbCr_container data,short sequencenum)
{

    static cv::TickMeter tm1;
    #if time_ycbcr_rgb == 1
        TIC(tm1);
    #endif
    if(lastsequencenum>0){
        short diff = sequencenum-lastsequencenum;
        if(diff>1)
            qDebug()<<"frame context receive drop: "<< sequencenum << " ";
    }

//    cv::Mat rgb;
    data.Y.copyTo(Y);
    data.CB.copyTo(CB);
    data.CR.copyTo(CR);

//    cv::Mat Ytempp;
//    Y.convertTo(Ytempp,CV_16UC1,1.0);
//    cv::imshow("Yframe",Ytempp);
//    cv::waitKey(1);

    //Y = (Y+64) * 3.0;
    static short seqq = sequencenum;
//    if(cv::abs(sequencenum-seqq) == 10){
//        Y.convertTo(Y16,CV_16UC1,1.0);
//        cv::imwrite("Images/14bit_Y.png",Y16);
//        //savemat(Y, "Images/14bit_Y.mat", size_t(Y.depth()));
//        std::cerr << "depth" << Y16.depth();
//    }

    if(illum_click)
    {
        double minVal;
        double maxVal;
        cv::Point minLoc;
        cv::Point maxLoc;
        cv::minMaxLoc( Y, &minVal, &maxVal, &minLoc, &maxLoc );

        Y = Y/maxVal;
        cv::Mat gammaY1, gammaY2;
        cv::pow(Y,0.5,gammaY1);
        cv::pow(Y,2.0,gammaY2);
        Y = 0.5*(gammaY1 + gammaY2);
        cv::normalize(Y,Y,0,1,cv::NORM_MINMAX);
        Y = Y*maxVal;
    }

    adjust(sequencenum);

//    if(white_balance_click)
//    {
//         //postproc->apply_clahe(Ydisp);
//        wb_simple_calculate();
//        //setWhite_balance_click(false);
//    }

//    cv::Mat Ytemp;
//    Ydisp.convertTo(Ytemp,CV_8UC1,1/64.);
//    cv::imshow("Yframe",Ytemp);
//    cv::waitKey(1);

    if(sharp_value != 0)
    {
        apply_shaprness();
    }

    Ydisp.convertTo(Ytemp,Y.type(),64); // 16 bit
    cv::divide(Ytemp, Y+10, Ydiv);
    Ydiv.setTo(0,Y<=10);
    applymask(Ydiv);
//    applymask(Ytemp);




#if enable_glare_detection == 1
    glare_detection(sequencenum);
#endif

    static int lInd = 0;

    if(m_led_stepping && lInd<m_new_led_setting)
    {

        lInd = m_new_led_setting;

        meanY.assign(10,0); //10 for mean plus index, (32*9) for histogram 32 bins in each tile --- so 298
        //hist_vector.assign(288,0);
        eval_mean(lInd);
        //m_led_stepping = false;
        //hist_cat_vector.assign(298,0);
        hist_cat_vector.clear();
        hist_cat_vector.insert(hist_cat_vector.end(), meanY.begin(), meanY.end());
        hist_cat_vector.insert(hist_cat_vector.end(), hist_vector.begin(), hist_vector.end());
        //eval_hist(lInd);

        try {
            std::ofstream meanfile("Y_mean_file3.csv",std::ofstream::out | std::ofstream::app);
            meanfile<< cv::format(cv::Mat(hist_cat_vector).t(), cv::Formatter::FMT_CSV) << std::endl;
            //meanfile.flush();
//            cv::print(cv::Mat(meanY));
            meanfile.close();
            std::cerr << std::endl << "mean file" << lInd;

        } catch (std::exception & ex) {
            std::cerr << ex.what();
        }
        m_new_led_setting = 0;
        if (lInd == 255){
            lInd = 0;
        }
    }

    else {
    }

    ycbcr2rgb(sequencenum); //Y,CB,CR);


    if(white_balance_click)
    {
        //wb_simple_calculate();
        postproc->wb_calculate(rgb);
        setWhite_balance_click(false);
        if(!white_balance_available)
            white_balance_available = true;
        rend->wb_apply();
    }


    if(saturation != 0)
    {
        image_saturation();
    }



//    if (white_balance_available)
//    {
//        std::cout<<"white balancce"<<std::endl;
//        postproc->wb_apply(rgb);

//    }


    if(flipy == true){
        cv::flip(rgb,rgb,0);
    }

    if(flipx == true){
        cv::flip(rgb,rgb,1);
    }


    if(!getFreeze_flag())
    {
            // if we are in no_freeze mode
        buffer_append(Ydisp,rgb);
    }


    if(zoom_value != 0)
    {
        //FIXME: why zoom?
        postproc->image_zoom(rgb,zoom_value);
    }

    if(video_save == true)
    {
        if( !QDir("Videos/"+name+"_"+id).exists())
        {
            QDir().mkdir("Videos/"+name+"_"+id);
        }

        QTime timeone = QTime::currentTime();
        QString current_timeone = timeone.toString();
        QDate date = QDate::currentDate();
        QString current_date = date.toString("dd-MM-yyyy");

        if(!video.isOpened())
             video.open("Videos/"+name.toStdString()+"_"+id.toStdString()+"/video_out_"+current_date.toStdString()+"_"+current_timeone.toStdString()+".avi",CV_FOURCC('m', 'p', 'e', 'g'),30, cv::Size(960,720),true);
        cv::cvtColor(rgb, rgb_save,  cv::COLOR_BGR2RGB);
        video.write(rgb_save);


        video.write(rgb_save);
    }

    else if(video_save == false)
    {
        video.release();
    }


    if(image_save)
    {
//        std::cout<<"i am here in the image save "<<std::endl;
        if( !QDir("Images/"+name+"_"+id).exists())
        {
            QDir().mkdir("Images/"+name+"_"+id);
        }
        cv::cvtColor(rgb, rgb_save,  cv::COLOR_BGR2RGB);
        QTime time = QTime::currentTime();
        QString current_time = time.toString();
        QDate date = QDate::currentDate();
        QString current_date = date.toString("dd-MM-yyyy");
        if(file)
        {
            cv::imwrite(filename+"/"+image_name,rgb_save);
            file = false;
        }
        else
        {
            cv::imwrite("Images/"+name.toStdString()+"_"+id.toStdString()+"/image_"+ current_date.toStdString() +current_time.toStdString() +".png",rgb_save);

        }
        image_save = false;
    }

#if RGB_conversion == 1
    emit rgb_ready(rgb, getFreeze_flag());
#else
    emit rgb_ready(rgbG, getFreeze_flag());
#endif

#if time_ycbcr_rgb == 1
    TOC(tm1,sequencenum);
    if(tm1.getCounter()%600==0) //once in 10 seconds
        qDebug() << "framecontext: " << laptime << " ";
#endif

}

void FrameContext::buffer_append(cv::Mat & Y, cv::Mat & rgb)
{
    Y.copyTo(Y_buffer[currentbufidx]);
    rgb.copyTo(frame_buffer[currentbufidx]);
    currentbufidx = (currentbufidx+1)%buf_capacity;
}

void FrameContext::image_saturation()
{
//    cv::Mat hsv,hsv_split[3],temp;
    rgb.convertTo(rgb32,CV_32FC1,1/255.0);
    cv::cvtColor(rgb32,hsv,CV_BGR2HSV);
    cv::split(hsv,hsv_split);
    sat_temp = 1+saturation*(1-hsv_split[1]);
    hsv_split[1]=hsv_split[1].mul(sat_temp);
    cv::merge(hsv_split,3,hsv);
    cv::cvtColor(hsv,rgb32,CV_HSV2BGR);
    rgb32.convertTo(rgb,CV_8UC3,255);
}
