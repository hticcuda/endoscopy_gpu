#ifndef EXTERNAL_INPUT_H
#define EXTERNAL_INPUT_H

#include <QObject>
#include "opencv2/opencv.hpp"

class External_input : public QObject
{
    Q_OBJECT

    QThread * workerThread;
    cv::VideoCapture cap;
    cv::Mat frame;


public:
    explicit External_input(QObject *parent = nullptr);

signals:

public slots:
    void show_external_input();
private slots:
//    void show_external_input();
};

#endif // EXTERNAL_INPUT_H
