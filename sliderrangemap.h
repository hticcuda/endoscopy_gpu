#ifndef SLIDERRANGEMAP_H
#define SLIDERRANGEMAP_H

#include <QPair>
#include <QSlider>


typedef QPair<float,float> SliderYRange;
typedef QPair<int, int> SliderXRange;

enum SliderParam {
    NONE=0,
    I_BRIGHTNESS,
    I_CONTRAST,
    I_GAMMA,
    S_EXPOSURE,
    I_SHARPNESS,
    I_SHARP_BGTHRESHOLD,
    I_GAIN_R,
    I_GAIN_B,
    I_SATURATION,
    I_CHROMA,
    D_FPSRATE,
    I_THRESHOLD
};

struct SliderData {
    SliderXRange xrange;
    SliderYRange yrange;
};

class SliderRangeMap
{
//     QHash<QSlider *, SliderYRange> m_map;
    QHash<SliderParam, SliderData> m_map;

public:
    SliderRangeMap();
    int add(SliderParam slidername, double miny, double maxy, double step);
    float getMappedValue(SliderParam slidername, int xvalue);
    SliderData getSliderData(SliderParam slidername)
    {
        return m_map.value(slidername);
    }

//    void add( QSlider * const slider, float miny, float maxy);
//    float getMappedValue( QSlider * const slider, int xvalue);
//    int add(QSlider * const slider, float miny, float maxy, float step);
};

#endif // SLIDERRANGEMAP_H
