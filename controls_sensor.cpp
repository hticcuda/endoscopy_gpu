#include "controls_sensor.h"

void Controls_sensor::reset() {
    brightness1=128;
    brightness2=128;
    exposure=255;
    variable_gain=191;
    blacklev=255;
}

void Controls_sensor::copyFrom(Controls_sensor & obj)
{
    this->brightness1=obj.brightness1;
    this->brightness2=obj.brightness2;
    this->exposure=obj.exposure;
    this->blacklev=obj.blacklev;
    this->variable_gain=obj.variable_gain;
    this->alc_aec_state = obj.alc_aec_state;
    this->led = obj.led;
}

QDataStream & operator << (QDataStream & out, const Controls_sensor & obj)
{
    out<<obj.brightness1 << obj.brightness2 << obj.exposure << obj.variable_gain << obj.blacklev << obj.alc_aec_state << obj.led;
    return out;
}

QDataStream & operator >> (QDataStream & in,  Controls_sensor & obj)
{
    in >> obj.brightness1
            >>obj.brightness2
            >> obj.exposure
            >> obj.variable_gain
            >> obj.blacklev
            >> obj.alc_aec_state
            >> obj.led;
    return in ;
}
