#ifndef PATIENT_INFORMATION_H
#define PATIENT_INFORMATION_H

#include <QObject>

class Patient_Information : public QObject
{
    Q_OBJECT
public:
    QString id,name,gender,dob,comments,age;
    explicit Patient_Information(QObject *parent = nullptr);

signals:

public slots:
};


QDataStream & operator << (QDataStream & st, const Patient_Information & obj);
QDataStream & operator >> (QDataStream & st, Patient_Information & obj);


#endif // PATIENT_INFORMATION_H
