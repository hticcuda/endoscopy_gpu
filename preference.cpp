#include "preference.h"
#include <QDataStream>

Preference::Preference(QObject *parent) : QObject(parent)
{

}

QDataStream & operator << (QDataStream & out, const Preference & obj)
{
    out<<obj.pip_window_size << obj.pip_window_position << obj.gain_r <<obj.gain_b << obj.enhancement_level << obj.image_preset << obj.mask;
    return out;
}

QDataStream & operator >> (QDataStream & in, Preference & obj)
{
    in >> obj.pip_window_size
            >> obj.pip_window_position
            >> obj.gain_r
            >> obj.gain_b
            >> obj.enhancement_level
            >> obj.image_preset
            >> obj.mask;

    return in;
}
