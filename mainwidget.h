#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>
//#include "uvccommunication.h"
#include "codec.h"
#include <debug.h>
#include <QTime>
#include <framecontext.h>
#include <QThread>
#include "messagetypes.h"
#include "form.h"
//#include "form_shortcut.h"
#include "external_input.h"
#include "gpu_qrend.h"

namespace Ui {
class mainwidget;
}

Q_DECLARE_METATYPE(YCbCr_container)

class mainwidget : public QWidget
{
    Q_OBJECT

    FrameContext *frame;
    External_input * ext_inp;
    bool freezeready;
    //QTimer * wb_timer;
    Form * f;
    gpu_qrend *rend;

    Form_shortcut * shortcut_form;

    cv::TickMeter tm4;
//    QTime time,old_time;
    int time_old;
//    cv::Mat CB_half,CR_half,Yframe;

#if enable_chart == 1
    QLineSeries *m_series, *n_series, *Y_pre_series;
    QScatterSeries *chart1_series;
    QStringList m_titles,chart1_tittles,y_pre_tittles;
    QValueAxis *m_axis, *chart1_axis, *y_pre_axis; // = new QValueAxis;

    QLineSeries *r_series, *g_series, *b_series;
    QValueAxis *rgb_axis; // = new QValueAxis;
    //float R_gain,B_gain;

#endif

public:
    explicit mainwidget(QWidget *parent = 0);
    ~mainwidget();

#if enable_chart == 1
    QChart *chart,
//        *chart_1,
    *chart_2;// = new QChart;
        QChart *chartRGB;
#endif

//        GPU_Render *getRend() const;
//        void setRend(GPU_Render *value);
//        void show_image();
signals:
            void clickedq(cv::Mat, bool);
        //    void ycbcr_ready(short);

private slots:
    void start_camera_streaming(int a);
    void wb_done_message();
//    void collectFieldData(short sequencenum);
    void display_mainimage(cv::Mat rgb, bool ispip);
    void sample();
    void close_ui_application();
    void display_frozen(int);
    void show_shortcut_list();


#if enable_chart == 1
    void line_chart(int map_select);
    void line_chart_lut();
    void line_chart_rgb();
#endif

private:
    Ui::mainwidget *ui;
//    void calibration_intialization();

#if enable_chart == 1
    void chart_initialize();
#endif
    void setStyleSheet();
};

#endif // MAINWIDGET_H
