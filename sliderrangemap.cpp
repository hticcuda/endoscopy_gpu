#include "sliderrangemap.h"
#include "iostream"

SliderRangeMap::SliderRangeMap()
{

}

int SliderRangeMap::add(SliderParam slidername, double miny, double maxy, double step)
{
    SliderData temp;
    temp.yrange = qMakePair(miny,maxy);
    temp.xrange = qMakePair(0,int(0.5+(maxy-miny)/step));
    m_map.insert(slidername, temp);
    return temp.xrange.second;
}

float SliderRangeMap::getMappedValue(SliderParam slidername, int xvalue)
{
    SliderData data = m_map.value(slidername);
    float slope = (data.yrange.second-data.yrange.first)/(float(data.xrange.second)-float(data.xrange.first));
    return data.yrange.first + slope * (xvalue - data.xrange.first);
}

/*
void SliderRangeMap::add( QSlider * const slider, float miny, float maxy )
{
    m_map.insert(slider,qMakePair(miny,maxy));
}

int SliderRangeMap::add(QSlider * const slider, float miny, float maxy, float step)
{
    m_map.insert(slider,qMakePair(miny,maxy));
    // y-y1 = mx
    // xmax = (y2-y1)/m
    return ceil((maxy-miny)/step);
}

float SliderRangeMap::getMappedValue( QSlider * const slider, int xvalue)
{
    SliderYRange yrange = m_map.value(slider);
    // y-y1 = m (x-x1)
    //where m = (y2-y1)/(x2-x1)
    float slope = (yrange.second-yrange.first)/(float(slider->maximum())-float(slider->minimum()));

    // y = y1 + m (x-x1)
    return yrange.first + slope * (xvalue - slider->minimum());
}
*/

