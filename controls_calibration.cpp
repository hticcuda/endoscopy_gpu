#include "controls_calibration.h"
#include <QDataStream>

void Controls_calibration::copyFrom(const Controls_calibration &obj)
{
    this->rg = obj.rg;
    this->shd = obj.shd;
    this->shp = obj.shp;
    this->h1 = obj.h1;
    this->h2 = obj.h2;
    this->shd_width = obj.shd_width;
}

void Controls_calibration::reset() {
    rg=43;
    shd=56;
    shp=28;
    shd_width=31;
    h1=27;
    h2=1;
}

QDataStream & operator << (QDataStream & out, const Controls_calibration & obj)
{
    out<<obj.rg << obj.shd << obj.shd_width << obj.shp << obj.h1 << obj.h2;
    return out;
}

QDataStream & operator >> (QDataStream & in,  Controls_calibration & obj)
{
    in >> obj.rg
            >> obj.shd
            >> obj.shd_width
            >> obj.shp
            >> obj.h1
            >> obj.h2;

    return in ;
}

