#include "controls_display.h"
#include <QDataStream>


Controls_display::Controls_display(QObject *parent) : QObject(parent)
{
    reset();
}

void Controls_display::reset() {
    gamma = 16;
    sharpness = 10;
    gain_r = 3;
    gain_b = 4;
    chroma = 6;
    brightness_img = 0;
    contrast = 2;
    saturation = 0;
    threshold_shrp = 0;
}

QDataStream & operator << (QDataStream & out, const Controls_display & obj)
{
    out<<obj.gamma << obj.sharpness << obj.threshold_shrp << obj.gain_r << obj.gain_b << obj.brightness_img << obj.contrast << obj.saturation << obj.chroma;
    return out;
}

QDataStream & operator >> (QDataStream & in, Controls_display & obj)
{
    in >> obj.gamma
            >> obj.sharpness
            >> obj.threshold_shrp
            >> obj.gain_r
            >> obj.gain_b
            >> obj.brightness_img
            >> obj.contrast
            >> obj.saturation
            >> obj.chroma;
    return in;
}
