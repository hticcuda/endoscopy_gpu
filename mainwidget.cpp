#include "mainwidget.h"
#include "ui_mainwidget.h"
#include <QGridLayout>
#include <QMessageBox>
#include <QStyle>
#include <QLabel>
#include <QPushButton>
#include <QTimer>
#include <QThread>
#include <QDebug>
#include <QMainWindow>
#include "form.h"
#include <QShortcut>
#include <QDir>


//GPU_Render *mainwidget::getRend() const
//{
//    return rend;
//}

//void mainwidget::setRend(GPU_Render *value)
//{
//    rend = value;
//}

mainwidget::mainwidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::mainwidget),
    freezeready(false)
{
    ui->setupUi(this);

    tm4.start();


    if( !QDir("Images").exists())
    {
        QDir().mkdir("Images");
    }

    if( !QDir("Videos").exists())
    {
        QDir().mkdir("Videos");
    }

    rend = new gpu_qrend();

//    rend->start();

    frame = new FrameContext(rend);

    ext_inp = new External_input;
   QGridLayout * layout = new QGridLayout;
   layout->setContentsMargins(0,0,0,0);
//   layout->setHorizontalSpacing(2);
//   layout->setVerticalSpacing(2);


   f = new Form(frame,ext_inp,rend);

//   layout->addWidget(displaywid, 0, 0);
//   layout->addWidget(openGLLabel, 1, 0);
//   layout->addWidget(button,1,0);
//   layout->addWidget(openGLLabel,2,0);
    int id;
//   int id=qRegisterMetaType<YCbCr_container>("YCbCr_container");
//   connect(codecproc, SIGNAL(frameReady(YCbCr_container, short)), frame, SLOT(receive_ycbcr(YCbCr_container,short)),Qt::QueuedConnection);
//   connect(this,SIGNAL(ycbcr_ready(short)),frame,SLOT(receive_ycbcr(short)));
//   timer->start(10);
   id=qRegisterMetaType<cv::Mat>("cv::Mat");
   connect(frame, SIGNAL(rgb_ready(cv::Mat, bool)),this,SLOT(display_mainimage(cv::Mat,bool)),Qt::QueuedConnection);
//   connect(this,SIGNAL(clickedq(cv::Mat,bool)),rend,SLOT(gpuCpy(cv::Mat,bool)));
   connect(frame, SIGNAL(wbMsgReady()),this, SLOT(wb_done_message()),Qt::QueuedConnection);
   connect(f,SIGNAL(clicked_image()),this,SLOT(sample()));
   setLayout(layout);
   setWindowState(Qt::WindowFullScreen);

   f->show();

//   f->start_camera_streaming();
//   this->start_camera_streaming(2);
   //connect(&f->watcher,SIGNAL(finished()),this,SLOT(display_frozen()))
   connect(frame, SIGNAL(freeze_completed(int)),this,SLOT(display_frozen(int)),Qt::QueuedConnection);

   connect(f, SIGNAL(close_mainwidget_signal(int)),this,SLOT(start_camera_streaming(int)),Qt::QueuedConnection);

   connect(f,SIGNAL(clicked(int)),this,SLOT(start_camera_streaming(int)),Qt::QueuedConnection);

   new QShortcut(QKeySequence(Qt::Key_Q), this, SLOT(close_ui_application()));
   //TODO: add shortcut for F (freeze), R (record start/stop), S (start/stop streaming)
   new QShortcut(QKeySequence(Qt::Key_H),this,SLOT(show_shortcut_list()));

#if enable_chart == 1
    chart_initialize();
#endif

     std::cout<<"i am here !23"<<std::endl;
}

mainwidget::~mainwidget()
{
    delete ui;
}

void mainwidget::sample()
{
//    rend->gpuCpy();
    std::cout<<"i am here !23"<<std::endl;

}

void mainwidget::wb_done_message()
{
    QMessageBox *mbox = new QMessageBox{};
    //wb_timer = QTimer::singleShot(2000, mbox, SLOT(hide()));
    mbox->setStyleSheet("background-color: yellow; color:green");
    mbox->setWindowTitle(tr("Info"));
    mbox->setText("White balance completed");
    mbox->show();
    //wb_timer->(2000, mbox, SLOT(hide()));
    QTimer::singleShot(2000, mbox, SLOT(hide()));
}



void mainwidget::close_ui_application()
{
    f->htplg->exit = false;
//    f->htplg->wait();
    QApplication::closeAllWindows();

}


//slot to bring up the displaywidget
void mainwidget::start_camera_streaming(int a)
{
    std::cout<<a<<std::endl;
    if(a == 1)
    {
//        close();
    }
    else
    {
//        show();
    }

}

//void mainwidget::collectFieldData(short sequencenum)
//{
//    static cv::TickMeter tm1;
////   int time1 = QTime::currentTime().msec();
//#if collectfielddata == 1
//    tm1.reset();
//    tm1.start();
//#endif
//    //    time.msec();
////    QString current_time = time.toString();
////    qDebug()<<time1-time_old;
////    qDebug()
////    time_old = time1;
////    cv::Mat Y, CB, CR , RGB;
////    codecproc->copyResult(frame->Y,frame->CB,frame->CR);
//    emit ycbcr_ready(sequencenum);
////    Q_DECLARE_METATYPE(c::Mat);
////    int id = qRegisterMetaType<cv::Mat>();
////    QMetaObject::invokeMethod(frame,"ycbcr2rgb",Qt::QueuedConnection,Q_ARG(cv::Mat,Y),Q_ARG(cv::Mat, CB),Q_ARG(cv::Mat,CR));

////    ycbcr2rgb(RGB, Y, CB, CR);
////    cv::Mat image;
////    cv::cvtColor(RGB,image,CV_BGR2RGB);
////    ui->label->setPixmap(QPixmap::fromImage(QImage(image.data, image.cols, image.rows, image.step, QImage::Format_RGB888)));



////    qDebug()<<"time taken:  "<<tm4.getTimeMilli();

//#if collectfielddata == 1
//    tm1.stop();
//    qDebug()<<"collect field function time taken:  "<<tm1.getTimeMilli();
//#endif
//}

void mainwidget::display_mainimage(cv::Mat rgb, bool ispip)
{
//    int time2 = QTime::currentTime().msec();
//    cv::Mat rgba;
//    cv::cvtColor(rgb, rgba, CV_RGB2RGBA, 4);
//    cv::flip(rgba,rgba,0);
    rend->gpuCpy(rgb,1);
//    emit clickedq(rgb,1);

//    if(!freezeready)
//        displaywid->animate(rgb,ispip);
    //    cv::namedWindow("11");
//    cv::imshow("11",rgb);
//    qDebug()<<time2-time_old;
//    qDebug()
//    time_old = time2;
}

#if enable_chart == 1
void mainwidget::chart_initialize()
{
    chart = new QChart;
//    chart_1 = new QChart;
    chart_2 = new QChart;

    m_axis = new QValueAxis;
    chart1_axis = new QValueAxis;
    y_pre_axis = new QValueAxis;

    m_series = new QLineSeries;
    n_series = new QLineSeries;
    Y_pre_series = new QLineSeries;
//    chart1_series = new QScatterSeries();

//    chart1_series->setMarkerSize(5.0);

    QPen green(Qt::green);
    QPen red(Qt::red);
    QPen blue(Qt::blue);

    red.setWidth(3);
    green.setWidth(3);
    blue.setWidth(3);

    m_series->setPen(green);
    n_series->setPen(red);
    Y_pre_series->setPen(red);
//    chart1_series->setPen(blue);

    chart->addSeries(m_series);
    chart->addSeries(n_series);
//    chart_1->addSeries(chart1_series);
    chart_2->addSeries(Y_pre_series);

    chart->createDefaultAxes();
    chart->setAxisX(m_axis, m_series);
    chart->setAxisX(m_axis, n_series);
    chart_2->createDefaultAxes();
    chart_2->setAxisX(y_pre_axis,Y_pre_series);

//    chart_1->createDefaultAxes();
//    chart_1->setAxisX(chart1_axis, chart1_series);

    m_axis->setTickCount(20);
//    chart1_axis->setTickCount(20);

    y_pre_axis->setTickCount(20);
    chart_2->axisX()->setRange(0, 255);
    chart_2->axisY()->setRange(0,255);

    chart->axisX()->setRange(0, 255);
    chart->axisY()->setRange(0, 10/255.);

//    chart_1->axisX()->setRange(0,255);
//    chart_1->axisY()->setRange(0,255);

//    chart1_series->append(10,10);
//    chart1_series->append(100,100);
//    Y_pre_series->append(10,10);
//    Y_pre_series->append(100,100);


//    n_series->append(10, 0.0098);
//    n_series->append(20, 0.0098);
//    n_series->append(30, 0.0098);

    chart->setTitle("Y Histogram");
    chart_2->setTitle("LUT");
    chart->legend()->hide();
    chart_2->legend()->hide();

    chart->setAnimationOptions(QChart::NoAnimation);
    chart_2->setAnimationOptions(QChart::NoAnimation);


    chartRGB = new QChart;
    rgb_axis = new QValueAxis;
    r_series = new QLineSeries;
    g_series = new QLineSeries;
    b_series = new QLineSeries;
    r_series->setPen(red);
    g_series->setPen(green);
    b_series->setPen(blue);
    chartRGB->addSeries(r_series);
    chartRGB->addSeries(g_series);
    chartRGB->addSeries(b_series);

    chartRGB->createDefaultAxes();
    chartRGB->setAxisX(rgb_axis, r_series);
    chartRGB->setAxisX(rgb_axis, g_series);
    chartRGB->setAxisX(rgb_axis, b_series);

    rgb_axis->setTickCount(20);

    chartRGB->axisX()->setRange(0, 255);
    chartRGB->axisY()->setRange(0, 10/255.);

    chartRGB->setTitle("RGB Histogram");
    chartRGB->legend()->hide();

    chartRGB->setAnimationOptions(QChart::NoAnimation);


    connect(frame, SIGNAL(histReady(int)),this, SLOT(line_chart(int)),Qt::QueuedConnection);
    connect(frame, SIGNAL(lutReady()),this, SLOT(line_chart_lut()),Qt::QueuedConnection);

    connect(frame, SIGNAL(hist_rgbReady()),this, SLOT(line_chart_rgb()),Qt::QueuedConnection);

}
#endif


#if enable_chart == 1
void mainwidget::line_chart(int map_select)
{
    std::vector<float> bincounts ;
    //if(map_select == 1)
    {
        bincounts = frame->getHistY1();
        QList<QPointF> points = n_series->points();
        if(points.size()==0)
        {
            for(int x = 0; x < bincounts.size(); ++x)
                points.append(QPointF(x,bincounts[x]));
        }
        else
        {
            for(int x = 0; x < bincounts.size(); ++x)
                points[x]=QPointF(x,bincounts[x]);
        }
        n_series->replace(points);

    }
    //else
    if(map_select == 2)
    {
        bincounts = frame->getHistY2();
        QList<QPointF> points = m_series->points();
        if(points.size()==0)
        {
            for(int x = 0; x < bincounts.size(); ++x)
                points.append(QPointF(x,bincounts[x]));
        }
        else
        {
            for(int x = 0; x < bincounts.size(); ++x)
                points[x]=QPointF(x,bincounts[x]);
        }
        m_series->replace(points);

    }
//    ui->deviation->setNum(acq->mean_red);
    #if median_mode_table == 1
    if(true)
    {
        ui->red_tableWidget->setItem(0,0,new QTableWidgetItem(QString::number(acq->mean_red_TL)));
        ui->red_tableWidget->setItem(0,1,new QTableWidgetItem(QString::number(acq->mean_red_TR)));
        ui->red_tableWidget->setItem(0,2,new QTableWidgetItem(QString::number(acq->mean_red_BL)));
        ui->red_tableWidget->setItem(0,3,new QTableWidgetItem(QString::number(acq->mean_red_BR)));

        ui->red_tableWidget->setItem(1,0,new QTableWidgetItem(QString::number(acq->std_dev_red_TL)));
        ui->red_tableWidget->setItem(1,1,new QTableWidgetItem(QString::number(acq->std_dev_red_TR)));
        ui->red_tableWidget->setItem(1,2,new QTableWidgetItem(QString::number(acq->std_dev_red_BL)));
        ui->red_tableWidget->setItem(1,3,new QTableWidgetItem(QString::number(acq->std_dev_red_BR)));

        ui->green_tableWidget->setItem(0,0,new QTableWidgetItem(QString::number(acq->mean_green_TL)));
        ui->green_tableWidget->setItem(0,1,new QTableWidgetItem(QString::number(acq->mean_green_TR)));
        ui->green_tableWidget->setItem(0,2,new QTableWidgetItem(QString::number(acq->mean_green_BL)));
        ui->green_tableWidget->setItem(0,3,new QTableWidgetItem(QString::number(acq->mean_green_BR)));

        ui->green_tableWidget->setItem(1,0,new QTableWidgetItem(QString::number(acq->std_dev_green_TL)));
        ui->green_tableWidget->setItem(1,1,new QTableWidgetItem(QString::number(acq->std_dev_green_TR)));
        ui->green_tableWidget->setItem(1,2,new QTableWidgetItem(QString::number(acq->std_dev_green_BL)));
        ui->green_tableWidget->setItem(1,3,new QTableWidgetItem(QString::number(acq->std_dev_green_BR)));

        ui->blue_tableWidget->setItem(0,0,new QTableWidgetItem(QString::number(acq->mean_blue_TL)));
        ui->blue_tableWidget->setItem(0,1,new QTableWidgetItem(QString::number(acq->mean_blue_TR)));
        ui->blue_tableWidget->setItem(0,2,new QTableWidgetItem(QString::number(acq->mean_blue_BL)));
        ui->blue_tableWidget->setItem(0,3,new QTableWidgetItem(QString::number(acq->mean_blue_BR)));

        ui->blue_tableWidget->setItem(1,0,new QTableWidgetItem(QString::number(acq->std_dev_blue_TL)));
        ui->blue_tableWidget->setItem(1,1,new QTableWidgetItem(QString::number(acq->std_dev_blue_TR)));
        ui->blue_tableWidget->setItem(1,2,new QTableWidgetItem(QString::number(acq->std_dev_blue_BL)));
        ui->blue_tableWidget->setItem(1,3,new QTableWidgetItem(QString::number(acq->std_dev_blue_BR)));
    }
    #endif
}

void mainwidget::line_chart_rgb()
{
    std::vector<float> bincounts_r, bincounts_g, bincounts_b;
    //bincounts_rgb.assign(3,0);
    bincounts_r = frame->getHistR();
    bincounts_g = frame->getHistG();
    bincounts_b = frame->getHistB();

    QList<QPointF> points = r_series->points();
    if(points.size()==0)
    {
        for(int x = 0; x < bincounts_r.size(); ++x)
            points.append(QPointF(x,bincounts_r[x]));
    }
    else
    {
        for(int x = 0; x < bincounts_r.size(); ++x)
            points[x]=QPointF(x,bincounts_r[x]);
    }
    r_series->replace(points);

    points = g_series->points();
    if(points.size()==0)
    {
        for(int x = 0; x < bincounts_g.size(); ++x)
            points.append(QPointF(x,bincounts_g[x]));
    }
    else
    {
        for(int x = 0; x < bincounts_g.size(); ++x)
            points[x]=QPointF(x,bincounts_g[x]);
    }
    g_series->replace(points);

    points = b_series->points();
    if(points.size()==0)
    {
        for(int x = 0; x < bincounts_b.size(); ++x)
            points.append(QPointF(x,bincounts_b[x]));
    }
    else
    {
        for(int x = 0; x < bincounts_b.size(); ++x)
            points[x]=QPointF(x,bincounts_b[x]);
    }
    b_series->replace(points);

}

#endif

void mainwidget::display_frozen(int freezeidx)
{
    cv::Mat frozen;

    freezeready = true; // block display_mainimage
    frame->get_buffer_image(freezeidx,frozen);
    std::cerr<<std::endl << "frozen display idx is " << freezeidx <<std::endl;
//    displaywid->setFreeze(2);// send to main

//    displaywid->animate(frozen,false);
    freezeready = false; // allow display_mainimage
}

void mainwidget::show_shortcut_list()
{
    shortcut_form = new Form_shortcut();
    shortcut_form->show();
}


#if enable_chart == 1
void mainwidget::line_chart_lut()
{
//    chart1_series->clear();
    std::vector<uchar> lut8u = frame->getLUT();
    QList<QPointF> points = Y_pre_series->points();
    if(points.size()==0)
    {
        for(int x = 0;x<=255;x++)
            points.append(QPointF(x,lut8u[x]));

    }
    else
    {
        for(int x = 0;x<=255;x++)
            points[x]=QPointF(x,lut8u[x]);
    }
    Y_pre_series->replace(points);
//    Y_pre_series->clear();
//    for(int x = 0; x<=960;x++)
//    {
//        Y_pre_series->append(x,acq->a[x]);
//    }

}

//for showing the video output in the mat
//void MainWindow::display_video_e()
//{
//    cv::Mat image = acq->rgb;
//    cv::cvtColor(image,image,CV_BGR2RGB);
//    ui->label->setPixmap(QPixmap::fromImage(QImage(image.data, image.cols, image.rows, image.step, QImage::Format_RGB888)));

//}
#endif
