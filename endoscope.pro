#-------------------------------------------------
#
# Project created by QtCreator 2018-09-19T17:29:58
#
#-------------------------------------------------

QT       += core gui charts opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = endoscope
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

QMAKE_CXXFLAGS += -std=c++11 -pthread

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
    uvccommunication.cpp \
    mainwidget.cpp \
    codec.cpp \
    framecontext.cpp \
    form.cpp \
    controls_calibration.cpp \
    controls_display.cpp \
    controls_sensor.cpp \
    form_uvc.cpp \
    postprocessing.cpp \
    sliderrangemap.cpp \
    patient_information.cpp \
    patientwindow.cpp \
    form_shortcut.cpp \
    preference.cpp \
    external_input.cpp \
    hotplug.cpp \
    button_interface.cpp \
    gpu_qrend.cpp \
    imgfile_wrt.cpp

HEADERS += \
    uvccommunication.h \
    mainwidget.h \
    codec.h \
    debug.h \
    framecontext.h \
    messagetypes.h \
    form.h \
    controls_calibration.h \
    controls_display.h \
    controls_sensor.h \
    form_uvc.h \
    postprocessing.h \
    sliderrangemap.h \
    patientwindow.h \
    patient_information.h \
    form_shortcut.h \
    preference.h \
    external_input.h \
    hotplug.h \
    button_interface.h \
    gpu_qrend.h \
    imgfile_wrt.h

FORMS += \
    mainwidget.ui \
    form.ui \
    form_uvc.ui \
    patientwindow.ui \
    form_shortcut.ui


LIBS +=`pkg-config opencv --cflags --libs` -luvc -lusb-1.0 -lpthread -lGLU -lglut -lGL -lpng -lavcodec -lswscale -lavutil

#INCLUDEPATH += /usr/local/lib

RESOURCES += \
    resources.qrc

STATECHARTS += \
    sample.scxml

DISTFILES += \
    simpleTexture3D_kernel.cu


#cuda pro stuff

# CUDA settings <-- may change depending on your system
CUDA_SOURCES += ./simpleTexture3D_kernel.cu
CUDA_SDK = "/usr/local/cuda-10.0/"   # Path to cuda SDK install
CUDA_DIR = "/usr/local/cuda-10.0/"   # Path to cuda toolkit install
CUDA_COM = "/usr/local/cuda-10.0/samples/common/"   # Path to cuda samples common inc

# DO NOT EDIT BEYOND THIS UNLESS YOU KNOW WHAT YOU ARE DOING....

#SYSTEM_NAME = unix         # Depending on your system either 'Win32', 'x64', or 'Win64'
SYSTEM_TYPE = 64            # '32' or '64', depending on your system
CUDA_ARCH = sm_30           # Type of CUDA architecture, for example 'compute_10', 'compute_11', 'sm_10'
NVCC_OPTIONS = --use_fast_math

QMAKE_LIBDIR += $$CUDA_DIR/lib64

# include paths
INCLUDEPATH += \
    $$CUDA_DIR/include \
    $$CUDA_COM/inc


CUDA_OBJECTS_DIR = ./


# Add the necessary libraries
CUDA_LIBS = -lcuda -lcudart

# The following makes sure all path names (which often include spaces) are put between quotation marks
CUDA_INC = $$join(INCLUDEPATH,'" -I"','-I"','"')
#LIBS += $$join(CUDA_LIBS,'.so ', '', '.so')
LIBS += $$CUDA_LIBS

# Configuration of the Cuda compiler
CONFIG(debug, debug|release) {
    # Debug mode
    cuda_d.input = CUDA_SOURCES
    cuda_d.output = $$CUDA_OBJECTS_DIR/${QMAKE_FILE_BASE}_cuda.o
    cuda_d.commands = $$CUDA_DIR/bin/nvcc -D_DEBUG $$NVCC_OPTIONS $$CUDA_INC $$NVCC_LIBS --machine $$SYSTEM_TYPE -arch=$$CUDA_ARCH -c -o ${QMAKE_FILE_OUT} ${QMAKE_FILE_NAME} -D MESA_EGL_NO_X11_HEADERS
    cuda_d.dependency_type = TYPE_C
    QMAKE_EXTRA_COMPILERS += cuda_d
}
else {
    # Release mode
    cuda.input = CUDA_SOURCES
    cuda.output = $$CUDA_OBJECTS_DIR/${QMAKE_FILE_BASE}_cuda.o
    cuda.commands = $$CUDA_DIR/bin/nvcc $$NVCC_OPTIONS $$CUDA_INC $$NVCC_LIBS --machine $$SYSTEM_TYPE -arch=$$CUDA_ARCH -c -o ${QMAKE_FILE_OUT} ${QMAKE_FILE_NAME} -D MESA_EGL_NO_X11_HEADERS
    cuda.dependency_type = TYPE_C
    QMAKE_EXTRA_COMPILERS += cuda
}



