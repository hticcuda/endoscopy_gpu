#include "gpu_render.h"
//#include <QApplication>
#include <QDebug>
#include <helper_gl.h>
#include <GL/freeglut.h>

#include <iostream>

#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <png.h>
#include <libavcodec/avcodec.h>
//#include <libavutil/imgutils.h>
//#include <libavutil/opt.h>
//#include <libswscale/swscale.h>


using namespace std;

float w = 0;  // texture coordinate in z

GLuint pbo;     // OpenGL pixel buffer object
struct cudaGraphicsResource *cuda_pbo_resource; // CUDA Graphics Resource (to transfer PBO)

bool animate = true;

StopWatchInterface *timer = NULL;

uint *d_output = nullptr;

//// Auto-Verification Code
const int frameCheckNumber = 4;
int fpsCount = 0;        // FPS count for averaging
int fpsLimit = 1;        // FPS limit for sampling
int g_Index = 0;
unsigned int frameCount = 0;
unsigned int g_TotalErrors = 0;
volatile int g_GraphicsMapFlag = 0;

int hBufNo = 0;
int saveFlag = 0;
unsigned char *savedImg;
uchar *asd;
int pressCount = 0;
cv::VideoWriter outputVideo;
cv::Mat vpixels( height, width, CV_8UC3 );
cv::Mat cv_pixels( height, width, CV_8UC3 );

void *font = GLUT_BITMAP_TIMES_ROMAN_24;

static GLubyte *pixels = NULL;
static png_byte *png_bytes = NULL;
static png_byte **png_rows = NULL;
//static AVCodecContext *c = NULL;
//static AVFrame *frame;
//static AVPacket pkt;
//static FILE *file;
//static struct SwsContext *sws_context = NULL;
//static uint8_t *rgb = NULL;
//static unsigned int nframes = 0;

extern "C" void cleanup();
extern "C" void cpyD2H(uchar *);
extern "C" void cudaCpy(const uchar *h_volume, cudaExtent volumeSize, int dBufNo);
extern "C" void initCuda(cudaExtent volumeSize);
extern "C" void render_kernel(dim3 gridSize, dim3 blockSize, uint *d_output, uint imageW, uint imageH, float w, uchar*, int*);

static void screenshot_ppm(const char *filename, unsigned int width,
        unsigned int height, GLubyte **pixels) {
    size_t i, j, cur;
    const size_t format_nchannels = 3;
    FILE *f = fopen(filename, "w");
    fprintf(f, "P3\n%d %d\n%d\n", width, height, 255);
    *pixels = (GLubyte *)realloc(*pixels, format_nchannels * sizeof(GLubyte) * width * height);
    glReadPixels(0, 0, width, height, GL_RGB, GL_UNSIGNED_BYTE, *pixels);
//    for (i = 0; i < height; i++) {
//        for (j = 0; j < width; j++) {
//            cur = format_nchannels * ((height - i - 1) * width + j);
//            fprintf(f, "%3d %3d %3d ", (*pixels)[cur], (*pixels)[cur + 1], (*pixels)[cur + 2]);
//        }
//        fprintf(f, "\n");
//    }
    fclose(f);
}

static void screenshot_png(const char *filename, unsigned int width, unsigned int height,
        GLubyte **pixels, png_byte **png_bytes, png_byte ***png_rows) {
    size_t i, nvals;
    const size_t format_nchannels = 4;
    FILE *f = fopen(filename, "wb");
    nvals = format_nchannels * width * height;
    *pixels = (GLubyte *)realloc(*pixels, nvals * sizeof(GLubyte));
    *png_bytes = (png_byte *)realloc(*png_bytes, nvals * sizeof(png_byte));
    *png_rows = (png_byte **)realloc(*png_rows, height * sizeof(png_byte*));
    glReadPixels(0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, *pixels);
    for (i = 0; i < nvals; i++)
        (*png_bytes)[i] = (*pixels)[i];
    for (i = 0; i < height; i++)
        (*png_rows)[height - i - 1] = &(*png_bytes)[i * width * format_nchannels];
    png_structp png = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (!png) abort();
    png_infop info = png_create_info_struct(png);
    if (!info) abort();
    if (setjmp(png_jmpbuf(png))) abort();
    png_init_io(png, f);
    png_set_IHDR(
        png,
        info,
        width,
        height,
        8,
        PNG_COLOR_TYPE_RGBA,
        PNG_INTERLACE_NONE,
        PNG_COMPRESSION_TYPE_DEFAULT,
        PNG_FILTER_TYPE_DEFAULT
    );
    png_write_info(png, info);
    png_write_image(png, *png_rows);
    png_write_end(png, NULL);
    png_destroy_write_struct(&png, &info);
    free(png_bytes);
    free(png_rows);
    fclose(f);
}

//static void ffmpeg_encoder_set_frame_yuv_from_rgb(uint8_t *rgb) {
//    const int in_linesize[1] = { 4 * c->width };
//    sws_context = sws_getCachedContext(sws_context,
//            c->width, c->height, AV_PIX_FMT_RGB32,
//            c->width, c->height, AV_PIX_FMT_YUV420P,
//            0, NULL, NULL, NULL);
//    sws_scale(sws_context, (const uint8_t * const *)&rgb, in_linesize, 0,
//            c->height, frame->data, frame->linesize);
//}

//void ffmpeg_encoder_start(const char *filename, int codec_id, int fps, int width, int height) {
//    AVCodec *codec;
//    int ret;
//    avcodec_register_all();
//    codec = avcodec_find_encoder((AVCodecID)codec_id);
//    if (!codec) {
//        fprintf(stderr, "Codec not found\n");
//        exit(1);
//    }
//    c = avcodec_alloc_context3(codec);
//    if (!c) {
//        fprintf(stderr, "Could not allocate video codec context\n");
//        exit(1);
//    }
//    c->bit_rate = 400000;
//    c->width = width;
//    c->height = height;
//    c->time_base.num = 1;
//    c->time_base.den = fps;
//    c->gop_size = 10;
//    c->max_b_frames = 1;
//    c->pix_fmt = AV_PIX_FMT_YUV420P;
//    if (codec_id == AV_CODEC_ID_H264)
//        av_opt_set(c->priv_data, "preset", "slow", 0);
//    if (avcodec_open2(c, codec, NULL) < 0) {
//        fprintf(stderr, "Could not open codec\n");
//        exit(1);
//    }
//    file = fopen(filename, "wb");
//    if (!file) {
//        fprintf(stderr, "Could not open %s\n", filename);
//        exit(1);
//    }
//    frame = av_frame_alloc();
//    if (!frame) {
//        fprintf(stderr, "Could not allocate video frame\n");
//        exit(1);
//    }
//    frame->format = c->pix_fmt;
//    frame->width  = c->width;
//    frame->height = c->height;
//    ret = av_image_alloc(frame->data, frame->linesize, c->width, c->height, c->pix_fmt, 32);
//    if (ret < 0) {
//        fprintf(stderr, "Could not allocate raw picture buffer\n");
//        exit(1);
//    }
//}

//void ffmpeg_encoder_finish(void) {
//    uint8_t endcode[] = { 0, 0, 1, 0xb7 };
//    int got_output, ret;
//    do {
//        fflush(stdout);
//        ret = avcodec_encode_video2(c, &pkt, NULL, &got_output);
//        if (ret < 0) {
//            fprintf(stderr, "Error encoding frame\n");
//            exit(1);
//        }
//        if (got_output) {
//            fwrite(pkt.data, 1, pkt.size, file);
//            av_packet_unref(&pkt);
//        }
//    } while (got_output);
//    fwrite(endcode, 1, sizeof(endcode), file);
//    fclose(file);
//    avcodec_close(c);
//    av_free(c);
//    av_freep(&frame->data[0]);
//    av_frame_free(&frame);
//}

//void ffmpeg_encoder_encode_frame(uint8_t *rgb) {
//    int ret, got_output;
//    ffmpeg_encoder_set_frame_yuv_from_rgb(rgb);
//    av_init_packet(&pkt);
//    pkt.data = NULL;
//    pkt.size = 0;
//    ret = avcodec_encode_video2(c, &pkt, frame, &got_output);
//    if (ret < 0) {
//        fprintf(stderr, "Error encoding frame\n");
//        exit(1);
//    }
//    if (got_output) {
//        fwrite(pkt.data, 1, pkt.size, file);
//        av_packet_unref(&pkt);
//    }
//}

//void ffmpeg_encoder_glread_rgb(uint8_t **rgb, GLubyte **pixels, unsigned int width, unsigned int height) {
//    size_t i, j, k, cur_gl, cur_rgb, nvals;
//    const size_t format_nchannels = 4;
//    nvals = format_nchannels * width * height;
//    *pixels = (GLubyte *)realloc(*pixels, nvals * sizeof(GLubyte));
//    *rgb = (uint8_t *)realloc(*rgb, nvals * sizeof(uint8_t));
//    /* Get RGBA to align to 32 bits instead of just 24 for RGB. May be faster for FFmpeg. */
//    glReadPixels(0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, *pixels);
//    for (i = 0; i < height; i++) {
//        for (j = 0; j < width; j++) {
//            cur_gl  = format_nchannels * (width * (height - i - 1) + j);
//            cur_rgb = format_nchannels * (width * i + j);
//            for (k = 0; k < format_nchannels; k++)
//                (*rgb)[cur_rgb + k] = (*pixels)[cur_gl + k];
//        }
//    }
//}

void drawString(const char *str, int x, int y, float color[4], void *font)
{
    glPushAttrib(GL_LIGHTING_BIT | GL_CURRENT_BIT); // lighting and color mask
    glDisable(GL_LIGHTING);     // need to disable lighting for proper text color
    glDisable(GL_TEXTURE_2D);

    glColor4fv(color);          // set text color
    glRasterPos2i(x, y);        // place text position

    // loop all characters in the string
    while(*str)
    {
        glutBitmapCharacter(font, *str);
        ++str;
    }

    glEnable(GL_TEXTURE_2D);
    glEnable(GL_LIGHTING);
    glPopAttrib();
}

void showInfo()
{
    // backup current model-view matrix
    glPushMatrix();                     // save current modelview matrix
    glLoadIdentity();                   // reset modelview matrix
    gluOrtho2D(0, 1920, 0, 1080);  // set to orthogonal projection

    const int FONT_HEIGHT = 14;
    float color[4] = {1, 1, 1, 1};

    std::stringstream ss;
//    ss << "PBO: ";
//    if(pboUsed)
//        ss << "on" << std::ends;
//    else
//        ss << "off" << std::ends;

//    drawString(ss.str().c_str(), 1, SCREEN_HEIGHT-FONT_HEIGHT, color, font);
//    ss.str(""); // clear buffer

//    ss << "Read Time: " << readTime << " ms" << std::ends;
//    drawString(ss.str().c_str(), 1, SCREEN_HEIGHT-(2*FONT_HEIGHT), color, font);
//    ss.str("");

//    ss << std::fixed << std::setprecision(3);
//    ss << "Process Time: " << processTime << " ms" << std::ends;
//    drawString(ss.str().c_str(), 1, SCREEN_HEIGHT-(3*FONT_HEIGHT), color, font);
//    ss.str("");

//    ss << "Press SPACE to toggle PBO." << std::ends;
//    drawString(ss.str().c_str(), 200, 1000, color, font);

    ss << "Hi Ananth " << saveFlag++ << std::ends;
    drawString(ss.str().c_str(), 900, 530, color, font);

    // unset floating format
//    ss << std::resetiosflags(std::ios_base::fixed | std::ios_base::floatfield);

    // restore projection matrix
//    glPopMatrix();                   // restore to previous projection matrix

    // restore modelview matrix
//    glMatrixMode(GL_MODELVIEW);      // switch to modelview matrix
    glPopMatrix();                   // restore to previous modelview matrix
}

void computeFPS()
{
    frameCount++;
    fpsCount++;

    if (fpsCount == fpsLimit)
    {
        char fps[256];
        float ifps = 1.f / (sdkGetAverageTimerValue(&timer) / 1000.f);
        sprintf(fps, "simpleTexture3D: %3.1f fps", ifps);

        glutSetWindowTitle(fps);
        fpsCount = 0;

        fpsLimit = ftoi(MAX(1.0f, ifps));
        sdkResetTimer(&timer);
    }
}

// render image using CUDA
void render()
{
    // map PBO to get CUDA device pointer
    g_GraphicsMapFlag++;
    checkCudaErrors(cudaGraphicsMapResources(1, &cuda_pbo_resource, 0));
    size_t num_bytes;
    checkCudaErrors(cudaGraphicsResourceGetMappedPointer((void **)&d_output, &num_bytes, cuda_pbo_resource));
    //printf("CUDA mapped PBO: May access %ld bytes\n", num_bytes);

    // call CUDA kernel, writing results to PBO
    render_kernel(gridSize, blockSize, d_output, width, height, w, asd, &saveFlag);

    getLastCudaError("render_kernel failed");

    if (g_GraphicsMapFlag)
    {
        checkCudaErrors(cudaGraphicsUnmapResources(1, &cuda_pbo_resource, 0));
        g_GraphicsMapFlag--;
    }
}

// display results using OpenGL (called by GLUT)
void display()
{
    sdkStartTimer(&timer);

    render();
    // display results
    glClear(GL_COLOR_BUFFER_BIT);

    // draw image from PBO
    glDisable(GL_DEPTH_TEST);
    glRasterPos2i(0, 0);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, pbo);
    glDrawPixels(width, height, GL_RGBA, GL_UNSIGNED_BYTE, 0);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, 0);

    showInfo();

    glutSwapBuffers();
    glutReportErrors();

//    cout<<"pressCount = " <<pressCount<<endl;
//    if(pressCount%2==1)
//    {
////        cout<<"GO "<<endl;
//        frame->pts = nframes;
//        ffmpeg_encoder_glread_rgb(&rgb, &pixels, width, height);
//        ffmpeg_encoder_encode_frame(rgb);
//        nframes++;
//    }

    sdkStopTimer(&timer);
    computeFPS();
}

void idle()
{
    if (animate)
    {
        w += 1;
//        w = float(int(w) % 120);
        glutPostRedisplay();
    }
}

void keyboard(unsigned char key, int x, int y)
{
    switch (key)
    {
        case 27:
            glutDestroyWindow(glutGetWindow());
            return;

        case '+':
            w += 1;
            break;

        case '-':
            w -= 1;
            break;

        case ' ':
            animate = !animate;
            break;

        default:
            break;
    }

    glutPostRedisplay();
}

void reshape(int x, int y)
{
    glViewport(0, 0, x, y);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0.0, 1.0, 0.0, 1.0, 0.0, 1.0);
}

void cleanup()
{
    sdkDeleteTimer(&timer);

    // add extra check to unmap the resource before unregistering it
    if (g_GraphicsMapFlag)
    {
        checkCudaErrors(cudaGraphicsUnmapResources(1, &cuda_pbo_resource, 0));
        g_GraphicsMapFlag--;
    }

    // unregister this buffer object from CUDA C
    checkCudaErrors(cudaGraphicsUnregisterResource(cuda_pbo_resource));
    glDeleteBuffers(1, &pbo);
}

void initGLBuffers()
{
    // create pixel buffer object
    glGenBuffers(1, &pbo);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, pbo);
    glBufferData(GL_PIXEL_UNPACK_BUFFER_ARB, width*height*sizeof(GLubyte)*4, 0, GL_STREAM_DRAW_ARB);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, 0);

    // register this buffer object with CUDA
    checkCudaErrors(cudaGraphicsGLRegisterBuffer(&cuda_pbo_resource, pbo, cudaGraphicsMapFlagsWriteDiscard));
}

void GPU_Render::initGL(int *argc, char **argv)
{
    // initialize GLUT callback functions
    glutInit(argc, argv);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
    glutInitWindowSize(width, height);
    glutCreateWindow("CUDA 3D texture");
    glutFullScreen();
    glutDisplayFunc(display);
    glutKeyboardFunc(keyboard);
    glutReshapeFunc(reshape);
    glutIdleFunc(idle);

    if (!isGLVersionSupported(2,0) || !areGLExtensionsSupported("GL_ARB_pixel_buffer_object"))
    {
        fprintf(stderr, "Required OpenGL extensions are missing.");
        exit(EXIT_FAILURE);
    }
}
//uchar *hBuf;
void loadVolumeData()
{
    initCuda(volumeSize);
    sdkCreateTimer(&timer);
}

void GPU_Render::saveImg()
{
    screenshot_ppm("aImg",1920,1080,&pixels);

//    screenshot_png("bImg", 1920, 1080, &pixels, &png_bytes, &png_rows);

//    pressCount = pressCount + 1;

//    if(pressCount%2==1)
//    {
//        outputVideo.open( "video.avi", CV_FOURCC('M', 'P', 'E', 'G'), 30, cv::Size( width, height ), true);

//        cout<<"GO1"<<endl;
////        ffmpeg_encoder_start("tmp.mpg", AV_CODEC_ID_MPEG1VIDEO, 30, width, height);

//    }
//    else
//    {
//        outputVideo.release();
//        cout<<"GO2"<<endl;
////        ffmpeg_encoder_finish();
////        free(rgb);
//    }
}

void GPU_Render::gpuCpy(uchar * data)
{
//    cout<<"herher"<<endl;
    cudaCpy(data, volumeSize, hBufNo);
    glutPostRedisplay();
    glutMainLoopEvent();
    qDebug()<<"GPUhit";
    //    free(hBuf);
    hBufNo = (hBufNo + 1) % 5;

//    QCoreApplication::processEvents();

    if(outputVideo.isOpened())
    {
        glReadPixels(0, 0, width, height, GL_RGB, GL_UNSIGNED_BYTE, vpixels.data );
//        for( int y=0; y<height; y++ ) for( int x=0; x<width; x++ )
//        {
//            cv_pixels.at<cv::Vec3b>(y,x)[2] = vpixels.at<cv::Vec3b>(height-y-1,x)[0];
//            cv_pixels.at<cv::Vec3b>(y,x)[1] = vpixels.at<cv::Vec3b>(height-y-1,x)[1];
//            cv_pixels.at<cv::Vec3b>(y,x)[0] = vpixels.at<cv::Vec3b>(height-y-1,x)[2];
//        }
//        outputVideo << cv_pixels;
    }
}

GPU_Render::GPU_Render()
{

    int arg = 1;
    std::string astr = "/home/htic/Desktop/GPU/endoscopy";//QDir::currentPath().toStdString();
    cout<<astr<<endl;
    initGL(&arg, (char **)&astr);
    findCudaGLDevice(arg, (const char **)&astr);
    // OpenGL buffers
    initGLBuffers();
    loadVolumeData();
//    gpuCpy();

    cout<<"Press space to toggle animation\nPress '+' and '-' to change displayed slice\n"<<endl;

//    printf("Press space to toggle animation\n"
//           "Press '+' and '-' to change displayed slice\n");

    glutCloseFunc(cleanup);

//    glutMainLoop();
}
