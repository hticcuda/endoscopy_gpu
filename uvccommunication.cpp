        #include "uvccommunication.h"
    #include "codec.h"
    #include "QShortcut"

    bool UvcCommunication::ready() const
    {
        return m_ready;
    }


    libusb_device_handle *UvcCommunication::getUsb_devh() const
    {
        return usb_devh;
    }

    void UvcCommunication::setOpened(bool opened)
    {
        m_opened = opened;
    }

    void UvcCommunication::setInitialized(bool initialized)
    {
        m_initialized = initialized;
    }

    bool UvcCommunication::getReady() const
    {
        return m_ready;
    }

    void UvcCommunication::setReady(bool ready)
    {
        m_ready = ready;
    }

    bool UvcCommunication::getClose_window_flag() const
    {
        return close_window_flag;
    }

    void UvcCommunication::setClose_window_flag(bool value)
    {
        close_window_flag = value;
        emit close_camera_window();
    }

    bool UvcCommunication::getRestart_camera_streaming() const
    {
        return restart_camera_streaming;
    }

    void UvcCommunication::setRestart_camera_streaming(bool value)
    {
        restart_camera_streaming = value;

        m_initialized = false;
        m_opened = false;
        m_streaming = false;
        std::cout<<"hotplug set resart "<<std::endl;
        open_camera();
        emit restart_camera();
    }

    bool UvcCommunication::getDevice_status() const
    {
        return device_status;
    }

    void UvcCommunication::setDevice_status(bool value)
    {
        device_status = value;
    }

    int UvcCommunication::getButton_pressed() const
    {
        return button_pressed;
    }

    void UvcCommunication::setButton_pressed(int value)
    {
        button_pressed = value;
        emit buttonpressed(value);
    }

    bool UvcCommunication::getUsb_ready() const
    {
        return usb_ready;
    }

    void UvcCommunication::setUsb_ready(bool value)
    {
        usb_ready = value;
    }

    UvcCommunication::UvcCommunication(gpu_qrend * urendObj, QObject *parent) : QObject(parent)
    {
        urend = urendObj;
        m_initialized = false;
        m_opened = false;
        m_ready = false;
        m_streaming = false;
    //    connect(timer, SIGNAL(timeout()), this, SLOT(open_camera()));
    //    timer->start(100);
        open_camera();
    }

    //void UvcCommunication::poll_for_device(int interval)
    //{
    //    timer->start(interval);
    //}

    void UvcCommunication::open_camera()
    {
        uvc_error_t res;
        if(!m_initialized)
        {
            res = uvc_init(&ctx, NULL);
            if (res < 0) {
                uvc_perror(res, "uvc_init error");
                qCritical()<<__FUNCTION__<< "@uvc_init:" << uvc_strerror(res);
            }
            else
            {
                m_initialized = true;
            }
        }

        if(!m_opened    )
        {
            //uvc find the camera device connected
            res = uvc_find_device(ctx, &dev,8730, 256, NULL);
            if (res < 0)
            {
                uvc_perror(res, "uvc_find_device error");
                device_status = true;
                qCritical()<<__FUNCTION__<< "@uvc_find_device:" << uvc_strerror(res);
                uvc_exit(ctx);
                m_initialized = false;
            }
            else
            {
                //camera device founded, needs to open for streaming
                res = uvc_open(dev, &devh);
                if (res < 0)
                {
                  uvc_perror(res, "uvc_open error");
                    qCritical()<<__FUNCTION__<< "@uvc_open:" << uvc_strerror(res);
                }
                else
                {
                    m_opened = true;
                    //function will check the size of the camera frame out and the format supported
                    //962x383 is the size of the frame receiving
                    res = uvc_get_stream_ctrl_format_size(devh, &ctrl, UVC_FRAME_FORMAT_YUYV, fieldwidth, fieldheight, 60);
                    if (res < 0)
                    {
                        uvc_close(devh);
                        m_opened = false;
                        uvc_perror(res, "uvc_get_stream_ctrl_format_size error");
                        qCritical()<<__FUNCTION__<< "@uvc_get_stream_ctrl_format_size:" << uvc_strerror(res);
                    }
                    else
                    {
                        emit camera_status(true);
                        if(timer->isActive())
                            timer->stop();
                        m_ready = true;
                        device_status = false;
                        usb_devh = libusb_open_device_with_vid_pid(NULL,8730,256);
                        setUsb_ready(true);
                    }
                }
            }
        }
        else {
            //FIXME: already open? do `t allow anything here
    //        return false;
        }
    }


    UvcCommunication::~UvcCommunication()
    {
        if(devh!=NULL)
        {
    //        uvc_stop_streaming(devh);
            stop_streaming();
            uvc_close(devh);
        }

        if(dev!=NULL)
        {
            uvc_unref_device(dev);
        }
        if(ctx!=NULL)
        {
            uvc_exit(ctx);
        }
    }

    bool UvcCommunication::start_streaming(Codec * const cpobj)
    {
        if(!m_ready)
            open_camera();
    //    uvc_set_sharpness(devh,(int16_t)1);
        if(!m_ready)
        {
            std::cerr << __FUNCTION__ << ": check camera ready.";
            return false;
        }
        else
        {
            if(!m_streaming)
            {
                uvc_error_t res = uvc_start_streaming(devh, &ctrl,Codec::cb, cpobj, 0);
                if (res < 0)
                {
    //                uvc_perror(res, "uvc_start_streaming error");
                    qCritical()<<__FUNCTION__<< "@uvc_start_streaming:" << uvc_strerror(res);

                    return false;
                }
                m_streaming = true;

        //        return UVC_SUCCESS;
                return true;
            }
            else {
                //FIXME: already streaming? nothing to do
                return true;
            }
        }
    //    return UVC_ERROR_INVALID_DEVICE;
    }

    void UvcCommunication::stop_streaming()
    {
        if(m_streaming)
        {
            std::cerr << "try stop";
            uvc_stop_streaming(devh);
            std::cerr << "stopped streaming";
            m_streaming = false;
        }
    }

    void UvcCommunication::send_param(UVCparam uvcparamname, int value)
    {

        if(devh!=NULL && m_ready)
        switch(uvcparamname)
        {
        case BRIGHTNESS1: uvc_set_brightness(devh, static_cast<int16_t>(value)); break;
        case VGA: uvc_set_digital_multiplier_limit(devh,static_cast<uint16_t>(value)); break;
        case BLACK_CLAMP_LEVEL: uvc_set_digital_multiplier(devh,static_cast<uint16_t>(value)); break;
        case EXPOSURE: uvc_set_gamma(devh, static_cast<uint16_t>(value));break;
        case ALCAEC:  uvc_set_backlight_compensation(devh, static_cast<uint16_t>(value));break;
        case BRIGHTNESS2: uvc_set_power_line_frequency(devh, static_cast<uint8_t>(value)); break;
        case BLUE_LED: uvc_set_sharpness(devh,static_cast<uint16_t>(value));break;
        }
    }

    void UvcCommunication::send_calib(Calibparam calibparamname, int value )
    {
        if(devh!=NULL && m_ready)
        switch(calibparamname)
        {
        case RG: uvc_set_saturation(devh, static_cast<uint16_t>(value)); break;
        case SHD: uvc_set_gain(devh, static_cast<uint16_t>(value)); break;
        case SHD_WIDTH: uvc_set_contrast(devh, static_cast<uint16_t>(38));break;
        case SHP: uvc_set_white_balance_temperature(devh, static_cast<uint16_t>(value)); break;
        case H1: uvc_set_analog_video_standard(devh, static_cast<uint8_t>(value));break;
        case H2: uvc_set_analog_video_lock_status(devh, static_cast<uint8_t>(value));break;
        case THRESHOLD: uvc_set_hue_auto(devh, static_cast<uint8_t>(value));break;

        }

    }
