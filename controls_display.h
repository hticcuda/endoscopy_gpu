#ifndef CONTROLS_DISPLAY_H
#define CONTROLS_DISPLAY_H

#include <QObject>

class Controls_display : public QObject
{
    Q_OBJECT
public:
    explicit Controls_display(QObject *parent = nullptr);

    int gamma, sharpness,threshold_shrp, gain_r, gain_b,brightness_img,contrast,saturation, chroma;

    void reset();
signals:

public slots:
};

QDataStream & operator << (QDataStream & st, const Controls_display & obj);
QDataStream & operator >> (QDataStream & st, Controls_display & obj);

#endif // CONTROLS_DISPLAY_H
