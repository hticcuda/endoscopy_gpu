#ifndef FORM_SHORTCUT_H
#define FORM_SHORTCUT_H

#include <QWidget>
#include "QShortcut"

namespace Ui {
class Form_shortcut;
}

class Form_shortcut : public QWidget
{
    Q_OBJECT

public:
    explicit Form_shortcut(QWidget *parent = 0);
    ~Form_shortcut();

public slots:
    void show_shortcut_list();


private:
    Ui::Form_shortcut *ui;
};

#endif // FORM_SHORTCUT_H
