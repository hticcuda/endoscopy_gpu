#include "postprocessing.h"
#include <opencv2/opencv.hpp>
#include <algorithm>

cv::Mat Postprocessing::getSharpened() const
{
    return sharpened;
}

int Postprocessing::getUpper()
{
    return upper;
}

std::vector<uchar> Postprocessing::getLUT() const
{
    return LUT8U_auto_bright;
}

Postprocessing::Postprocessing()
{
    sigma = 1.2;
//    threshold = 2;

    LUT8U_auto_bright.assign(256,1);
}

std::vector<float> Postprocessing::getHistY1()
{
    return std::vector<float>(hist_Y[0].begin<float>(),hist_Y[0].end<float>());
}

std::vector<float> Postprocessing::getHistY2()
{
    return std::vector<float>(hist_Y[1].begin<float>(),hist_Y[1].end<float>());
}

void Postprocessing::applyLUT(cv::Mat & Y8)
{
    cv::LUT(Y8,LUT8U_auto_bright,Y8);
}

//void Postprocessing::applyLUT(cv::cuda::GpuMat & Y8)
//{
//    lut_d = cv::cuda::createLookUpTable(LUT8U_auto_bright);
//    lut_d->transform(Y8,Y8);
//}

void Postprocessing::apply_clahe(cv::Mat & image)
{
    //    //clahe->setTilesGridSize(cv::Size(320,240));
    //    //clahe->setClipLimit(2);
    //    cv::Ptr<cv::CLAHE> clahe = cv::createCLAHE();
    //    //cv::CLAHE clahe = cv::createCLAHE(4.0,cv::Size(2,2));
    ////    cv::Mat spl[3],splin,splout;
    ////    cv::split(image,spl);
    ////    splin = spl[0];
    //    clahe->setClipLimit(1.0);
    //    clahe->setTilesGridSize(cv::Size(8,6));
    //    clahe->apply(image,image);

    //    cv::resize(image,image,cv::Size(720,720));
    //    cv::equalizeHist(image,image);
    //    cv::resize(image,image,cv::Size(960,720));
}

void Postprocessing::image_sharpening(float sharp_value, float bgthreshold, const cv::Mat & image)
{
//    std::cout<<sharp_value<<std::endl;
    GaussianBlur(image, blurred, cv::Size(), sigma, sigma);
    lowContrastMask = abs(image - blurred) < bgthreshold;
    sharpened = image*(1+sharp_value) + blurred*(-sharp_value);
    image.copyTo(sharpened, lowContrastMask);
}

void sharpen_triscan(cv::Mat & channel, cv::Mat & output)
{
    cv::Mat blurry;
    cv::boxFilter(channel,blurry,-1,cv::Size(3,3));
    cv::Scalar meanv = cv::mean(channel);
    output = channel + 10*meanv.val[0]*blurry;
    cv::normalize(output,output,1,0,cv::NORM_L1);
    //return meanv.val[0];
}



void Postprocessing::enhance_triscan(cv::Mat & R, cv::Mat & G, cv::Mat & B, cv::Mat & rgb)
{
    //pass R,G,B without masking
    //tissuesurface
    cv::Mat R1, G1, B1;
    sharpen_triscan(R,R1);
    sharpen_triscan(G,G1);
    sharpen_triscan(B,B1);

    //mucosa
    float redmean = cv::mean(R1).val[0];

    float z = 0.2;
    float k = z + 0.5 * redmean;
    float g = 10 * log(2.3)* redmean;

    cv::Mat expterm;
    cv::exp(g*(k-R1),expterm);
    cv::Mat mucosa_enhanced_R = 1/(1+expterm);

    cv::Mat R8,G8,B8;
    mucosa_enhanced_R.convertTo(R8,CV_8UC1,255);
    G1.convertTo(G8,CV_8UC1,255);
    B1.convertTo(B8,CV_8UC1,255);

    //color tone
    cv::Mat R8e,G8e,B8e;
    cv::equalizeHist(R8,R8e);
    cv::equalizeHist(G8,G8e);
    cv::equalizeHist(B8,B8e);

    cv::Mat  arr [] = {R8e,G8e,B8e};
    cv::merge(arr,3,rgb);
}

void Postprocessing::hist_find_limits()
{
    lower = 0;
    upper_prev = upper;
    upper = hist_Y[0].rows-1;
    cv::Mat_<float>::const_iterator iter=hist_Y[0].begin<float>(),
            end = hist_Y[0].end<float>();

    cumhist_float.clear();
    float sum_i=0;
//    float maxvalue = 0;
    int xi = 0;//,maxindex=0;
    bool lower_found = false,
            upper_found = false;
    for(;iter!=end;++iter,++xi) {
        sum_i += *iter;
//        if(xi>10)
//        {
//            if(*iter>maxvalue)
//            {
//                maxvalue = *iter;
//                maxindex = xi;
//            }

//        }
        cumhist_float.push_back(sum_i);
        if(sum_i>0.01 && !lower_found) {
            lower = xi;
            lower_found = true;
        }
        if(sum_i>=0.95 && !upper_found) {
            upper = xi;
            upper_found = true;
        }
//        qDebug()<<"max index"<</maxindex;
    }
}

void Postprocessing::fixedLUT(float gamma) {
    //        den = upper-lower;
    //        if (den < 64)
    //            upper = 128;

    for(int i=0;i<64;++i){
        double i2 = pow((double(i))/64,gamma)*120.0;
        LUT8U_auto_bright[i]=cv::saturate_cast<uchar>(i2);
    }
    for(int i=64;i<128;++i)
        LUT8U_auto_bright[i]=cv::saturate_cast<uchar>(LUT8U_auto_bright[63]+(i-64)*2);

    double slope = double(255-LUT8U_auto_bright[127])/double(255-128);
    for(unsigned short i = 128; i < 256; ++i) {
        double i2 = LUT8U_auto_bright[127]+slope*(i-127+1);
        LUT8U_auto_bright[i] = cv::saturate_cast<uchar>(i2);
    }
}

void Postprocessing::update_lut_auto_bright(float brightness, float contrast,float gamma, bool auto_brightness_set, bool auto_brightness_peak)
{

    double den = 255;

    if(!auto_brightness_set)
    {
        upper = 255;
        lower = -19;
    }
    else
    {
//        fixedLUT(gamma);
//        return;

        upper = upper + 10;
//        if (gamma<1) lower = 10;
//        else
            lower = cv::max(0,lower - 7);

            den = upper-lower;

            if(auto_brightness_peak == true || den < 64)
            {
//                upper = cv::min(245,cv::max(lower+71,upper+31));
                upper = cv::max(70,upper);
                den = upper-lower;
            }

//            //unfinished upper prev use
//            int diff_upper = abs(upper - upper_prev);
//            if (!((upper > 64 && upper_prev > 64) || (upper < 64 && upper_prev < 64)))
//            {
//                if (upper > upper_prev){

//                }
//            }
            //...


    }



    for (unsigned short i = lower+20; i < upper; i++)
    {
        double i2 = pow((double(i))/den,gamma)*245.0;
        //if (i<lower) i2 = 0;
        //if(i>upper) i2 = 255;
        if(!auto_brightness_set)
            i2 = (i2 * contrast) + brightness;

        if(i2>245) i2=245;
        if(i2<0)i2=0;

        LUT8U_auto_bright[i] = static_cast<uchar>(i2);

    }

    LUT8U_auto_bright[0] = 0;

    double slope = double(LUT8U_auto_bright[lower+20])/(double(lower)+20);
    for(unsigned short i = 0 ; i < lower+20; ++i)
    {
//        double i2=slope*i;
        double i2= pow((double(i)/(double(lower)+20)),0.8)*(double(LUT8U_auto_bright[lower+20])+1);
        if(i2<0) i2 = 0;
        LUT8U_auto_bright[i] = static_cast<uchar>(i2);
    }
    slope = double(255-LUT8U_auto_bright[upper-1])/double(255-upper);
    for(unsigned short i = upper; i < 256; ++i) {
        double i2 = LUT8U_auto_bright[upper-1]+slope*(i-upper+1);
        if(i2>254) i2=255;
        LUT8U_auto_bright[i] = static_cast<uchar>(i2);
    }
    smooth_lut_auto_bright(255.0/den);
}

void Postprocessing::smooth_lut_auto_bright(float slope)
{
    cv::Mat lut(LUT8U_auto_bright), res;
    int sl = int(slope*2+1);
//    if(sl>10)
//        sl=9;
    if(sl%2==0)
        sl = sl + 1;

    cv::GaussianBlur(lut,res,cv::Size(1,sl),
                     slope/3.);

    LUT8U_auto_bright.assign(res.datastart,res.dataend);

}

void Postprocessing::findHist(cv::Mat histInput, cv::Mat & histCalc){
    //cv::resize(Ydisp,Y8,cv::Size(0,0),0.5,0.5,cv::INTER_AREA);
    int channels [] = {0};
    int histsize [] = {256};
    float yrange [] = {0,255};
    const float * ranges [] = {yrange};
    cv::calcHist(&histInput,1,channels,cv::Mat(),histCalc,1,histsize,ranges,true,false);
    cv::normalize(histCalc,histCalc,1,0,cv::NORM_L1);
}

void Postprocessing::update_hist(const cv::Mat & Y8,short sequencenum)
{
    //cv::resize(Ydisp,Y8,cv::Size(0,0),0.5,0.5,cv::INTER_AREA);

//    int channels [] = {0};
//    int histsize [] = {256};
//    float yrange [] = {0,255};
//    const float * ranges [] = {yrange};
//    cv::calcHist(&Y8,1,channels,cv::Mat(),hist_Y[0],1,histsize,ranges,true,false);


//    cv::normalize(hist_Y[0],hist_Y[0],1,0,cv::NORM_L1);
    findHist(Y8, hist_Y[0]);

}

void Postprocessing::update_hist2(const cv::Mat & altY)
{
//    int channels [] = {0};
//    int histsize [] = {256};
//    float yrange [] = {0,255};
//    const float * ranges [] = {yrange};
//    cv::calcHist(&altY,1,channels,cv::Mat(),hist_Y[1],1,histsize,ranges,true,false);

//    cv::normalize(hist_Y[1],hist_Y[1],1,0,cv::NORM_L1);
    findHist(altY, hist_Y[1]);
}

void Postprocessing::image_zoom(cv::Mat &rgb,float zoomval)
{
    cv::Mat rgb_temp;
    cv::resize(rgb,rgb_temp,cv::Size(0,0),zoomval,zoomval);

    cv::Size sizeval=rgb.size();
    int rsize=sizeval.height * zoomval;
    int csize=sizeval.width  * zoomval;

    int ysub = (rsize - sizeval.height) / 2;
    int xsub = (csize - sizeval.width) / 2;

    if(zoomval>1)
    {
        rgb_temp(cv::Rect(xsub,ysub,sizeval.width,sizeval.height)).copyTo(rgb);
    }
    else{
        rgb.setTo(0);
        rgb_temp.copyTo(rgb(cv::Rect(-xsub,-ysub,rgb_temp.cols,rgb_temp.rows)));
    }

}

void Postprocessing::quantizehist(unsigned char nbins)
{
    cv::Mat_<float>::const_iterator iter=hist_Y[1].begin<float>(),
            end = hist_Y[1].end<float>();
    int binsiz = hist_Y[1].rows/nbins;
    if(hist_Y_quantized.size()!=nbins)
    {
        hist_Y_quantized.clear();
        hist_Y_quantized.resize(nbins);
    }
    else
    {
        hist_Y_quantized.assign(4,0);
    }
    int ii=0;
    for(;iter!=end; ++iter,ii++)
    {
        int currentbin = ii/binsiz; //integer div
        hist_Y_quantized[currentbin]+=*iter;
    }

}

void Postprocessing::printhist()
{
    std::cerr << "["
             << hist_Y_quantized[0]<< " "
             << hist_Y_quantized[2] << " "
             << hist_Y_quantized[1] << " "
             << hist_Y_quantized[3] << "]" << std::endl;
}

int Postprocessing::compute_nextvga(int vga) {
//    std::cout<<"compute_nextvga"<<vga<<std::endl;
    const int MAXVGA = 511;

    if(hist_Y_quantized[3]>0.2 || hist_Y_quantized[3]>hist_Y_quantized[2]) {
        // then reduce vga
//        std::cout<<"1111111"<<std::endl;

        return vga/2;
    }
    if(hist_Y_quantized[0]>0.7) {
//        std::cout<<"222222"<<std::endl;

        return (vga+MAXVGA)/2;
    }
//    std::cout<<"abss"<<fabs(hist_Y_quantized[1]-hist_Y_quantized[2])<<std::endl;
    if(fabs(hist_Y_quantized[1]-hist_Y_quantized[2])<0.005 && hist_Y_quantized[1]>=0.2)
    {
//        std::cout<<"333333"<<std::endl;

        return vga;
    }
    return vga;

//    std::cout<<"return value"<<vga<<std::endl;


//    int nextvga = (currentvgarange[1]+currentvgarange[0])/2;

//    if(hist_Y_quantized[3]>0.01)
//    {
//        currentvgarange[1]=nextvga;
//    }
//    if(hist_Y_quantized[2]<0.01)
//    {
//        currentvgarange[0]=nextvga;
//    }

//    if(currentvgarange[0]==currentvgarange[1])
//        auto_sensor_on=false;
//    return nextvga;

}

float Postprocessing::dimmingFactor(const cv::Mat & ROI)
{
    cv::Scalar mu = cv::mean(ROI);
    float xk = (mu.val[0]-lower)/(upper-lower);

    float g = 12, k = 0.7;

    return 1-1/(1+cv::exp(g*(k-xk)));
}

void Postprocessing::wb_apply(cv::Mat & BGR/*, float r_gain, float b_gain*/)
{
    cv::Mat channels[3];
   cv::Mat im_out[3];
    cv::split(BGR,channels);
    cv::Mat im_temp;
    float zero,one,two,three,four,five,six,seven,eight;
std::ifstream fileread;
fileread.open ("CATConfig.txt");
        fileread>>zero>>one>>two>>three>>four>>five>>six>>seven>>eight;    //read matrix value from file
fileread.close();

    //R
    cv::addWeighted(channels[0], two, channels[1], one, 0, im_temp);         //merge the matrix value with the input frame
    cv::addWeighted(im_temp, 1, channels[2], zero /*+ r_gain*/, 0, im_out[2]);
    // G
    cv::addWeighted(channels[0], five, channels[1], four, 0, im_temp);
    cv::addWeighted(im_temp, 1, channels[2], three, 0, im_out[1]);
    // B
    cv::addWeighted(channels[0], eight /*+ b_gain*/, channels[1], seven, 0, im_temp);
    cv::addWeighted(im_temp, 1, channels[2], six, 0, im_out[0]);
    // Modified RGB image
    cv::merge(im_out, 3, BGR);
}



bool Postprocessing::wb_calculate(cv::Mat input)
{
     cv::Mat im_orig;
     double b = 0.001;
     im_orig = input;
     im_orig.convertTo(im_orig, CV_32FC3);
     cv::Mat mat1, mat2, mat3, mat4, mat5, mat6, mat7, mat8, mat9, mat10, mat11, mat12, mat13, mat14, mat15, matrix;
     cv::Mat xfm_cat02 = (cv::Mat_<double>(3,3) << 0.7328, 0.4296, -.1624, -.7036, 1.6975, 0.0061, 0.0030, 0.0136, 0.9834);
     cv::Mat sRGBtoXYZ = (cv::Mat_<double>(3,3) << 0.4124564, 0.3575761, 0.1804375, 0.2126729, 0.7151522, 0.0721750, 0.0193339, 0.1191920, 0.9503041);
     cv::Mat inv_sRGBtoXYZ = sRGBtoXYZ.clone().inv();
     cv::Mat imRGB;


     for (int iter=1; iter<=15; iter++)
    {

        cv::Mat im_RGB[3];
        cv::split(im_orig.clone(),im_RGB);

        cv::Mat im_temp;
        cv::Mat im_out[3];

        // Input Frame Modification with Gain Values
        cv::Mat outMat;
        cv::Mat d_gain;

        // ------------------- Color conversion BGR to YUV --------------------
        cv::Mat img_cpu, img_cpu1;
        img_cpu = im_orig;
        img_cpu1 = im_orig;

         // ---------------------------    Y Component    ------------------------
        cv::Mat im_RGB1[3];
        cv::split(img_cpu1.clone(),im_RGB1);
        cv::Mat im_RGB1_1, im_RGB2_1;
        cv::addWeighted(im_RGB1[0], 0.114, im_RGB1[1], 0.587, 0, im_RGB1_1);
        cv::addWeighted(im_RGB1_1, 1, im_RGB1[2], 0.299, 0, im_RGB2_1);
        cv::transpose(im_RGB2_1.clone(),im_RGB2_1);

        // ---------------------------    U Component    ------------------------
        cv::Mat im_RGB2[3];
        cv::split(img_cpu1.clone(),im_RGB2);
        cv::Mat im_RGB1_2, im_RGB2_2;
        cv::addWeighted(im_RGB2[0], 0.886, im_RGB2[1], -0.587, 0, im_RGB1_2);
        cv::addWeighted(im_RGB1_2, 1, im_RGB2[2], -0.299, 0, im_RGB2_2);
        cv::transpose(im_RGB2_2.clone(),im_RGB2_2);

        // ---------------------------    V Component    -------------------------
        cv::Mat im_RGB3[3];
        cv::split(img_cpu1.clone(),im_RGB3);
        cv::Mat im_RGB1_3, im_RGB2_3;
        cv::addWeighted(im_RGB3[0], -0.114, im_RGB3[1], -0.587, 0, im_RGB1_3);
        cv::addWeighted(im_RGB1_3, 1, im_RGB3[2], 0.701, 0, im_RGB2_3);
        cv::transpose(im_RGB2_3.clone(),im_RGB2_3);




        // --------------------- Gray Chromaticity   (F = (|U|+|V|)/Y)) -----------

        cv::Mat U(400,400, CV_32FC1);

        cv::Mat V(400,400, CV_32FC1);

        cv::Mat UV(400,400, CV_32FC1);

        cv::Mat F(400,400, CV_32FC1);

        U = cv::abs(im_RGB2_2);
        V = cv::abs(im_RGB2_3);

       double minVal;
    double maxVal;
cv::Point minLoc;
cv::Point maxLoc;

cv::minMaxLoc( U, &minVal, &maxVal, &minLoc, &maxLoc );

//cout << "min val : " << minVal << endl;
//cout << "max val: " << maxVal << endl;

        cv::add(U,V,UV);

        cv::divide(UV,im_RGB2_1,F);

        // ----------------- for all (F<T) in image find mean of U and V------------
        cv::Mat thr,UU, VV;

        cv::threshold(F,thr,0.3,1,cv::THRESH_BINARY_INV);

        double thrSum = cv::sum(thr)[0];

      // cout<<thrSum<<endl;

        cv::multiply(im_RGB2_2.clone(),thr,UU);
        cv::multiply(im_RGB2_3.clone(),thr,VV);
       double U1, V1, U_bar, V_bar;

        U1 = cv::sum(UU)[0];
        V1 = cv::sum(VV)[0];

        U_bar = U1/thrSum;

        V_bar = V1/thrSum;

//      cout << "V_bar: "<<V_bar;

        //..............................rgbEst......................................
        double rgbEst1 = (100+V_bar);
        double rgbEst2 = (100+U_bar*-0.1942078364565588+V_bar*-0.5093696763202725);
        double rgbEst3 = (100+U_bar);

        //.................... xyz = sRGBtoXYZ * rgbEst..............................
        double xyz1 = (rgbEst1*0.4124564+rgbEst2*0.3575761+rgbEst3*0.1804375);
        double xyz2 = (rgbEst1*0.2126729+rgbEst2*0.7151522+rgbEst3*0.072175);
        double xyz3 = (rgbEst1*0.0193339+rgbEst2*0.119192+rgbEst3*0.9503041);

        //..................... Calculate xy chromaticity............................
        double s = xyz1+xyz2+xyz3;
        double xy1 = xyz1/s;
        double xy2 = xyz2/s;

        //.................... Normalize Y to 100 so D65 luminance comparable........
        double xyzEst1 = 100/xy2*xy1;
        double xyzEst2 = 100;
        double xyzEst3 = 100/xy2*(1-xy1-xy2);

        //.................gain = ((xfm*xyz_target) ./ (xfm*xyz_est))................

        double B_Gainn = (94.9232)/(0.7328*xyzEst1+0.4296*xyzEst2+(-0.1624)*xyzEst3);
        double G_Gainn = (103.544)/((-0.7036)*xyzEst1+(1.6975)*xyzEst2+0.0061*xyzEst3);
        double R_Gainn = (108.718)/((0.0030)*xyzEst1+(0.0136)*xyzEst2+0.9834*xyzEst3);



        d_gain = (cv::Mat_<double>(3,3) << B_Gainn, 0, 0, 0, G_Gainn, 0, 0, 0, R_Gainn);
        cv::solve(xfm_cat02,(d_gain*xfm_cat02), outMat);
        outMat = inv_sRGBtoXYZ * outMat.clone() * sRGBtoXYZ;
        if(iter==1){
            mat1 = outMat;
        }
        else if(iter==2){
            mat2 = outMat;
        }
        else if(iter==3){
            mat3 = outMat;
        }
        else if(iter==4){
            mat4 = outMat;
        }
        else if(iter==5){
            mat5 = outMat;
        }
        else if(iter==6){
            mat6 = outMat;
        }
        else if(iter==7){
            mat7 = outMat;
        }
        else if(iter==8){
            mat8 = outMat;
        }
        else if(iter==9){
            mat9 = outMat;
        }
        else if(iter==10){
            mat10 = outMat;
        }
        else if(iter==11){
            mat11 = outMat;
        }
        else if(iter==12){
            mat12 = outMat;
        }
        else if(iter==13){
            mat13 = outMat;
        }

        else if(iter==14){
            mat14 = outMat;
        }
        else{
            mat15 = outMat;
        }

        // R
        cv::addWeighted(im_RGB[0], outMat.at<double> (2), im_RGB[1], outMat.at<double> (1), 0, im_temp);
        cv::addWeighted(im_temp, 1, im_RGB[2], outMat.at<double> (0), 0, im_out[2]);
        // G
        cv::addWeighted(im_RGB[0], outMat.at<double> (5), im_RGB[1], outMat.at<double> (4), 0, im_temp);
        cv::addWeighted(im_temp, 1, im_RGB[2], outMat.at<double> (3), 0, im_out[1]);
        // B
        cv::addWeighted(im_RGB[0], outMat.at<double> (8), im_RGB[1], outMat.at<double> (7), 0, im_temp);
        cv::addWeighted(im_temp, 1, im_RGB[2], outMat.at<double> (6), 0, im_out[0]);

        // Modified RGB image
        cv::merge(im_out, 3, imRGB);

        imRGB.clone().copyTo(im_orig);



 }
   matrix =  mat15 * mat14 * mat13 * mat12 * mat11 * mat10 * mat9 * mat8 * mat7 * mat6 * mat5 * mat4 * mat3 * mat2 * mat1;
//   std::cout<<"\n"<<"Matrix : "<<matrix<<std::endl;
   std::ofstream cfgfile;                              //write matrix into CATConfig.txt file
   cfgfile.open ("CATConfig.txt");
   for(int i=0;i<3;i++)
   {
       for(int j=0;j<3;j++)
       {
           cfgfile <<matrix.at<double>(i,j)<<" ";
//                qDebug()<<"hello";
       }

   }
   cfgfile.close();                    //calculates white balance matrix only once
  return true;
}



