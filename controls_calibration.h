#ifndef CONTROLS_CALIBRATION_H
#define CONTROLS_CALIBRATION_H

#include <QDataStream>

class Controls_calibration
{



public:
    int rg,shd,shd_width,shp,h1,h2;

    void reset();
//    explicit Controls_calibration(QObject *parent = nullptr);

    void copyFrom(const Controls_calibration & obj);

};

QDataStream & operator << (QDataStream & st, const Controls_calibration & obj);
QDataStream & operator >> (QDataStream & st, Controls_calibration & obj);

#endif // CONTROLS_CALIBRATION_H
