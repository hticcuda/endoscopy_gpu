#ifndef FORM_H
#define FORM_H

#include <QWidget>
#include <uvccommunication.h>
#include "form_uvc.h"
#include "QPushButton"
#include <QLabel>
#include <framecontext.h>
#include "sliderrangemap.h"
#include <QFile>
#include <QTextStream>
#include <QDir>
#include <controls_calibration.h>
#include <controls_display.h>
#include <controls_sensor.h>
#include <patientwindow.h>
#include <QtConcurrent/QtConcurrentRun>
#include <QFutureWatcher>
#include "form_shortcut.h"
#include "codec.h"
#include "preference.h"
#include "external_input.h"
#include "hotplug.h"
#include "button_interface.h"
//#include "userselect.h"
#include <QRadioButton>
#include "gpu_qrend.h"


namespace Ui {
class Form;
}

class StateCycle {
    int numstates;
    int state;
public:
    void init(int nstates=3,int startstate=0) {
        numstates = nstates;
        state = startstate;
    }
    void change() {
        state = (state+1)%numstates;
    }
    int get(){
        return state;
    }
};
class UserSelect
{
public:
    int led_value1,led_value2,led_state,exposture,vga_gain,black_level_clamp,h1,h2,shd,shp,rg,brightness,threshold;
    float contrast,gamma,redg,blueg,chroma,sharpness;
};


class Form : public QWidget
{
    Q_OBJECT

    UvcCommunication *uvc;
    Codec * codecproc;
    FrameContext *frame;
    External_input *ext_int;
    Form_uvc * uvcform;
    SliderRangeMap rangemap;
    Controls_sensor sens;
    Controls_display disp;
    Controls_calibration calib;
    PatientWindow * patientwindow;
    Form_shortcut * key_short;
    Preference pref;
    UserSelect user;
    gpu_qrend *frend;



    StateCycle ledstate,alc_aec_state;

    SliderParam currentslider;

    QTimer * timer_led_stepping;
    QTimer * timer_wb_click;
#if led_ramp == 1
    QTimer *timer_hotplug = new QTimer(this);
#endif
    int disconnect_count = 0;
    unsigned long framenumber;
    int count=0,count_store=0,count_new_value;
    QString filen,filename;
    QPushButton *selected_push_button;
public:
    explicit Form(FrameContext *frameobj,External_input *extint_obj,gpu_qrend *frendobj, QWidget *parent = 0);

    void manage_slider_change(SliderParam slidername, int value);

    QFutureWatcher<void> watcher;
    HotPlug * htplg;
    Button_interface * btn_int;
#if led_ramp == 1
    int count1 = 0;
#endif

    ~Form();

public slots:
    void start_camera_streaming();
#if led_ramp ==1
    void led_increase();
#endif

signals:
    void clicked(int b);

    void close_mainwidget(int a);

    void close_mainwidget_signal(int a);

    void clicked_image();

private slots:

    void Led_stepping_start();

    void on_enginerring_settings_clicked();

    void on_led_pushButton_clicked();

    void change_stylesheets(QPushButton *Button, int option, QString icon_name, QFrame *line, QLabel *label,QString label_ntext);

    void on_Zoom_pushbutton_clicked();

    void remove_status_messsage_after_timeout();

    void on_mask_pushbutton_clicked();

    void on_freeze_pushButton_clicked();

    void on_save_video_pushButton_clicked();

    void on_save_image_pushButton_clicked();

    void on_home_pushButton_clicked();

    void on_pip_position_selection_clicked();

    void on_pip_size_pushButton_clicked();

    void on_image_settings_pushButton_clicked();

    void on_display_control_pushButton_clicked();

    void on_exposure_slider_valueChanged(int value);

    void on_black_clamp_level_slider_valueChanged(int value);

    void on_vga_gain_slider_valueChanged(int value);

    void on_preset_pushButton_clicked();

    void on_brightness_pushButton_clicked();

    void enable_disable_all_preset_buttons(bool state);

    void enable_disable_preset_slider_set(bool state);

    void on_preset_slider_valueChanged(int value);

    void on_contrast_pushButton_clicked();

    void on_gamma_pushButton_clicked();

    void on_red_gain_pushButton_clicked();

    void on_blue_gain_pushButton_clicked();

    void on_sharpness_pushButton_clicked();

    void on_threshold_pushButton_clicked();

    void on_chroma_pushButton_clicked();

    void on_saturation_pushButton_clicked();

    void on_preset1_pushButton_clicked();

    void on_save_preset_pushButton_clicked();

    void on_preset2_pushButton_clicked();

    void on_preset3_pushButton_clicked();

    void on_preset4_pushButton_clicked();

    void preset_selection_enable_disable_buttons(bool state_change);

    void preset_read_and_apply(QString preset_selection);

    void initialize_display();

    void on_auto_contrast_pushButton_clicked();

    void camera_status_update(bool status);

    void on_white_balance_pushbutton_2_clicked();

    void on_patient_info_pushButton_clicked();

    void on_alc_aec_pushbutton_clicked();

    void on_flipx_pushButton_clicked();

    void on_flipy_pushButton_clicked();

    void show_shortcut_list();

    void on_image_resize_pushButton_clicked();

    void on_mblu_pushButton_2_clicked();

    void on_auto_vga_button_clicked();

    void close_application();

    void on_hdmi_in_pushButton_clicked();

    void change_led();

    void on_save_preference_clicked();

    void on_delete_preference_clicked();

    void on_apply_preference_clicked();

    void on_user_options_pushButton_clicked();

    void on_preference_pushButton_1_clicked();

    void on_preference_pushButton_2_clicked();

    void on_preference_pushButton_3_clicked();

    void on_preference_pushButton_4_clicked();

    void on_preference_pushButton_5_clicked();

    void on_preference_pushButton_6_clicked();

    void on_preference_pushButton_7_clicked();

    void on_preference_pushButton_8_clicked();

    void on_preference_pushButton_clicked();

    void on_preset1_pushButton_6_clicked();

    void on_pref_pip_size_pushButton_clicked();

    void on_pref_pip_position_pushButton_clicked();

    void on_pref_gain_r_pushButton_clicked();

    void on_pref_gain_b_pushButton_clicked();

    void on_pref_image_preset_pushButton_clicked();

    void on_pref_mask_pushButton_clicked();

    void close_mainwidget_slot();

    void on_illum_pushButton_clicked();

    void on_deinter_pushButton_clicked();

    void on_toggle_checkbox_blue_white_stateChanged(int arg1);

    void led_auto_control_sync(int auto_control_state);

    void auto_control_led_sync(int led_state);

    void on_pump_pushButton_2_clicked();

    void on_save_frame_pushButton_clicked();

    void button_mapping(int button);

 #if led_ramp == 1
    void on_LED_RAMP_pushButton_clicked();
#endif

    void on_button_pushbutton_clicked();

    void on_remove_options_pushButton_clicked();

    void on_button1_pushButton_clicked();

    void on_button2_pushButton_clicked();

    void on_button3_pushButton_clicked();

    void on_radioButton_clicked();

    void on_radioButton_2_clicked();

    void on_radioButton_3_clicked();

    void on_radioButton_4_clicked();

    void on_radioButton_5_clicked();

    void on_radioButton_6_clicked();

    void on_radioButton_7_clicked();

    void on_radioButton_8_clicked();

    void on_set_button_pushButton_clicked();


    void on_caliberation_pushButton_clicked();

    void on_zoom_slider_valueChanged(int value);

public slots:

    void on_led_brightness_slider_valueChanged(int value);

    void on_led_brightness_slider_2_valueChanged(int value);

private:
    Ui::Form *ui;

    void set_sliders_value();
    void sensor_initialization();
    void calibration_intialization();
    void change_led_dimfactor();
    void dynamic_radio_button_name_change(QPushButton *button, QRadioButton *radio);
};

#endif // FORM_H
