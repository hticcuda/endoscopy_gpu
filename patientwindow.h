#ifndef PATIENTWINDOW_H
#define PATIENTWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <patient_information.h>
#include <form_shortcut.h>
#include <framecontext.h>

namespace Ui {
class PatientWindow;
}

class PatientWindow : public QMainWindow
{
    Q_OBJECT

    Patient_Information patient_info;
    Form_shortcut * shortcut;
    FrameContext * frame;
    QString patient_id,patient_name,patient_dob,patient_gender,patient_comments,patient_age;

public:
    explicit PatientWindow(FrameContext *frameobj, QWidget *parent = 0);
    ~PatientWindow();

private slots:
    void on_patient_save_pushButton_clicked();

    void on_patient_listWidget_currentTextChanged(const QString &currentText);

    void on_new_patient_create();

    void on_clear_patients_info();

    void on_edit_patient_info();

    void on_patient_reset_pushButton_clicked();

    void on_patient_set_pushButton_clicked();

    void show_shortcut_list_on_display();

    void remove_focus();

    void delete_delected_patient_info();


private:
    Ui::PatientWindow *ui;
//    QPushButton *create_pushbutton;
};

#endif // PATIENTWINDOW_H
