#ifndef UVCCOMMUNICATION_H
#define UVCCOMMUNICATION_H

#include <QObject>
#include <libuvc/libuvc.h>
#include <QTimer>
#include <iostream>
#include <debug.h>
#include <libusb-1.0/libusb.h>
#include <gpu_qrend.h>

class Codec ;

enum UVCparam {
    BRIGHTNESS1,
    VGA,
    BLACK_CLAMP_LEVEL,
    EXPOSURE,
    ALCAEC,
    BRIGHTNESS2,
    BLUE_LED
};

enum Calibparam {
    RG,
    SHD,
    SHD_WIDTH,
    SHP,
    H1,
    H2,
    THRESHOLD
};


class UvcCommunication : public QObject
{
    Q_OBJECT
    uvc_context_t *ctx;
    uvc_stream_ctrl_t ctrl;
    uvc_device_t *dev;
    uvc_device_handle_t *devh;
    struct libusb_device_handle *usb_devh;
    gpu_qrend * urend;

//     CodecProcessing * codecproc;

    unsigned short fieldwidth = 962;
#if raw == 1
    unsigned short fieldheight = 383;
#else
    unsigned short fieldheight = 766;
#endif
    unsigned long fieldcount = 0;
    bool m_initialized;
    bool m_opened;
    bool m_ready;
    bool m_streaming;
    QTimer *timer = new QTimer(this);
    bool status_window=false;
    int count = 0;
//    cv::Mat hotplug_image;
    bool close_window_flag = false;
    bool restart_camera_streaming = false;
    bool device_status = false;
    int button_pressed = 0;
    bool usb_ready = false;

public:
    explicit UvcCommunication(gpu_qrend * urendObj, QObject *parent = nullptr);
    ~UvcCommunication();
    bool start_streaming(Codec * const cpobj);
    void stop_streaming();
    void send_param(UVCparam uvcparamname, int value);
    void send_calib(Calibparam calibparamname, int value);
    bool ready() const;

//    bool time_start= false;


    libusb_device_handle *getUsb_devh() const;

    void poll_for_device(int interval=1000);

    void setOpened(bool opened);

    void setInitialized(bool initialized);

    bool getReady() const;
    void setReady(bool ready);

    bool getClose_window_flag() const;
    void setClose_window_flag(bool value);

    bool getRestart_camera_streaming() const;
    void setRestart_camera_streaming(bool value);

    bool getDevice_status() const;
    void setDevice_status(bool value);

    int getButton_pressed() const;
    void setButton_pressed(int value);

    bool getUsb_ready() const;
    void setUsb_ready(bool value);

public slots:
    void open_camera();

signals:
    void camera_status(bool);
    void start_camera_hotplug(bool ready2);
    void close_camera_window();
    void restart_camera();
    void buttonpressed(int button);
    void hitplug();

private slots:
//    void open_camera();

public slots:
};

#endif // UVCCOMMUNICATION_H
