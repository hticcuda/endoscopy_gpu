#include "form.h"
#include "ui_form.h"
#include <QShortcut>
#include <QProcess>

int zoom_btn_press_cnt=0,enhance_btn_press_cnt=0,mask_btn_press_cnt=0;
int alc_aec_btn_press_cnt=0,flipx_btn_press_cnt=0,flipy_btn_press_cnt=0,save_video_btn_press_cnt=0;
int prefreeze_btn_press_cnt=0;
int slider_value=0, counts = 0;
SliderParam sliderchange;
int preset_bright_prs_cnt=0,preset_contrast_prs_cnt=0,preset_gamma_prs_cnt=0,preset_red_gain_prs_cnt=0;
int brightness_slider_val=0,contrast_slider_val=0,gamma_slider_val=0,red_gain_slider_value=0;
int preset_blue_gain_prs_cnt=0,preset_sharp_prs_cnt=0,preset_threshold_prs_cnt=0,preset_chroma_prs_cnt=0,preset_saturation_prs_cnt=0;
int blue_gain_slider_val=0,sharpness_slider_val=0,threshold_slider_val=0,chroma_slider_val=0,saturation_slider_val=0;
int preset1_btn_prs_cnt=0,preset2_btn_prs_cnt=0,preset3_btn_prs_cnt=0,preset4_btn_prs_cnt=0;
bool load_from_display_bin = false;
QString preset_selection_file_name,selected_button_text,selected_radio_button_text;
int pip_size_btn_prs_cnt = 0;
int selected_button = 0,button1_prs_cnt = 0,button2_prs_cnt = 0,button3_prs_cnt = 0;
int pip_position_btn_prs_cnt=0,image_size_press_cnt=0;
bool preset1_state = false,preset2_state = false,preset3_state=false,preset4_state= false;
int hdmi_in_prs_cnt = 0,wb_cnt = 0;
QString preference_file_option;
int mask_pref_btn_prs_cnt = 0,pip_size_pref_btn_prs_cnt = 0;
int pip_position_pref_btn_prs_cnt = 0,r_gain_pref_btn_prs_count = 0, b_gain_pref_btn_prs_cnt = 0;
int preset_pref_btn_prs_cnt = 1,save=0;
std::string selection1 ,selection2 ,selection3 ;
//std::string radio_select ;//, radio2 , radio3 , radio4 , radio5 , radio6 , radio7 , radio8 , radio9 ;

uint sh = 0, sw = 0;
Form::Form(FrameContext *frameobj ,External_input *extint_obj,gpu_qrend *frendobj,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Form)
{
    codecproc = new Codec();
    ui->setupUi(this);
    if( !QDir("Saved_Frames/").exists())
    {
        QDir().mkdir("Saved_Frames");
    }
    frend = frendobj;
    uvc = new UvcCommunication(frend);

    camera_status_update(uvc->getReady());
    frame = frameobj;
    ext_int = extint_obj;

    htplg = new HotPlug(frend,uvc);
    htplg->start();
    btn_int = new Button_interface(uvc);
    btn_int->start();

//    timer_hotplug = new timerEvent();
    ui->menu_options_stackwidget->setCurrentIndex(0);
    new QShortcut(QKeySequence(Qt::Key_S), this, SLOT(start_camera_streaming()));
    new QShortcut(QKeySequence(Qt::Key_Q), this, SLOT(close_application()));
    new QShortcut(QKeySequence(Qt::Key_H), this, SLOT(show_shortcut_list()));
    new QShortcut(QKeySequence(Qt::Key_P), this, SLOT(on_patient_info_pushButton_clicked()));
    new QShortcut(QKeySequence(Qt::Key_L), this, SLOT(Led_stepping_start()));
    new QShortcut(QKeySequence(Qt::Key_R), this, SLOT(r_key_press()));
    int id=qRegisterMetaType<YCbCr_container>("YCbCr_container");
    connect(codecproc, SIGNAL(frameReady(YCbCr_container, short)), frame, SLOT(receive_ycbcr(YCbCr_container,short)),Qt::QueuedConnection);
    set_sliders_value();
    connect(uvc,SIGNAL(restart_camera()),this,SLOT(start_camera_streaming()));
    connect(uvc,SIGNAL(close_camera_window()),this,SLOT(close_mainwidget_slot()));
    connect(uvc,SIGNAL(buttonpressed(int)),this,SLOT(button_mapping(int)));
    ledstate.init(4,3);
    alc_aec_state.init(4);
    on_led_pushButton_clicked();
#if led_ramp == 1
    connect(timer_hotplug, SIGNAL(timeout()), this, SLOT(led_increase()));
#endif
}

void Form::Led_stepping_start() {

    std::ofstream meanfile("Y_mean_file.csv",std::ofstream::out);
    meanfile.close();
    timer_led_stepping = new QTimer(this);
    frame->setLed_stepping(true);
    connect(timer_led_stepping, SIGNAL(timeout()), this, SLOT(change_led()));
    timer_led_stepping->start(400);
    std::cout<<"Led_stepping_start"<<std::endl;
}

#define MIN_LED 75

void Form::change_led(){
    static int lInd = MIN_LED;
    static int lInd2 = MIN_LED;
    if(lInd <= 255)
    {
        on_led_brightness_slider_valueChanged(lInd);
        on_led_brightness_slider_2_valueChanged(0);
        frame->setNew_led_setting(lInd);
        lInd ++;
    }
    else if(lInd2 <= 255)
    {
        on_led_brightness_slider_valueChanged(0);
        on_led_brightness_slider_2_valueChanged(lInd2);
        frame->setNew_led_setting(lInd2);
        lInd2 ++;
    }
    else{
        frame->setLed_stepping(false);
        timer_led_stepping->stop();
        lInd = MIN_LED;
        lInd2 = MIN_LED;
//        frame->setNew_led_setting(0);
    }

}

void Form::change_led_dimfactor(){
    if(frame->getWhite_balance_click()){
        std::pair<float,float> dimFactorLed = frame->getDim_factor();
        on_led_brightness_slider_valueChanged(sens.brightness1 * dimFactorLed.first);
        on_led_brightness_slider_2_valueChanged(sens.brightness2 * dimFactorLed.second);
    }
    else
        timer_wb_click->stop();
}

Form::~Form()
{
    delete ui;
}


void Form::close_application()
{
    uvc->send_param(ALCAEC,0);
    uvc->send_param(BLUE_LED,1);
    uvc->send_param(BRIGHTNESS1,0);
    uvc->send_param(BRIGHTNESS2,0);
    uvc->send_param(BLUE_LED,0);
    uvc->send_param(BRIGHTNESS1,0);
    uvc->send_param(BRIGHTNESS2,0);
    QApplication::closeAllWindows();

        htplg->exit = false;
        btn_int->exit = true;
//    htplg->wait();
}


void Form::show_shortcut_list()
{
    key_short = new Form_shortcut();
    key_short->show();
}

void Form::start_camera_streaming()
{
    on_preset1_pushButton_clicked();
    uvc->send_param(BLUE_LED,0);
    bool ready = uvc->getReady();
    camera_status_update(ready);

    if(ready)
    {
        uvc->start_streaming(codecproc);
        calibration_intialization();
        sensor_initialization();
        emit clicked(2);

    }
    else {
//        uvc->poll_for_device(1000);
    }

}

#if led_ramp ==1
void Form::led_increase()
{
    count1 = count1 + 1;
    if(count1<256)
    {
        uvc->send_param(BRIGHTNESS1,count1);
        uvc->send_param(BRIGHTNESS2,count1);
        if(count1 == 255)
        {
            count1 = 0;
        }
    }
}
#endif

void Form::calibration_intialization()
{
    QString user_settings = QDir::currentPath()+"/calib.bin";
    if (QFileInfo::exists(user_settings)){
        QFile file(user_settings);
        file.open(QIODevice::ReadOnly | QIODevice::Text);
        QDataStream in_all(&file);
        in_all>>calib;
        file.close();
    }

    uvc->send_calib(RG,calib.rg);
    uvc->send_calib(SHD,calib.shd);
    uvc->send_calib(SHD_WIDTH,calib.shd_width);
    uvc->send_calib(SHP,calib.shp);
    uvc->send_calib(H1,calib.h1);
    uvc->send_calib(H2,calib.h2);

    user.rg = calib.rg;
    user.shd = calib.shd;
    user.shp = calib.shp;
    user.h1 = calib.h1;
    user.h2 = calib.h2;

}



void Form::sensor_initialization()
{
    QString user_settings = QDir::currentPath()+"/sens.bin";
    if (QFileInfo::exists(user_settings))
    {
        QFile file(user_settings);
        file.open(QIODevice::ReadOnly | QIODevice::Text);
        QDataStream in_all(&file);
        in_all>>sens;
        file.close();
    }
    else{
        sens.reset();
    }


    on_led_brightness_slider_valueChanged(sens.brightness1);
    on_led_brightness_slider_2_valueChanged(sens.brightness2);
    on_vga_gain_slider_valueChanged(sens.variable_gain);
    on_exposure_slider_valueChanged(sens.exposure);
    on_black_clamp_level_slider_valueChanged(sens.blacklev);

}

void Form::set_sliders_value()
{
    ui->led_brightness_slider->setRange(0,255);
    ui->led_brightness_slider_2->setRange(0,255);
    ui->vga_gain_slider->setRange(0,1023);
    ui->black_clamp_level_slider->setRange(0,1023);
    ui->exposure_slider->setRange(0,255);

    sensor_initialization();


//    ui->gamma_slider->setRange(0,36);
//    counts = rangemap.add(ui->gamma_slider, 0.2f, 2.0f, 0.05f);
    counts = rangemap.add(I_GAMMA,0.2f, 2.0f, 0.05f);

//    counts = rangemap.add(ui->exposure_slider, 0.f, 255.f, 15.f);
    counts = rangemap.add(S_EXPOSURE,0.f,255.f, 15.f);

//    counts = rangemap.add(ui->sharpness_slider, 0.f, 6.f, 0.3f);
    counts = rangemap.add(I_SHARPNESS, 0.f, 6.f, 0.3f);

//    counts = rangemap.add(ui->red_gain_slider,0.2f,2.f,0.2f);
    counts = rangemap.add(I_GAIN_R,0.5,1.5,0.1);

//    counts = rangemap.add(ui->green_gain_slider, 0.2f, 2.f, 0.2f);
    counts = rangemap.add(I_GAIN_B,0.5,1.5,0.1);

//    ui->contrast_slider->setRange(0,10);
//    counts = rangemap.add(ui->contrast_slider,0.5f,3.f,0.25f);
    counts = rangemap.add(I_CONTRAST,0.5f,3.f,0.25f);

//    counts = rangemap.add(ui->saturation_slider, 0.f,1.f, 0.1f);
    counts = rangemap.add(I_SATURATION, 0.f,1.f, 0.1f);

    ui->display_slider->setRange(1,10);

//    counts = rangemap.add(ui->chroma_slider, 0.f,1.f,0.1f);
    counts = rangemap.add(I_CHROMA, 0,1.5, 0.1);

}

void Form::manage_slider_change(SliderParam slidername, int value){
    float contrast_val,
            saturation1,
            gam,
            chroma,
            sharpness_value,
            red_gain_value,
            blue_gain_value;
//    int exposure_value;

    switch(slidername){
    case I_CONTRAST:
        contrast_val = rangemap.getMappedValue(I_CONTRAST,value);
        frame->setContrast(contrast_val);
        ui->preset_slider_value_label->setNum(contrast_val);
        ui->contrast_label->setNum(contrast_val);
        user.contrast = contrast_val;
        disp.contrast = value;
        frend->setGcontrast(contrast_val);
        break;

    case I_SATURATION:
        saturation1 = rangemap.getMappedValue(I_SATURATION,value);
        frame->setSaturation(saturation1);
        ui->preset_slider_value_label->setNum(saturation1);
        ui->saturation_label->setNum(saturation1);
        disp.saturation = value;
        break;

    case I_GAMMA:
        gam = rangemap.getMappedValue(I_GAMMA,value);
        frame->setGamma(gam);
        ui->preset_slider_value_label->setNum(gam);
        ui->gamma_label->setNum(gam);
        disp.gamma = value;
        user.gamma = gam;
        frend->setGgam(gam);
        break;

    case I_CHROMA:
        chroma = rangemap.getMappedValue(I_CHROMA,value);
        frame->setChromafactor(chroma);
        ui->preset_slider_value_label->setNum(chroma);
        ui->chroma_label->setNum(chroma);
        user.chroma = chroma;
        frend->setGchroma(chroma);
        break;

    case I_SHARPNESS:
        sharpness_value = rangemap.getMappedValue(I_SHARPNESS,value);
        if(value != 0 )
        {
            frame->setSharp_value(sharpness_value);
//            std::cout<<sharpness_value<<std::endl;
        }
        ui->preset_slider_value_label->setNum(double(sharpness_value));
        ui->sharpness_label->setNum(sharpness_value);
        disp.sharpness = value;
        user.sharpness = sharpness_value;
        break;

    case I_GAIN_R:
        red_gain_value = rangemap.getMappedValue(I_GAIN_R,value);
        frame->setR_gain(red_gain_value);
        ui->preset_slider_value_label->setNum(red_gain_value);
        ui->red_gain_label->setNum(red_gain_value);
        disp.gain_r=value;
        pref.gain_r = value;
        user.redg = red_gain_value;
        frend->setGrGain(red_gain_value);
        break;

    case I_GAIN_B:
        blue_gain_value = rangemap.getMappedValue(I_GAIN_B, value);
        frame->setB_gain(blue_gain_value);
        ui->preset_slider_value_label->setNum(blue_gain_value);
        ui->blue_gain_label->setNum(blue_gain_value);
        disp.gain_b=value;
        pref.gain_b=value;
        user.blueg = blue_gain_value;
        frend->setGbGain(blue_gain_value);
        break;

//    case S_EXPOSURE:
//        exposure_value = (int)rangemap.getMappedValue(S_EXPOSURE,value);
//    //    std::cerr<<"i am in exposure"<<std::endl;
//        acq->getComm()->send_param(EXPOSURE,exposure_value);
//        ui->exposure_slider_label->setNum(exposure_value);
//        sens.exposure = value;
//        break;

    case I_BRIGHTNESS:
        frame->setBrightness(value);
        ui->preset_slider_value_label->setNum(value);
        ui->brightness_label->setNum(value);
        disp.brightness_img = value;
        user.brightness = value;
        frend->setGbright(value);
        break;

    case I_THRESHOLD:
        frame->setThreshold(value);
        ui->preset_slider_value_label->setNum(value);
        ui->threshold_label->setNum(value);
        disp.threshold_shrp = value;
        user.threshold = value;
        break;

    default:
        break;
    }
}


void Form::initialize_display()
{

    manage_slider_change(I_SHARPNESS,disp.sharpness);
    manage_slider_change(I_GAMMA,disp.gamma);
    manage_slider_change(I_GAIN_R,disp.gain_r);
    manage_slider_change(I_GAIN_B,disp.gain_b);
    manage_slider_change(I_CONTRAST,disp.contrast);
    manage_slider_change(I_CHROMA,disp.chroma);
    manage_slider_change(I_BRIGHTNESS,disp.brightness_img);
    manage_slider_change(I_SATURATION,disp.saturation);
    manage_slider_change(I_THRESHOLD,disp.threshold_shrp);
    load_from_display_bin = true;

}

void Form::on_led_brightness_slider_valueChanged(int value)
{
    ui->led_brightness_silder_label->setNum(value);
    ui->led_brightness_slider->setValue(value);
    sens.brightness1 = value;
    user.led_value1 = value;
    uvc->send_param(BRIGHTNESS1,value);
}

void Form::on_enginerring_settings_clicked()
{
    ui->menu_options_stackwidget->setCurrentIndex(8);
    ui->label_20->setText("Engeering Settings");
}

#define FLASH_STATUS(msg) \
    ui->status_label->setText(msg);\
    QTimer::singleShot(3000,this,SLOT(remove_status_messsage_after_timeout()))


void Form::on_led_pushButton_clicked()
{
//    std::cout<<"i am inside led"<<std::endl;
    ledstate.change();
    std::string messages [] = { "White LED ON","Blue LED ON","LEDs OFF","LEDs ON"};
    switch(ledstate.get())
    {
    case 1:
        {
        uvc->send_param(BLUE_LED,0);
        uvc->send_param(BRIGHTNESS1,0);
        uvc->send_param(BRIGHTNESS2,0);
        uvc->send_param(BLUE_LED,1);
        on_led_brightness_slider_valueChanged(sens.brightness1); //slider one
        on_led_brightness_slider_2_valueChanged(sens.brightness2); //slider_two
        change_stylesheets(ui->led_pushButton,1,"light.png",ui->led_line,ui->led_label,"Blue Led On");
        }
        break;
    case 2:
        {
        uvc->send_param(BLUE_LED,1);
        on_led_brightness_slider_valueChanged(sens.brightness1); //slider one
        on_led_brightness_slider_2_valueChanged(sens.brightness2); //slider_two
        uvc->send_param(BLUE_LED,0);
        on_led_brightness_slider_valueChanged(sens.brightness1); //slider one
        on_led_brightness_slider_2_valueChanged(sens.brightness2); //slider_two
        change_stylesheets(ui->led_pushButton,2,"light.png",ui->led_line,ui->led_label,"Both Led on");
        }

        break;
    case 3:
        {
        uvc->send_param(BLUE_LED,0);
        uvc->send_param(BRIGHTNESS1,0);
        uvc->send_param(BRIGHTNESS2,0);
        uvc->send_param(BLUE_LED,1);
        uvc->send_param(BRIGHTNESS1,0);
        uvc->send_param(BRIGHTNESS2,0);
        change_stylesheets(ui->led_pushButton,2,"light.png",ui->led_line,ui->led_label,"Both Led Off");
        }

        break;
    case 0:
        {
            uvc->send_param(BLUE_LED,1);
            uvc->send_param(BRIGHTNESS1,0);
            uvc->send_param(BRIGHTNESS2,0);
            uvc->send_param(BLUE_LED,0);
            on_led_brightness_slider_valueChanged(sens.brightness1); //slider one
            on_led_brightness_slider_2_valueChanged(sens.brightness2); //slider_two
            change_stylesheets(ui->led_pushButton,4,"light.png",ui->led_line,ui->led_label,"White Led On");
        }
        break;
    }
    user.led_state = ledstate.get();
    led_auto_control_sync(sens.alc_aec_state);
    FLASH_STATUS(messages[ledstate.get()].c_str());
}


void Form::change_stylesheets(QPushButton *Button, int option,QString icon_name, QFrame *line, QLabel *label,QString label_ntext)
{
    if(option == 1)
    {
        Button->setStyleSheet("border-radius: 10px;"
                              "qproperty-icon:url(:/images/icons/"+icon_name+");"
                              "qproperty-iconSize: 90px 90px;"
                              "background-color: rgb(255, 153, 102);"
                              "border:3px solid;"
                              "border-color: rgb(255, 153, 102);"
                              "padding-top:40px;");
        line->setStyleSheet("border:3px solid;"
                            "border-color: black;");
        label->setStyleSheet("background:transparent;"
                             "border-color: rgb(255,255,255);"
                             "color:black;"
                             "font:bold;"
                             "font-size:15px;"
                             "border-radius: 1px;"
                             "qproperty-alignment: AlignCenter;");
        label->setText(label_ntext);

    }
    else if(option == 2)
    {
        Button->setStyleSheet("border-radius: 10px;"
                              "qproperty-icon:url(:/images/icons/"+icon_name+");"
                              "qproperty-iconSize: 90px 90px;"
                              "background-color: rgb(102, 255, 102);"
                              "border:3px solid;"
                              "border-color: rgb(102, 255, 102);"
                              "padding-top:40px;");
        line->setStyleSheet("border:3px solid;"
                            "border-color: black;");
        label->setStyleSheet("background:transparent;"
                             "border-color: rgb(255,255,255);"
                             "color:black;"
                             "font:bold;"
                             "font-size:15px;"
                             "border-radius: 1px;"
                             "qproperty-alignment: AlignCenter;");
        label->setText(label_ntext);

    }
    else if(option == 3)
    {
        Button->setStyleSheet("border-radius: 10px;"
                              "qproperty-icon:url(:/images/icons/"+icon_name+");"
                              "qproperty-iconSize: 90px 90px;"
                              "background-color: rgb(102, 204, 255);"
                              "border:3px solid;"
                              "border-color: rgb(102, 204, 255);"
                              "padding-top:40px;");
        line->setStyleSheet("border:3px solid;"
                            "border-color: black;");
        label->setStyleSheet("background:transparent;"
                             "border-color: rgb(255,255,255);"
                             "color:black;"
                             "font:bold;"
                             "font-size:15px;"
                             "border-radius: 1px;"
                             "qproperty-alignment: AlignCenter;");
    label->setText(label_ntext);
    }
    else if(option == 4)
    {
        Button->setStyleSheet("border-radius: 10px;"
                              "qproperty-icon:url(:/images/icons/"+icon_name+");"
                              "qproperty-iconSize: 90px 90px;"
                              "background:transparent;"
                              "border:3px solid;"
                              "border-color: rgb(64, 66, 68);"
                              "padding-top:40px;");
        line->setStyleSheet("border:3px solid;"
                            "border-color: white;");
        label->setStyleSheet("background:transparent;"
                             "border-color: rgb(255,255,255);"
                             "color:white;"
                             "font:bold;"
                             "font-size:15px;"
                             "border-radius: 1px;"
                             "qproperty-alignment: AlignCenter;");
        label->setText(label_ntext);

    }
}

void Form::remove_status_messsage_after_timeout()
{
    ui->status_label->clear();
}


void Form::on_Zoom_pushbutton_clicked()
{
    zoom_btn_press_cnt = zoom_btn_press_cnt + 1;
    if(zoom_btn_press_cnt%4 == 1)
    {
        frend->setZoomVal(1.2);
//        frame->setZoom_value(1.2);
        change_stylesheets(ui->Zoom_pushbutton,1,"zoom.png",ui->zoom_line,ui->zoom_label,"Zoom 1.2x");
        ui->status_label->setText("Zoom Level 120% of original image");
        QTimer::singleShot(3000,this,SLOT(remove_status_messsage_after_timeout()));

    }
    else if(zoom_btn_press_cnt%4 == 2)
    {
        frend->setZoomVal(1.5);
//        frame->setZoom_value(1.4);
        change_stylesheets(ui->Zoom_pushbutton,3,"zoom.png",ui->zoom_line,ui->zoom_label,"Zoom 1.5x");
        ui->status_label->setText("Zoom Level 150% of original image");
        QTimer::singleShot(3000,this,SLOT(remove_status_messsage_after_timeout()));

    }
//    else if(zoom_btn_press_cnt%4 == 3)
//    {
//        frame->setZoom_value(1.4);
//        change_stylesheets(ui->Zoom_pushbutton,3,"zoom.png",ui->zoom_line,ui->zoom_label,"Zoom 1.4x");
//        ui->status_label->setText("Zoom Level 140% of original image");
//        QTimer::singleShot(3000,this,SLOT(remove_status_messsage_after_timeout()));

//    }
    else if(zoom_btn_press_cnt%4 == 3)
    {
        frend->setZoomVal(1);
//        frame->setZoom_value(0);
        change_stylesheets(ui->Zoom_pushbutton,4,"zoom.png",ui->zoom_line,ui->zoom_label,"Zoom Off");
        ui->status_label->setText("Zoom turned Off");
        QTimer::singleShot(3000,this,SLOT(remove_status_messsage_after_timeout()));
        zoom_btn_press_cnt = 0;
    }

}

void Form::on_mask_pushbutton_clicked()
{

    mask_btn_press_cnt = mask_btn_press_cnt + 1;

    if(mask_btn_press_cnt%3 == 1)
    {
        frend->setMaskSz(2);
//        frame->setMask(1);
//        pref.mask = 1;
        change_stylesheets(ui->mask_pushbutton,1,"circular.png",ui->mask_line,ui->mask_label,"Circular Mask");
        ui->status_label->setText("Circular Mask Applied to the image ");
        QTimer::singleShot(3000,this,SLOT(remove_status_messsage_after_timeout()));
    }
    else if(mask_btn_press_cnt%3 == 2)
    {
        frend->setMaskSz(0);
//        frame->setMask(0);
//        pref.mask = 0;
        change_stylesheets(ui->mask_pushbutton,2,"no_mask.png",ui->mask_line,ui->mask_label,"No Mask");
        ui->status_label->setText("Mask removed from the image ");
        QTimer::singleShot(3000,this,SLOT(remove_status_messsage_after_timeout()));

    }
    else if(mask_btn_press_cnt%3 == 0)
    {
        frend->setMaskSz(1);
//        frame  ->setMask(2);
//        pref.mask = 2;
        mask_btn_press_cnt = 0;
        change_stylesheets(ui->mask_pushbutton,4,"circular.png",ui->mask_line,ui->mask_label,"Octagonal Mask");
        ui->status_label->setText("Octagonal Mask Applied to the image ");
        QTimer::singleShot(3000,this,SLOT(remove_status_messsage_after_timeout()));

    }
}

void Form::on_freeze_pushButton_clicked()
{
    prefreeze_btn_press_cnt = prefreeze_btn_press_cnt + 1;
    if(prefreeze_btn_press_cnt%2 != 0)
    {
        change_stylesheets(ui->freeze_pushButton,1,"freeze.png",ui->freeze_line,ui->freeze_label,"Freeze On");
//        QFuture<void> future = QtConcurrent::run(frame,&FrameContext::pre_freeze);
//        watcher.setFuture(future);
//        frame->pre_freeze(); // run on form thread
        frend->setPrFrz(true);
        ui->pip_size_pushButton->setDisabled(true);
        ui->image_resize_pushButton->setDisabled(true);
        ui->button_pushbutton->setDisabled(true);
        ui->hdmi_in_pushButton->setDisabled(true);
        ui->patient_info_pushButton->setDisabled(true);
        ui->pip_position_selection->setDisabled(true);
    }
    else if(prefreeze_btn_press_cnt%2 == 0)
    {
        frend->setPrFrz(false);
//        frame->setFreeze_flag(false);
        change_stylesheets(ui->freeze_pushButton,4,"freeze.png",ui->freeze_line,ui->freeze_label,"Freeze Off");
        prefreeze_btn_press_cnt = 0;
        ui->pip_size_pushButton->setDisabled(false);
        ui->image_resize_pushButton->setDisabled(false);
        ui->button_pushbutton->setDisabled(false);
        ui->hdmi_in_pushButton->setDisabled(false);
        ui->patient_info_pushButton->setDisabled(false);
        ui->pip_position_selection->setDisabled(false);

    }
}


void Form::on_save_video_pushButton_clicked()
{
    save_video_btn_press_cnt = save_video_btn_press_cnt + 1;
    if(save_video_btn_press_cnt%2 != 0)
    {
        frame->setVideo_save(true);
        change_stylesheets(ui->save_video_pushButton,1,"save_video.png",ui->save_video_line,ui->save_video_label,"Saving Video");
        ui->status_label->setText("Video Save Started at 30 FPS ");
        QTimer::singleShot(3000,this,SLOT(remove_status_messsage_after_timeout()));

    }
    else if(save_video_btn_press_cnt%2 == 0)
    {
        frame->setVideo_save(false);
        save_video_btn_press_cnt = 0;
        change_stylesheets(ui->save_video_pushButton,4,"save_video.png",ui->save_video_line,ui->save_video_label,"Video Save");
        ui->status_label->setText("Video Save Stopped ");
        QTimer::singleShot(3000,this,SLOT(remove_status_messsage_after_timeout()));

    }
}

void Form::on_save_image_pushButton_clicked()
{
//    frame->setImage_save(true);
    frend->saveImg();
    ui->status_label->setText("Image saved ..");
    QTimer::singleShot(3000,this,SLOT(remove_status_messsage_after_timeout()));
}

void Form::on_home_pushButton_clicked()
{
    ui->menu_options_stackwidget->setCurrentIndex(0);
    ui->label_20->setText("Home");
}

void Form::on_pip_position_selection_clicked()
{
    pip_position_btn_prs_cnt = pip_position_btn_prs_cnt + 1;
    if(pip_position_btn_prs_cnt%4 == 1)
    {
        change_stylesheets(ui->pip_position_selection,4,"pip_size",ui->pip_line,ui->pip_position_label,"Bottom Right ");
        frend->setPheight(1080);
        frend->setPwidth(sw);
    }
    else if(pip_position_btn_prs_cnt%4 == 2)
    {
        change_stylesheets(ui->pip_position_selection,1,"pip_size",ui->pip_line,ui->pip_position_label,"Bottom Left ");
        frend->setPheight(1080);
        frend->setPwidth(1440);
    }
    else if(pip_position_btn_prs_cnt%4 == 3)
    {
        change_stylesheets(ui->pip_position_selection,1,"pip_size",ui->pip_line,ui->pip_position_label,"Top Right ");
        frend->setPheight(sh);
        frend->setPwidth(sw);
    }
    else if(pip_position_btn_prs_cnt%4 == 0)
    {
        change_stylesheets(ui->pip_position_selection,1,"pip_size",ui->pip_line,ui->pip_position_label,"Top Left ");
        frend->setPheight(sh);
        frend->setPwidth(1440);
    }
}


void Form::on_pip_size_pushButton_clicked()
{
    pip_size_btn_prs_cnt = pip_size_btn_prs_cnt + 1;
    if(pip_size_btn_prs_cnt%3 == 1)
    {
        sw = 480;
        sh = 360;
        frend->setPipSz(0);
        change_stylesheets(ui->pip_size_pushButton,1,"pip_size",ui->pip_size_line,ui->pip_size_label,"PIP 1/3 size");
    }
    else if(pip_size_btn_prs_cnt%3 == 2)
    {
        change_stylesheets(ui->pip_size_pushButton,2,"pip_size",ui->pip_size_line,ui->pip_size_label,"PIP Off");

    }
    else if(pip_size_btn_prs_cnt%3 == 0)
    {
        sw = 720;
        sh = 540;
        frend->setPipSz(1);
        change_stylesheets(ui->pip_size_pushButton,4,"pip_size",ui->pip_line,ui->pip_size_label,"PIP 1/2 size");
        pip_size_btn_prs_cnt = 0;
    }
    pip_position_btn_prs_cnt = pip_position_btn_prs_cnt - 1;
    on_pip_position_selection_clicked();
}

void Form::on_image_settings_pushButton_clicked()
{
    ui->menu_options_stackwidget->setCurrentIndex(1);
    ui->label_20->setText("Settings");
}

void Form::on_display_control_pushButton_clicked()
{
    ui->menu_options_stackwidget->setCurrentIndex(3);
//    ui->label_20->setText("Presets");
}

void Form::on_led_brightness_slider_2_valueChanged(int value)
{
    ui->led_brightness_silder_label_2->setNum(value);
    ui->led_brightness_slider_2->setValue(value);
    sens.brightness2 = value;
    user.led_value2 = value;
    uvc->send_param(BRIGHTNESS2,value);
}

void Form::on_exposure_slider_valueChanged(int value)
{
    ui->exposure_slider_label->setNum(value);
    ui->exposure_slider->setValue(value);
    sens.exposure = value;
    user.exposture = sens.exposure;
    uvc->send_param(EXPOSURE,value);
}

void Form::on_black_clamp_level_slider_valueChanged(int value)
{
    ui->black_clamp_level_slider_label->setNum(value);
    ui->black_clamp_level_slider->setValue(value);
    sens.blacklev = value;
    user.black_level_clamp = sens.blacklev;
    uvc->send_param(BLACK_CLAMP_LEVEL,value);
}

void Form::on_vga_gain_slider_valueChanged(int value)
{
    ui->vga_gain_slider_label->setNum(value);
    ui->vga_gain_slider->setValue(value);
    sens.variable_gain = value;
    user.vga_gain = sens.variable_gain;
    uvc->send_param(VGA,value);
}

void Form::on_preset_pushButton_clicked()
{
    ui->menu_options_stackwidget->setCurrentIndex(2);
    ui->label_20->setText("User Presets");
}


void Form::on_brightness_pushButton_clicked()
{
    preset_bright_prs_cnt = preset_bright_prs_cnt + 1;
    if(preset_bright_prs_cnt%2 != 0)
    {
        enable_disable_all_preset_buttons(true);
        ui->brightness_pushButton->setDisabled(false);
        enable_disable_preset_slider_set(false);
        ui->preset_slider->setRange(-128,128);
        ui->preset_slider->setValue(disp.brightness_img);
        ui->preset_slider_value_label->setNum(disp.brightness_img);
        sliderchange = I_BRIGHTNESS;


    }
    else if(preset_bright_prs_cnt%2 == 0)
    {
        enable_disable_all_preset_buttons(false);
        enable_disable_preset_slider_set(true);
        preset_bright_prs_cnt = 0;
        disp.brightness_img = slider_value;
    }
}

void Form::enable_disable_all_preset_buttons(bool state)
{
    ui->brightness_pushButton->setDisabled(state);
    ui->contrast_pushButton->setDisabled(state);
    ui->gamma_pushButton->setDisabled(state);
    ui->red_gain_pushButton->setDisabled(state);
    ui->blue_gain_pushButton->setDisabled(state);
    ui->sharpness_pushButton->setDisabled(state);
    ui->threshold_pushButton->setDisabled(state);
    ui->chroma_pushButton->setDisabled(state);
    ui->saturation_pushButton->setDisabled(state);
}

void Form::enable_disable_preset_slider_set(bool state)
{
    ui->preset_slider->setDisabled(state);
    ui->preset_minus_pushButton->setDisabled(state);
    ui->preset_plus_pushButton->setDisabled(state);
    ui->preset_slider_value_label->setDisabled(state);

}

//void Form::preset_slider(float value,SliderParam slidername)
//{
//    manage_slider_change(slidername,value);
//}


void Form::on_preset_slider_valueChanged(int value)
{
    slider_value = value;
//    preset_slider(slider_value,sliderchange);
    manage_slider_change(sliderchange,slider_value);
}

void Form::on_contrast_pushButton_clicked()
{
    preset_contrast_prs_cnt = preset_contrast_prs_cnt + 1;
    if(preset_contrast_prs_cnt%2 != 0)
    {
        enable_disable_all_preset_buttons(true);
        ui->contrast_pushButton->setDisabled(false);
        enable_disable_preset_slider_set(false);
        sliderchange = I_CONTRAST;
        counts = rangemap.add(sliderchange,0.5f,3.f,0.25f);
        ui->preset_slider->setRange(0,counts);
        ui->preset_slider->setValue(disp.contrast);
        ui->preset_slider_value_label->setNum(rangemap.getMappedValue(I_CONTRAST,disp.contrast));

    }
    else if(preset_contrast_prs_cnt%2 == 0)
    {
        enable_disable_all_preset_buttons(false);
        enable_disable_preset_slider_set(true);
        preset_contrast_prs_cnt = 0;
        disp.contrast = slider_value;
    }

}

void Form::on_gamma_pushButton_clicked()
{
    preset_gamma_prs_cnt = preset_gamma_prs_cnt + 1;
    if(preset_gamma_prs_cnt%2 != 0)
    {
        enable_disable_all_preset_buttons(true);
        ui->gamma_pushButton->setDisabled(false);
        enable_disable_preset_slider_set(false);
        sliderchange = I_GAMMA;
        counts = rangemap.add(sliderchange,0.2f, 2.0f, 0.05f);
        ui->preset_slider->setRange(0,counts);
        ui->preset_slider->setValue(disp.gamma);
        ui->preset_slider_value_label->setNum(rangemap.getMappedValue(sliderchange,disp.gamma));
    }
    else if(preset_gamma_prs_cnt%2 == 0)
    {

        preset_gamma_prs_cnt = 0;
        enable_disable_all_preset_buttons(false);
        enable_disable_preset_slider_set(true);
        disp.gamma = slider_value;
    }
}

void Form::on_red_gain_pushButton_clicked()
{
    preset_red_gain_prs_cnt = preset_red_gain_prs_cnt + 1;
    if(preset_red_gain_prs_cnt%2 != 0)
    {
        enable_disable_all_preset_buttons(true);
        ui->red_gain_pushButton->setDisabled(false);
        enable_disable_preset_slider_set(false);
        sliderchange = I_GAIN_R;
        counts = rangemap.getSliderData(sliderchange).xrange.second;
//        counts = rangemap.add(sliderchange,0.2f,2.f,0.2f);
        ui->preset_slider->setRange(0,counts);
        ui->preset_slider->setValue(disp.gain_r);
        ui->preset_slider_value_label->setNum(rangemap.getMappedValue(sliderchange,disp.gain_r));
    }
    else if(preset_red_gain_prs_cnt%2 == 0)
    {

        preset_red_gain_prs_cnt = 0;
        enable_disable_all_preset_buttons(false);
        enable_disable_preset_slider_set(true);
        disp.gain_r = slider_value;
    }

}

void Form::on_blue_gain_pushButton_clicked()
{
    preset_blue_gain_prs_cnt = preset_blue_gain_prs_cnt + 1;
    if(preset_blue_gain_prs_cnt%2 != 0)
    {
        enable_disable_all_preset_buttons(true);
        ui->blue_gain_pushButton->setDisabled(false);
        enable_disable_preset_slider_set(false);
        sliderchange = I_GAIN_B;
//        counts = rangemap.add(sliderchange,0.2f,2.f,0.2f);
        counts = rangemap.getSliderData(sliderchange).xrange.second;
        ui->preset_slider->setRange(0,counts);
        ui->preset_slider->setValue(disp.gain_b);
        ui->preset_slider_value_label->setNum(rangemap.getMappedValue(sliderchange,disp.gain_b));
    }
    else if(preset_blue_gain_prs_cnt%2 == 0)
    {

        preset_blue_gain_prs_cnt = 0;
        enable_disable_all_preset_buttons(false);
        enable_disable_preset_slider_set(true);
        disp.gain_b = slider_value;
    }
}

void Form::on_sharpness_pushButton_clicked()
{
    preset_sharp_prs_cnt = preset_sharp_prs_cnt + 1;
    if(preset_sharp_prs_cnt%2 != 0)
    {
        enable_disable_all_preset_buttons(true);
        ui->sharpness_pushButton->setDisabled(false);
        enable_disable_preset_slider_set(false);
        sliderchange = I_SHARPNESS;
        counts = rangemap.add(sliderchange, 0.f, 6.f, 0.3f);
        ui->preset_slider->setRange(0,counts);
        ui->preset_slider->setValue(disp.sharpness);
        ui->preset_slider_value_label->setNum(rangemap.getMappedValue(sliderchange,disp.sharpness));
    }
    else if(preset_sharp_prs_cnt%2 == 0)
    {

        preset_sharp_prs_cnt = 0;
        enable_disable_all_preset_buttons(false);
        enable_disable_preset_slider_set(true);
        disp.sharpness = slider_value;
    }
}

void Form::on_threshold_pushButton_clicked()
{
    preset_threshold_prs_cnt = preset_threshold_prs_cnt + 1;
    if(preset_threshold_prs_cnt%2 != 0)
    {
        enable_disable_all_preset_buttons(true);
        ui->threshold_pushButton->setDisabled(false);
        enable_disable_preset_slider_set(false);
        sliderchange = I_THRESHOLD;
        ui->preset_slider->setRange(1,10);
        ui->preset_slider->setValue(disp.threshold_shrp);
        ui->preset_slider_value_label->setNum(disp.threshold_shrp);
    }
    else if(preset_threshold_prs_cnt%2 == 0)
    {

        preset_threshold_prs_cnt = 0;
        enable_disable_all_preset_buttons(false);
        enable_disable_preset_slider_set(true);
        disp.threshold_shrp = slider_value;
    }
}

void Form::on_chroma_pushButton_clicked()
{
    preset_chroma_prs_cnt = preset_chroma_prs_cnt + 1;
    if(preset_chroma_prs_cnt%2 != 0)
    {
        enable_disable_all_preset_buttons(true);
        ui->chroma_pushButton->setDisabled(false);
        enable_disable_preset_slider_set(false);
        sliderchange = I_CHROMA;
//        counts = rangemap.add(sliderchange, 0.f,1.f, 0.1f);
        counts = rangemap.getSliderData(sliderchange).xrange.second;
        ui->preset_slider->setRange(0,counts);
        ui->preset_slider->setValue(disp.chroma);
        ui->preset_slider_value_label->setNum(rangemap.getMappedValue(sliderchange,disp.chroma));
    }
    else if(preset_chroma_prs_cnt%2 == 0)
    {

        preset_chroma_prs_cnt = 0;
        enable_disable_all_preset_buttons(false);
        enable_disable_preset_slider_set(true);
        disp.chroma = slider_value;
    }
}

void Form::on_saturation_pushButton_clicked()
{
    preset_saturation_prs_cnt = preset_saturation_prs_cnt + 1;
    if(preset_saturation_prs_cnt%2 != 0)
    {
        enable_disable_all_preset_buttons(true);
        ui->saturation_pushButton->setDisabled(false);
        enable_disable_preset_slider_set(false);
        sliderchange = I_SATURATION;
        counts = rangemap.add(sliderchange, 0.f,1.f, 0.1f);
        ui->preset_slider->setRange(0,counts);
        ui->preset_slider->setValue(disp.saturation);
        ui->preset_slider_value_label->setNum(rangemap.getMappedValue(sliderchange,disp.saturation));
    }
    else if(preset_saturation_prs_cnt%2 == 0)
    {
        preset_saturation_prs_cnt = 0;
        enable_disable_all_preset_buttons(false);
        enable_disable_preset_slider_set(true);
        disp.saturation = slider_value;
    }
}

void Form::on_preset1_pushButton_clicked()
{
    preset1_btn_prs_cnt = preset1_btn_prs_cnt + 1;
    if(preset1_btn_prs_cnt%2 != 0)
    {
        ui->preset_frame->setDisabled(false);
//        if(preset1_btn_prs_cnt == 1)
        {
            preset_selection_file_name = "Preset1.bin";
            preset_read_and_apply(preset_selection_file_name);

        }
        preset_selection_enable_disable_buttons(true);
        ui->preset1_pushButton->setDisabled(false);
        ui->preset1_pushButton->setStyleSheet( "border-radius: 10px;"
                                               "background-color: rgb(255, 153, 102);"
                                               "border:1px solid;"
                                               "border-color: rgb(255, 153, 102);"
                                               "font:bold;"
                                               "font-size:18px;");

    }
    else if(preset1_btn_prs_cnt%2 == 0)
    {
        ui->preset_frame->setDisabled(true);
        preset_selection_enable_disable_buttons(false);
        ui->preset1_pushButton->setStyleSheet( "border-radius: 10px;"
                                               "background-color: rgb(123, 127, 131);"
                                               "border:1px solid;"
                                               "border-color: rgb(64, 66, 68);"
                                               "font:bold;"
                                               "font-size:18px;");

    }

}

void Form::on_save_preset_pushButton_clicked()
{
    QString preference = QDir::currentPath()+"/"+preset_selection_file_name;
    {
        QFile file(preference);
        file.open(QIODevice::WriteOnly | QIODevice::Text);
        QDataStream out(&file);
        out<<disp;
        file.close();
    }

//    std::cout<<"brightness"<<disp.brightness_img<<std::endl;
//    std::cout<<"contrast"<<disp.contrast<<std::endl;
//    std::cout<<"gamma"<<disp.gamma<<std::endl;

//    std::cout<<"gain_red"<<disp.gain_r<<std::endl;
//    std::cout<<"gain_blue"<<disp.gain_b<<std::endl;
//    std::cout<<"sharpness"<<disp.sharpness<<std::endl;

//    std::cout<<"threshold"<<disp.threshold_shrp<<std::endl;
//    std::cout<<"chroma"<<disp.chroma<<std::endl;
//    std::cout<<"saturation"<<disp.saturation<<std::endl;


}

void Form::on_preset2_pushButton_clicked()
{
    preset2_btn_prs_cnt = preset2_btn_prs_cnt + 1;
    if(preset2_btn_prs_cnt%2 != 0)
    {
        ui->preset_frame->setDisabled(false);
//        if(preset2_btn_prs_cnt == 1)
        {
            preset_selection_file_name = "Preset2.bin";
            preset_read_and_apply(preset_selection_file_name);

        }
        preset_selection_enable_disable_buttons(true);
        ui->preset2_pushButton->setDisabled(false);
        ui->preset2_pushButton->setStyleSheet( "border-radius: 10px;"
                                               "background-color: rgb(255, 153, 102);"
                                               "border:1px solid;"
                                               "border-color: rgb(255, 153, 102);"
                                               "font:bold;"
                                               "font-size:18px;");

    }
    else if(preset2_btn_prs_cnt%2 == 0)
    {
        ui->preset_frame->setDisabled(true);
        preset_selection_enable_disable_buttons(false);
        ui->preset2_pushButton->setStyleSheet( "border-radius: 10px;"
                                               "background-color: rgb(123, 127, 131);"
                                               "border:1px solid;"
                                               "border-color: rgb(64, 66, 68);"
                                               "font:bold;"
                                               "font-size:18px;");

    }
}



void Form::on_preset3_pushButton_clicked()
{
    preset3_btn_prs_cnt = preset3_btn_prs_cnt + 1;
    if(preset3_btn_prs_cnt%2 != 0)
    {
        ui->preset_frame->setDisabled(false);
        preset_selection_enable_disable_buttons(true);
//        if(preset3_btn_prs_cnt == 1)
        {
            preset_selection_file_name = "Preset3.bin";
            preset_read_and_apply(preset_selection_file_name);

        }
        ui->preset3_pushButton->setDisabled(false);
        ui->preset3_pushButton->setStyleSheet( "border-radius: 10px;"
                                               "background-color: rgb(255, 153, 102);"
                                               "border:1px solid;"
                                               "border-color: rgb(255, 153, 102);"
                                               "font:bold;"
                                               "font-size:18px;");

    }
    else if(preset3_btn_prs_cnt%2 == 0)
    {
        ui->preset_frame->setDisabled(true);
        preset_selection_enable_disable_buttons(false);
        ui->preset3_pushButton->setStyleSheet( "border-radius: 10px;"
                                               "background-color: rgb(123, 127, 131);"
                                               "border:1px solid;"
                                               "border-color: rgb(64, 66, 68);"
                                               "font:bold;"
                                               "font-size:18px;");

    }

}



void Form::on_preset4_pushButton_clicked()
{
    preset4_btn_prs_cnt = preset4_btn_prs_cnt + 1;
    if(preset4_btn_prs_cnt%2 != 0)
    {
        ui->preset_frame->setDisabled(false);
        preset_selection_enable_disable_buttons(true);
        if(preset4_btn_prs_cnt == 1)
        {
            preset_selection_file_name = "Preset4.bin";
            preset_read_and_apply(preset_selection_file_name);

        }
        ui->preset4_pushButton->setDisabled(false);
        ui->preset4_pushButton->setStyleSheet( "border-radius: 10px;"
                                               "background-color: rgb(255, 153, 102);"
                                               "border:1px solid;"
                                               "border-color: rgb(255, 153, 102);"
                                               "font:bold;"
                                               "font-size:18px;");

    }
    else if(preset4_btn_prs_cnt%2 == 0)
    {
        ui->preset_frame->setDisabled(true);
        preset_selection_enable_disable_buttons(false);
        ui->preset4_pushButton->setStyleSheet( "border-radius: 10px;"
                                               "background-color: rgb(123, 127, 131);"
                                               "border:1px solid;"
                                               "border-color: rgb(64, 66, 68);"
                                               "font:bold;"
                                               "font-size:18px;");

    }

}

void Form::preset_selection_enable_disable_buttons(bool state_change)
{
    ui->preset1_pushButton->setDisabled(state_change);
    ui->preset2_pushButton->setDisabled(state_change);
    ui->preset3_pushButton->setDisabled(state_change);
    ui->preset4_pushButton->setDisabled(state_change);
}

void Form::preset_read_and_apply(QString preset_selection)
{
    QString user_settings = QDir::currentPath()+"/"+preset_selection;
    if (QFileInfo::exists(user_settings)){
        QFile file(user_settings);
        file.open(QIODevice::ReadOnly | QIODevice::Text);
        QDataStream in_all(&file);
        in_all>>disp;
        file.close();
    }
//    std::cout<<"disp.gamma"<<disp.gamma<<std::endl;

    initialize_display();
}


void Form::on_auto_contrast_pushButton_clicked()
{


    enhance_btn_press_cnt = enhance_btn_press_cnt + 1;
    if(enhance_btn_press_cnt%2 != 0)
    {
        frame->setAuto_brightness_set(true);
        change_stylesheets(ui->auto_contrast_pushButton,1,"enhance.png",ui->enhance_line,ui->enhance_label,"Auto Contrast");
        ui->status_label->setText("Auto contrast turned On ");
        QTimer::singleShot(3000,this,SLOT(remove_status_messsage_after_timeout()));

    }
    else if(enhance_btn_press_cnt%2 == 0)
    {
        frame->setAuto_brightness_set(false);
        change_stylesheets(ui->auto_contrast_pushButton,4,"enhance.png",ui->enhance_line,ui->enhance_label,"Auto Contrast");
        enhance_btn_press_cnt = 0;
        ui->status_label->setText("Auto contrast turned Off ");
        QTimer::singleShot(3000,this,SLOT(remove_status_messsage_after_timeout()));

    }
}

void Form::camera_status_update(bool status)
{
    if(status == true)
    {
        ui->scope_connect_label->setText("Connected");
        ui->camera_status_pushButton->setStyleSheet("font: 20px;"
                                                    "color:white;"
                                                    "text-align:center;"
                                                    "background-color: rgb(255, 255, 255);"
                                                    "qproperty-icon:url(:/images/icons/connected_camera.png);"
                                                    "qproperty-iconSize: 50px 50px;");
    }
    else if(status == false)
    {
        ui->scope_connect_label->setText("Disconnected");
        ui->camera_status_pushButton->setStyleSheet("font: 20px;"
                                                    "color:white;"
                                                    "text-align:center;"
                                                    "background-color: rgb(255, 255, 255);"
                                                    "qproperty-icon:url(:/images/icons/disconnected_camera.png);"
                                                    "qproperty-iconSize: 50px 50px;");

    }
}

//static int wb_cnt = 0;
void Form::on_white_balance_pushbutton_2_clicked()
{
    frame->setWhite_balance_click(true);
    //wb_cnt = wb_cnt+1;
    //if(wb_cnt % 2 == 1)
        //frame->setWhite_balance_click(true);
    //else
        //frame->setWhite_balance_click(false);


//    timer_wb_click = new QTimer(this);
//    connect(timer_wb_click, SIGNAL(timeout()), this, SLOT(change_led_dimfactor()));
//    timer_wb_click->start(100);
}

void Form::on_patient_info_pushButton_clicked()
{
    patientwindow = new PatientWindow(frame);
    patientwindow->show();
}

void Form::on_alc_aec_pushbutton_clicked()
{
    alc_aec_state.change();
    std::string messages [] = { "AEC On","ALC ON", "ALC&AEC On", "ALC&AEC Off"};
    switch(alc_aec_state.get())
    {
    case 1:
    {
        uvc->send_param(ALCAEC,1);
        sens.alc_aec_state = 1;
        uvc->send_param(BRIGHTNESS1,sens.brightness1);
        uvc->send_param(BRIGHTNESS2,sens.brightness2);
        change_stylesheets(ui->alc_aec_pushbutton,1,"auto.png",ui->alc_aec_line,ui->alc_aec_label,"AEC On");
    }
        break;
    case 2:
    {
        uvc->send_param(ALCAEC,2);
        sens.alc_aec_state = 2; 
        uvc->send_param(EXPOSURE,sens.exposure);
        change_stylesheets(ui->alc_aec_pushbutton,2,"auto.png",ui->alc_aec_line,ui->alc_aec_label,"ALC On");
    }
        break;
    case 3:
    {
        uvc->send_param(ALCAEC,3);
        sens.alc_aec_state = 3;
        change_stylesheets(ui->alc_aec_pushbutton,3,"auto.png",ui->alc_aec_line,ui->alc_aec_label,"ALC&AEC On");
        ui->status_label->setText("Automatic Light Control and Exposure turned On ");

    }
        break;
    case 0:
    {
        std::cout<<"led at the case 0"<<std::endl;
        uvc->send_param(ALCAEC,0);
        alc_aec_btn_press_cnt = 0;
        sens.alc_aec_state = 0;
        change_stylesheets(ui->alc_aec_pushbutton,4,"auto.png",ui->alc_aec_line,ui->alc_aec_label,"ALC&AEC Off");
        uvc->send_param(BRIGHTNESS1,sens.brightness1);
        uvc->send_param(BRIGHTNESS2,sens.brightness2);
        uvc->send_param(EXPOSURE,sens.exposure);
    }
        break;
    }
//    auto_control_led_sync(sens.led);
    FLASH_STATUS(messages[alc_aec_state.get()].c_str());

}

void Form::on_flipx_pushButton_clicked()
{
    flipx_btn_press_cnt = flipx_btn_press_cnt + 1;
    if(flipx_btn_press_cnt%2 != 0)
    {
        frame->setFlipx(true);
        change_stylesheets(ui->flipx_pushButton,1,"flipx.png",ui->flipx_line,ui->flipx_label,"Flip X-axis");
        ui->status_label->setText("Image Flipped on the X-axis ");
        QTimer::singleShot(3000,this,SLOT(remove_status_messsage_after_timeout()));

    }
    else if(flipx_btn_press_cnt%2 == 0)
    {
        frame->setFlipx(false);
        change_stylesheets(ui->flipx_pushButton,4,"flipx.png",ui->flipx_line,ui->flipx_label,"Flip X-axis");
        flipx_btn_press_cnt = 0;
        ui->status_label->setText("Flipped back to normal Image");
        QTimer::singleShot(3000,this,SLOT(remove_status_messsage_after_timeout()));

    }

}

void Form::on_flipy_pushButton_clicked()
{
    flipy_btn_press_cnt = flipy_btn_press_cnt + 1;
    if(flipy_btn_press_cnt%2 != 0)
    {
        frame->setFlipy(true);
        change_stylesheets(ui->flipy_pushButton,1,"flipy.png",ui->flipy_line,ui->flipy_label,"Flip Y-axis");
        ui->status_label->setText("Image Flipped on the Y-axis ");
        QTimer::singleShot(3000,this,SLOT(remove_status_messsage_after_timeout()));

    }
    else if(flipy_btn_press_cnt%2 == 0)
    {
        frame->setFlipy(false);
        change_stylesheets(ui->flipy_pushButton,4,"flipy.png",ui->flipy_line,ui->flipy_label,"Flip Y-axis");
        flipy_btn_press_cnt = 0;
        ui->status_label->setText("Flipped back to normal Image");
        QTimer::singleShot(3000,this,SLOT(remove_status_messsage_after_timeout()));

    }

}

void Form::on_image_resize_pushButton_clicked()
{
    image_size_press_cnt = image_size_press_cnt + 1;
    if(image_size_press_cnt%2 != 0)
    {
        frend->setImSz(2);
    }
    else if(image_size_press_cnt%2 == 0)
    {
        frend->setImSz(3);
    }

}

void Form::on_mblu_pushButton_2_clicked()
{

}

void Form::on_auto_vga_button_clicked()
{
    QString preference = QDir::currentPath()+"/sens.bin";
    {
        QFile file(preference);
        file.open(QIODevice::WriteOnly | QIODevice::Text);
        QDataStream out(&file);
        out<<sens;
        file.close();
    }
}


void Form::on_hdmi_in_pushButton_clicked()
{
    ext_int->show_external_input();
    hdmi_in_prs_cnt = hdmi_in_prs_cnt + 1;
    if(hdmi_in_prs_cnt%2 != 0)
    {
        change_stylesheets(ui->hdmi_in_pushButton,1,"hdmi_in.png",ui->hdmi_in_line,ui->hdmi_in_label,"HDMI-IN On");
        ui->status_label->setText(" The Video from HDMI streaming is started ");
        QTimer::singleShot(3000,this,SLOT(remove_status_messsage_after_timeout()));

    }
    else if(hdmi_in_prs_cnt%2 == 0)
    {
        change_stylesheets(ui->hdmi_in_pushButton,4,"hdmi_in.png",ui->hdmi_in_line,ui->hdmi_in_label,"HDMI-IN Off");
        ui->status_label->setText("The Video from HDMI streaming is Stopped ");
        QTimer::singleShot(3000,this,SLOT(remove_status_messsage_after_timeout()));
        hdmi_in_prs_cnt = 0;
    }

}


void Form::on_save_preference_clicked()
{
    QString preference = QDir::currentPath()+"/"+preference_file_option+".bin";
    {
        QFile file(preference);
        file.open(QIODevice::WriteOnly | QIODevice::Text);
        QDataStream out(&file);
        out<<pref;
        file.close();
    }
    ui->stackedWidget->setCurrentIndex(5);
}

void Form::on_delete_preference_clicked()
{

}

void Form::on_apply_preference_clicked()
{
    QString user_settings = QDir::currentPath()+"/"+preference_file_option+".bin";
    if (QFileInfo::exists(user_settings)){
        QFile file(user_settings);
        file.open(QIODevice::ReadOnly | QIODevice::Text);
        QDataStream in_all(&file);
        in_all>>pref;
        file.close();\
    }
   // std::cout<<pref.mask<<std::endl;

    if(pref.mask == 1)
    {
        mask_btn_press_cnt = 0;
        on_mask_pushbutton_clicked();
    }
    else if(pref.mask == 2)
    {
        mask_btn_press_cnt = 2;
        on_mask_pushbutton_clicked();

    }
    else if(pref.mask == 0)
    {
        mask_btn_press_cnt = 1;
        on_mask_pushbutton_clicked();
    }

    if(pref.pip_window_size == 1)
    {
        pip_size_btn_prs_cnt = 0;
        on_pip_size_pushButton_clicked();
    }
    else if(pref.pip_window_size == 2)
    {
        pip_size_btn_prs_cnt = 1;
        on_pip_size_pushButton_clicked();
    }
    else if(pref.pip_window_size == 3)
    {
        pip_size_btn_prs_cnt = 2;
        on_pip_size_pushButton_clicked();
    }

    if(pref.pip_window_position == 1)
    {
        pip_position_btn_prs_cnt = 0;
        on_pip_position_selection_clicked();
    }
    else if(pref.pip_window_position == 2)
    {
        pip_position_btn_prs_cnt = 1;
        on_pip_position_selection_clicked();
    }
    else if(pref.pip_window_position == 3)
    {
        pip_position_btn_prs_cnt = 2;
        on_pip_position_selection_clicked();
    }
    else if(pref.pip_window_position == 4)
    {
        pip_position_btn_prs_cnt = 3;
        on_pip_position_selection_clicked();
    }

    if(pref.gain_r == 1)
    {
        frame->setR_gain(0.9);
        ui->preset_slider_value_label->setNum(0.9);
        ui->red_gain_label->setNum(0.9);
    }
    else if(pref.gain_r == 2)
    {
        frame->setR_gain(1.1);
        ui->preset_slider_value_label->setNum(1.1);
        ui->red_gain_label->setNum(1.1);

    }
    else if(pref.gain_r == 3)
    {
        frame->setR_gain(1.0);
        ui->preset_slider_value_label->setNum(1.1);
        ui->red_gain_label->setNum(1.1);
    }


    if(pref.gain_b == 1)
    {
        frame->setB_gain(0.9);
        ui->preset_slider_value_label->setNum(0.9);
        ui->blue_gain_label->setNum(0.9);
    }
    else if(pref.gain_b == 2)
    {
        frame->setB_gain(1.1);
//        ui->preset_slider_value_label->setNum(1.1);
        ui->blue_gain_label->setNum(1.1);

    }
    else if(pref.gain_b == 3)
    {
        frame->setB_gain(1.0);
//        ui->preset_slider_value_label->setNum(1.1);
        ui->blue_gain_label->setNum(1.1);

    }

    if(pref.image_preset == 1)
    {
        preset1_btn_prs_cnt = 0;
        on_preset1_pushButton_clicked();
    }
    else if(pref.image_preset == 2)
    {
        preset2_btn_prs_cnt = 0;
        on_preset2_pushButton_clicked();
    }
    else if(pref.image_preset == 3)
    {
        preset3_btn_prs_cnt = 0;
        on_preset3_pushButton_clicked();
    }
    else if(pref.image_preset == 4)
    {
        preset4_btn_prs_cnt = 0;
        on_preset4_pushButton_clicked();
    }


}

void Form::on_user_options_pushButton_clicked()
{
    ui->menu_options_stackwidget->setCurrentIndex(6);
    ui->label_20->setText("User Options");
}


void Form::on_preference_pushButton_1_clicked()
{
    preference_file_option = "preference_1";
    ui->menu_options_stackwidget->setCurrentIndex(7);
}

void Form::on_preference_pushButton_2_clicked()
{
    preference_file_option = "preference_2";
    ui->menu_options_stackwidget->setCurrentIndex(7);
}

void Form::on_preference_pushButton_3_clicked()
{
    preference_file_option = "preference_3";
    ui->menu_options_stackwidget->setCurrentIndex(7);
}

void Form::on_preference_pushButton_4_clicked()
{
    preference_file_option = "preference_4";
    ui->menu_options_stackwidget->setCurrentIndex(7);

}

void Form::on_preference_pushButton_5_clicked()
{
    preference_file_option = "preference_5";
    ui->menu_options_stackwidget->setCurrentIndex(7);

}

void Form::on_preference_pushButton_6_clicked()
{
    preference_file_option = "preference_6";
    ui->menu_options_stackwidget->setCurrentIndex(7);

}

void Form::on_preference_pushButton_7_clicked()
{
    preference_file_option = "preference_7";
    ui->menu_options_stackwidget->setCurrentIndex(7);

}

void Form::on_preference_pushButton_8_clicked()
{
    preference_file_option = "preference_8";
    ui->menu_options_stackwidget->setCurrentIndex(7);

}

void Form::on_preference_pushButton_clicked()
{
    ui->menu_options_stackwidget->setCurrentIndex(5);
}


void Form::on_preset1_pushButton_6_clicked()
{
//    on_mask_pushbutton_clicked();
    mask_pref_btn_prs_cnt = mask_pref_btn_prs_cnt + 1;
    if(mask_pref_btn_prs_cnt%3 == 1)
    {
        pref.mask = 1;
    }
    else if(mask_pref_btn_prs_cnt%3 == 2)
    {
        pref.mask = 0;
    }
    else if(mask_pref_btn_prs_cnt%3 == 0)
    {
        pref.mask = 2;
        mask_pref_btn_prs_cnt = 0;
    }
}

void Form::on_pref_pip_size_pushButton_clicked()
{
    pip_size_pref_btn_prs_cnt = pip_size_pref_btn_prs_cnt + 1;
    if(pip_size_pref_btn_prs_cnt%3 == 1)
    {
        pref.pip_window_size = 1;
        ui->pref_pip_size_pushButton->setText("1/3 size");
    }
    else if(pip_size_pref_btn_prs_cnt%3 == 2)
    {
        pref.pip_window_size = 2;
        ui->pref_pip_size_pushButton->setText("no pip");
    }
    else if(pip_size_pref_btn_prs_cnt%3 == 0)
    {
        pref.pip_window_size = 3;
        ui->pref_pip_size_pushButton->setText("1/2 size");
    }
}

void Form::on_pref_pip_position_pushButton_clicked()
{
    pip_position_pref_btn_prs_cnt = pip_position_pref_btn_prs_cnt + 1;
    if(pip_position_pref_btn_prs_cnt%4 == 1)
    {
        pref.pip_window_position = 1;
        ui->pref_pip_position_pushButton->setText("Top Left");
    }
    else if(pip_position_pref_btn_prs_cnt%4 == 2)
    {
        pref.pip_window_position = 2;
        ui->pref_pip_position_pushButton->setText("Top Right");

    }
    else if(pip_position_pref_btn_prs_cnt%4 == 3)
    {
        pref.pip_window_position = 3;
        ui->pref_pip_position_pushButton->setText("Bottom Left");

    }
    else if(pip_position_pref_btn_prs_cnt%4 == 0)
    {
        pref.pip_window_position = 4;
        ui->pref_pip_position_pushButton->setText("Bottom Left");

    }
}

void Form::on_pref_gain_r_pushButton_clicked()
{
    r_gain_pref_btn_prs_count = r_gain_pref_btn_prs_count + 1;
    if(r_gain_pref_btn_prs_count%3 == 1 )
    {
        pref.gain_r = 1;
        ui->pref_gain_r_pushButton->setText("0.9 R Gain");
    }
    else if(r_gain_pref_btn_prs_count%3 == 2)
    {
        pref.gain_r = 2;
        ui->pref_gain_r_pushButton->setText("1.1 R Gain");
    }
    else if(r_gain_pref_btn_prs_count%3 == 0)
    {
        pref.gain_r = 3;
        ui->pref_gain_r_pushButton->setText("1 R Gain");
    }
}

void Form::on_pref_gain_b_pushButton_clicked()
{
    b_gain_pref_btn_prs_cnt = b_gain_pref_btn_prs_cnt + 1;
    if(b_gain_pref_btn_prs_cnt%3 == 1 )
    {
        pref.gain_b = 1;
        ui->pref_gain_b_pushButton->setText("0.9 B Gain");
    }
    else if(b_gain_pref_btn_prs_cnt%3 == 2)
    {
        pref.gain_b = 2;
        ui->pref_gain_b_pushButton->setText("1.1 B Gain");
    }
    else if(b_gain_pref_btn_prs_cnt%3 == 0)
    {
        pref.gain_b = 3;
        ui->pref_gain_b_pushButton->setText("1 B Gain");
    }

}

void Form::on_pref_image_preset_pushButton_clicked()
{
    preset_pref_btn_prs_cnt = preset_pref_btn_prs_cnt + 1;
    if(preset_pref_btn_prs_cnt%4 == 1)
    {
        pref.image_preset = 1;
        ui->pref_image_preset_pushButton->setText("Preset 1");
    }
    else if(preset_pref_btn_prs_cnt%4 == 2)
    {
        pref.image_preset = 2;
        ui->pref_image_preset_pushButton->setText("Preset 2");
    }
    else if(preset_pref_btn_prs_cnt%4 == 3)
    {
        pref.image_preset = 3;
        ui->pref_image_preset_pushButton->setText("Preset 3");
    }
    else if(preset_pref_btn_prs_cnt%4 == 4)
    {
        pref.image_preset = 4;
        ui->pref_image_preset_pushButton->setText("Preset 4");
    }

}

void Form::on_pref_mask_pushButton_clicked()
{
    mask_pref_btn_prs_cnt = mask_pref_btn_prs_cnt + 1;
    if(mask_pref_btn_prs_cnt%3 == 1)
    {
        pref.mask = 1;
        ui->pref_mask_pushButton->setText("Round");
    }
    else if(mask_pref_btn_prs_cnt%3 == 2)
    {
        pref.mask = 0;
        ui->pref_mask_pushButton->setText("No Mask");

    }
    else if(mask_pref_btn_prs_cnt%3 == 0)
    {
        pref.mask = 2;
        ui->pref_mask_pushButton->setText("octagon");
        mask_pref_btn_prs_cnt = 0;
    }
}

void Form::close_mainwidget_slot()
{
    emit close_mainwidget_signal(1);
}

static int illumCnt = 0;
void Form::on_illum_pushButton_clicked()
{
    illumCnt = illumCnt+1;
    if(illumCnt % 2 == 1)
    {
        frame->setIllum_click(true);
        change_stylesheets(ui->illum_pushButton,1,"mblue.png",ui->zoom_line,ui->illum_label,"Illum On");
    }
    else{
        frame->setIllum_click(false);
        change_stylesheets(ui->illum_pushButton,1,"mblue.png",ui->zoom_line,ui->illum_label,"Illum Off");
    }
}

static int deinterCnt = 0;
void Form::on_deinter_pushButton_clicked()
{
    deinterCnt = deinterCnt+1;
    if(deinterCnt % 3 == 1)
    {
        //frame->setIllum_click(true);
        codecproc->setCombineFields(true);
        codecproc->setAdaptiveDeinter(false);
        change_stylesheets(ui->deinter_pushButton,1,"mblue.png",ui->zoom_line,ui->deinter_label,"combined");
    }
    else if(deinterCnt % 3 == 2)
    {
        //frame->setIllum_click(false);
        codecproc->setCombineFields(false);
        codecproc->setAdaptiveDeinter(true);
        change_stylesheets(ui->deinter_pushButton,2,"mblue.png",ui->zoom_line,ui->deinter_label,"adaptive");
    }
    else{
        codecproc->setCombineFields(false);
        codecproc->setAdaptiveDeinter(false);
        change_stylesheets(ui->deinter_pushButton,3,"mblue.png",ui->zoom_line,ui->deinter_label,"interpolated");
    }
}

void Form::on_toggle_checkbox_blue_white_stateChanged(int arg1)
{
    std::cout<<arg1<<std::endl;
    if(arg1 == 2)
    {
        uvc->send_param(BLUE_LED,1);
    }
    else
    {
        uvc->send_param(BLUE_LED,0);

    }
}

void Form::led_auto_control_sync(int auto_control_state)
{
    switch(auto_control_state)
    {
    case 1:
    {
        alc_aec_state.init(4,0);
        on_alc_aec_pushbutton_clicked();
    }
        break;
    case 2:
    {
        alc_aec_state.init(4,1);
        on_alc_aec_pushbutton_clicked();

    }
        break;
    case 3:
    {
        alc_aec_state.init(4,2);
        on_alc_aec_pushbutton_clicked();

    }
        break;
    case 0:
    {
//        alc_aec_state.init(4,3);
//        on_alc_aec_pushbutton_clicked();

    }
        break;
    }
}

void Form::auto_control_led_sync(int led_state)
{
    switch(led_state)
    {
    case 1:
    {
        ledstate.init(4,0);
        on_led_pushButton_clicked();
    }
        break;
    case 2:
    {
        ledstate.init(4,1);
        on_led_pushButton_clicked();
    }
        break;
    case 3:
    {
        ledstate.init(4,2);
        on_led_pushButton_clicked();
    }
        break;
    case 0:
    {
//        ledstate.init(4,3);
//        on_led_pushButton_clicked();();

    }
        break;
    }

}


void Form::on_pump_pushButton_2_clicked()
{

}

void Form::on_save_frame_pushButton_clicked()
{

    QString user_settings = QDir::currentPath()+"/calib.bin";
    if (QFileInfo::exists(user_settings)){
        QFile file(user_settings);
        file.open(QIODevice::ReadOnly | QIODevice::Text);
        QDataStream in_all(&file);
        in_all>>calib;
        file.close();
    }

    user.rg = calib.rg;
    user.shd = calib.shd;
    user.shp = calib.shp;
    user.h1 = calib.h1;
    user.h2 = calib.h2;

//    std::cout<<user.led_value1<<std::endl;
    int num = ui->spinBox->value();
//    std::cout<<"num  "<<num<<std::endl;
    QTime time = QTime::currentTime();
    QString current_time = time.toString("hhmmss");
//    std::cout<<current_time.toStdString()<<std::endl;
    QDate date = QDate::currentDate();
    QString current_date = date.toString("ddMMyyyy");
//    std::cout<<(date.toString("dd-MM-yyyy")).toStdString()<<std::endl;
    filen,filename = "Saved_Frames/date"+current_date;
    QDir().mkdir(filename);
    filen = filename + "/"+current_time;
    QDir().mkdir(filen);
    codecproc->filename = filen.toStdString();
//    frame->test_flag = false;
    frame->file = true;
    frame->filename = filen.toStdString();
//    std::cout<<frame->filename<<std::endl;
    frame->setImage_save(true);
    frame->image_name = "with_wb_gamma.png";
    codecproc->save_raw_flag = true;
    if(num == 0)
    {
        num = 10;
    }
    codecproc->limit = num;




    QString userselect = QDir::currentPath()+"/"+filen+"/user_selected_data.text";
    {
        QFile file(userselect);
        file.open(QIODevice::WriteOnly | QIODevice::Text);

        QTextStream out(&file);
        out<<"LED STATUS : "<< user.led_state<<"   "<<"LED SLIDER 1 : "<<"   "<<user.led_value1<<"   "<<"LED SLIDER 2 : "<<user.led_value2 <<"\n";
        out<<"\n";
        out<<"EXPOSURE : "<<user.exposture<<"   "<<"VGA GAIN : "<<user.vga_gain<<"   "<<"Black clamp level : "<<user.black_level_clamp<<"\n";
        out<<"\n";
        out<<"H1 : "<<user.h1<<"    "<<"H2 : "<<user.h2<<"  "<<"RG : "<<user.rg<<"  "<<"SHP : "<<user.shp<<"    "<<"SHP : "<<user.shd<<"\n";
        out<<"\n";
        out<<"Sharpness : "<<user.sharpness<<"  "<<"Brightness : "<<user.brightness<<"  "<<"Gamma : "<<user.gamma<<"\n";
        out<<"\n";
        out<<"Contrast : "<<user.contrast<<"    "<<"Red Gain : "<<user.redg<<"  "<<"Blue Gain : "<<user.blueg<<"\n";
        out<<"\n";
        out<<"Threshold : "<<user.threshold<<"  "<<"Chroma : "<<user.chroma<<"\n";

        file.close();
    }

}

void Form::button_mapping(int button)
{
    if ((selection1 == "Led Control") && (button == 1))
      {
          on_led_pushButton_clicked();
          button = 0;
      } else if((selection1 == "Led Control") && (button == 2))
      {
        on_led_pushButton_clicked();
        button = 0;

      }else if((selection1 == "Led Control") && (button == 3))
      {
        on_led_pushButton_clicked();
        button = 0;
      }

    if ((selection1 == "Mask") && (button == 1))
      {
          on_mask_pushbutton_clicked();
          button = 0;
      } else if((selection1 == "Mask") && (button == 2))
      {
        on_mask_pushbutton_clicked();
        button = 0;

      }else if((selection1 == "Mask") && (button == 3))
      {
        on_mask_pushbutton_clicked();
        button = 0;
      }

    if ((selection1 == "Freeze") && (button == 1))
      {
          on_freeze_pushButton_clicked();
          button = 0;
      } else if((selection1 == "Freeze") && (button == 2))
      {
        on_freeze_pushButton_clicked();
        button = 0;

      }else if((selection1 == "Freeze") && (button == 3))
      {
        on_freeze_pushButton_clicked();
        button = 0;
      }

    if ((selection1 == "Zoom") && (button == 1))
      {
          on_led_pushButton_clicked();
          button = 0;
      } else if((selection1 == "Zoom") && (button == 2))
      {
        on_led_pushButton_clicked();
        button = 0;

      }else if((selection1 == "Zoom") && (button == 3))
      {
        on_led_pushButton_clicked();
        button = 0;
      }

    if ((selection1 == "Save Image") && (button == 1))
      {
          on_led_pushButton_clicked();
          button = 0;
      } else if((selection1 == "Save Image") && (button == 2))
      {
        on_led_pushButton_clicked();
        button = 0;

      }else if((selection1 == "Save Image") && (button == 3))
      {
        on_led_pushButton_clicked();
        button = 0;
      }

    if ((selection1 == "Pump Control") && (button == 1))
      {
          on_led_pushButton_clicked();
          button = 0;
      } else if((selection1 == "Pump Control") && (button == 2))
      {
        on_led_pushButton_clicked();
        button = 0;

      }else if((selection1 == "Pump Control") && (button == 3))
      {
        on_led_pushButton_clicked();
        button = 0;
      }

    if ((selection1 == "Mblu") && (button == 1))
      {
          on_led_pushButton_clicked();
          button = 0;
      } else if((selection1 == "Mblu") && (button == 2))
      {
        on_led_pushButton_clicked();
        button = 0;

      }else if((selection1 == "Mblu") && (button == 3))
      {
        on_led_pushButton_clicked();
        button = 0;
      }

    if ((selection1 == "White Balance") && (button == 1))
      {
          on_led_pushButton_clicked();
          button = 0;
      } else if((selection1 == "White Balance") && (button == 2))
      {
        on_led_pushButton_clicked();
        button = 0;

      }else if((selection1 == "White Balance") && (button == 3))
      {
        on_led_pushButton_clicked();
        button = 0;
      }

    if ((selection1 == "Image Size") && (button == 1))
      {
          on_led_pushButton_clicked();
          button = 0;
      } else if((selection1 == "Image Size") && (button == 2))
      {
        on_led_pushButton_clicked();
        button = 0;

      }else if((selection1 == "Image Size") && (button == 3))
      {
        on_led_pushButton_clicked();
        button = 0;
      }

    if ((selection1 == "Save Video") && (button == 1))
      {
          on_led_pushButton_clicked();
          button = 0;
      } else if((selection1 == "Save Video") && (button == 2))
      {
        on_led_pushButton_clicked();
        button = 0;

      }else if((selection1 == "Save Video") && (button == 3))
      {
        on_led_pushButton_clicked();
        button = 0;
      }

}

#if led_ramp == 1
void Form::on_LED_RAMP_pushButton_clicked()
{
    timer_hotplug->start(100);
}
#endif

void Form::on_button_pushbutton_clicked()
{
    ui->menu_options_stackwidget->setCurrentIndex(4);
    ui->label_20->setText("Button Interface");
}

void Form::on_remove_options_pushButton_clicked()
{
    save = save + 1;
    if(save%2 != 0)
    {
       frame->setWhite_balance_available(false);
       frame->setGamma(1);
       frame->setSharp_value(0);
       frame->filename = filen.toStdString();
       frame->file = true;
       frame->image_name = "no_wb_gamma.png";

//       frame->gam = disp.gamma;
    }
    else
    {
//        frame->test_flag = true;
        frame->setImage_save(true);
        frame->setWhite_balance_available(true);
        preset1_btn_prs_cnt = 0;
        on_preset1_pushButton_clicked();

    }

}

void Form::on_button1_pushButton_clicked()
{
    button1_prs_cnt = button1_prs_cnt + 1;
    if(button1_prs_cnt%2 != 0)
    {
        selected_button = 1;
        selected_button_text = ui->button1_pushButton->text();
        selected_push_button = ui->button1_pushButton;
        ui->button1_pushButton->setStyleSheet("background-color: white;"
                                               "color:black;");
    }
    else if(button1_prs_cnt%2 == 0)
    {
        selected_button = 0;
        ui->button1_pushButton->setStyleSheet("background: transparent;"
                                               "color:white;");
    }
}

void Form::on_button2_pushButton_clicked()
{
    button2_prs_cnt = button2_prs_cnt + 1;
    if(button2_prs_cnt%2 != 0)
    {
        selected_button = 2;
        ui->button2_pushButton->setStyleSheet("background-color: white;"
                                               "color:black;");
        selected_button_text = ui->button2_pushButton->text();
        selected_push_button = ui->button2_pushButton;
    }
    else if(button2_prs_cnt%2 == 0)
    {
        selected_button = 0;
        ui->button2_pushButton->setStyleSheet("background: transparent;"
                                               "color:white;");
    }

}

void Form::on_button3_pushButton_clicked()
{
    button3_prs_cnt = button3_prs_cnt + 1;
    if(button3_prs_cnt%2 != 0)
    {
        selected_button = 3;
        ui->button3_pushButton->setStyleSheet("background-color: white;"
                                               "color:black;");
        selected_button_text = ui->button3_pushButton->text();
        selected_push_button = ui->button3_pushButton;
    }
    else if(button3_prs_cnt%2 == 0)
    {
        selected_button = 0;
        ui->button3_pushButton->setStyleSheet("background: transparent;"
                                               "color:white;");
    }

}

void Form::dynamic_radio_button_name_change(QPushButton *button, QRadioButton *radio)
{
    button->setText(selected_radio_button_text);
    radio->setText(selected_button_text);
    selected_button_text = button->text();
}

void Form::on_radioButton_clicked()
{
    selected_radio_button_text = ui->radioButton->text();
    dynamic_radio_button_name_change(selected_push_button, ui->radioButton);
}

void Form::on_radioButton_2_clicked()
{
    selected_radio_button_text = ui->radioButton_2->text();
    dynamic_radio_button_name_change(selected_push_button, ui->radioButton_2);

}

void Form::on_radioButton_3_clicked()
{
    selected_radio_button_text = ui->radioButton_3->text();
    dynamic_radio_button_name_change(selected_push_button, ui->radioButton_3);

}

void Form::on_radioButton_4_clicked()
{
    selected_radio_button_text = ui->radioButton_4->text();
    dynamic_radio_button_name_change(selected_push_button, ui->radioButton_4);

}

void Form::on_radioButton_5_clicked()
{
    selected_radio_button_text = ui->radioButton_5->text();
    dynamic_radio_button_name_change(selected_push_button, ui->radioButton_5);

}

void Form::on_radioButton_6_clicked()
{
    selected_radio_button_text = ui->radioButton_6->text();
    dynamic_radio_button_name_change(selected_push_button, ui->radioButton_6);

}

void Form::on_radioButton_7_clicked()
{
    selected_radio_button_text = ui->radioButton_7->text();
    dynamic_radio_button_name_change(selected_push_button, ui->radioButton_7);

}

void Form::on_radioButton_8_clicked()
{
    selected_radio_button_text = ui->radioButton_8->text();
    dynamic_radio_button_name_change(selected_push_button, ui->radioButton_8);

}

void Form::on_set_button_pushButton_clicked()
{
    selection1 = (ui->button1_pushButton->text()).toStdString();
    selection2 = (ui->button2_pushButton->text()).toStdString();
    selection3 = (ui->button3_pushButton->text()).toStdString();

    std::cout<< selection1 << " -- "<< selection2 << " -- "<< selection3 <<std::endl;
}

void Form::on_caliberation_pushButton_clicked()
{
    uvcform = new Form_uvc(uvc);
    uvcform->show();
}

void Form::on_zoom_slider_valueChanged(int value)
{
    float val = value * 0.05;
    ui->zoom_slider_value_label->setNum(val);
    frend->setZoomVal(val);
}
